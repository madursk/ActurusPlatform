#ifndef DIAG_HANDLER_CFG_H
#define DIAG_HANDLER_CFG_H

#include "DiagHandler_Types.h"
#include "UART_Types.h"

typedef struct
{
    uint8_t u8Status;
    ComDrivers_t cxComDriver;
    ComHwUnit_t cxComDriverId;
#if(INTERNAL_DIAG_CHANNEL==UART_INT_DIAG)
    UartHwUnits_t* cxInternalDiagnosticChannelPtr;
#endif
}InternalDiag_t;

typedef struct
{
    InternalDiag_t cxInternalDiagChannel;

}Diagnostic_Channels_t;
#if(INTERNAL_DIAG_CHANNEL==UART_INT_DIAG)
    #ifdef INT_DIAG_UART0
    #define INT_DIAG_HWUNIT UART_HW_UNIT0
    #endif
    #ifdef INT_DIAG_UART1
     #define INT_DIAG_HWUNIT UART_HW_UNIT1
    #endif
    #ifdef INT_DIAG_UART2
     #define INT_DIAG_HWUNIT UART_HW_UNIT2
    #endif
    #ifdef INT_DIAG_UART3
     #define INT_DIAG_HWUNIT UART_HW_UNIT3
    #endif
    #ifdef INT_DIAG_UART4
     #define INT_DIAG_HWUNIT UART_HW_UNIT4
    #endif
    #ifdef INT_DIAG_UART5
     #define INT_DIAG_HWUNIT UART_HW_UNIT5
    #endif

#endif

VAR(Diagnostic_Channels_t, DEFAULT) cxDiagChannels;
#if(INTERNAL_DIAG_CHANNEL==UART_INT_DIAG)
P2VAR(UartHwUnits_t, DEFAULT) cxComInternalChannelPtr;
#endif

FUNC(std_ReturnType,AUTOMATIC) stdfnDiagnosticLoadout(void);
FUNC(std_ReturnType,AUTOMATIC) stdfnSetInternalDiagnosticChannel(void);
FUNC_P2VAR(void,AUTOMATIC) fnGetInternalDiagnosticChannel(void);
FUNC(boolean, AUTOMATIC) bGetTxConfirmationIntDiag(void);
FUNC(std_ReturnType,AUTOMATIC) stdfnInternalDiagnosticChannel_Send(char_t* chQueueJobPtr);



#endif