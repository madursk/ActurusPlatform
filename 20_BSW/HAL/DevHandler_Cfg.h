#ifndef DEV_HANDLER_CFG_H
#define DEV_HANDLER_CFG_H

#include "DevHandler_Types.h"

#define MAX_NUMBER_DEVICES 10u

typedef struct
{
    DeviceType_t device_t;
    InterfaceType_t interface_t;
    InterfaceChannel_t interfaceChannel_t;

}DevHandler_Cfg_st;

typedef DevHandler_Cfg_st* DevHandler_Cfg_stPtr;

FUNC(void,AUTOMATIC)vfnSetDevices();
#endif