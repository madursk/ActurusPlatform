#include "DevHandler.h"
#include "DevHandler_Cfg.h"
#include "DiagM.h"

#define PRINT(x)     vfnDiagnosticConsole_Print(HAL,DEVH,x)

extern DevHandler_Cfg_stPtr devices_at[MAX_NUMBER_DEVICES];

FUNC(void,AUTOMATIC)vfnDevHandler_Init()
{
    vfnSetDevices();
}

FUNC(void,AUTOMATIC)vfnDevHandler_Main()
{
    
}