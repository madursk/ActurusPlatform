#ifndef DIAGHANDLER_H
#define DIAGHANDLER_H
#include "DiagHandler_cfg.h"

struct queueNode
{
    char_t chMessage[INT_DIAG_MSG_BUFFER];
    struct queueNode* cxNextQueueNode;
};

typedef struct queueNode IntNodeQueue_t;

FUNC(uint16_t,AUTOMATIC)u16fnDiagHandler_Init();
FUNC(void, AUTOMATIC) vfnDiagHandler_TxQueueProcessing(void);
FUNC(std_ReturnType,AUTOMATIC) stdfnDiagHandler_InsertToQueue(char_t cRxDiagMessage[], uint8_t u8SelectedChannel);
FUNC(std_ReturnType,AUTOMATIC) stdfnDiagHandler_RemoveFromQueue(uint8_t u8SelectedChannel);
FUNC(void,AUTOMATIC) vfnBuildFrameForIntDiag(Layer_t cxlayerSource, Module_t cxModuleSource, char_t RawInput[]);
FUNC(uint8_t, AUTOMATIC) u8fnSearchIntDiagMsg(char_t* chSourceBufferPtr, char_t chCharacterToSearch);
FUNC(std_ReturnType,AUTOMATIC) stdfnInternalDiag_Dequeue(void);
FUNC(std_ReturnType,AUTOMATIC) stdfnInternalDiag_Enqueue(char_t lcMessageVctr[]);

#endif