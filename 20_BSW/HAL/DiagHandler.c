
#include "DiagHandler.h"
#include "DiagHandler_cfg.h"

InternalDiag_Msg_t cxIntDiagMessgCstrct;


IntNodeQueue_t* cxHeadQueuePtr;
IntNodeQueue_t cxNodeQueue[16u];
uint8_t u8QueueCounter = 0u;
uint8_t u8OverallQueueInsertion = 0u;
IntNodeQueue_t* cxTailQueuePtr;



/**
 * Name u16fnDiagHandler_Init
 * @brief Function that initializes
 * the dianostic handler
 * 
 * @param None
 * 
 * @return TBD which type of return should be
 */
uint16_t u16fnDiagHandler_Init()
{
    uint16_t diagHandlerStatus_lu16 = 0;
    stdfnDiagnosticLoadout();
    /*Sets the Internal Diagnostic queue head and tail as empty*/
    cxHeadQueuePtr = NULLPTR;
    cxTailQueuePtr = NULLPTR;

    return diagHandlerStatus_lu16;
}

/**
 * Name vfnDiagHandler_TxQueueProcessing
 * @brief Function Processes all the transmissions
 * for internal and external diagnostics
 * 
 * @param None
 * 
 * @return void
 */
FUNC(void, AUTOMATIC) vfnDiagHandler_TxQueueProcessing(void)
{
    /*checks if there is transmission ongoing and if there is something in queue*/
    if((bGetTxConfirmationIntDiag() != FALSE) && (NULLPTR != cxHeadQueuePtr))
    {
        /*Sends*/
        stdfnInternalDiagnosticChannel_Send(&(*cxHeadQueuePtr).chMessage[0]);
        stdfnDiagHandler_RemoveFromQueue(SELECT_INTERNAL_CHANNEL);
    }
}

/**
 * Name stdfnDiagHandler_InsertToQueue
 * @brief Function that inserts array of characters into a queue
 * @param cRxDiagMessage: array of characters to instert
 * @param u8SelectedChannel: which diagnostic channel to insert
 * 
 * @return std_ReturnType returns E_OK is message inserted
 */
FUNC(std_ReturnType,AUTOMATIC) stdfnDiagHandler_InsertToQueue(char_t cRxDiagMessage[], uint8_t u8SelectedChannel)
{
    std_ReturnType stdResult;
    IntNodeQueue_t* testcxHeadQueuePtr = NULLPTR;

    if( SELECT_INTERNAL_CHANNEL ==u8SelectedChannel)
    {
        stdfnInternalDiag_Enqueue(&cRxDiagMessage[0]);
        stdResult= E_OK;

    }
    else if(SELECT_EXTERNAL_CHANNEL==u8SelectedChannel)
    {
        stdResult = E_OK;
    }
    else
    {
        stdResult = E_NOT_OK;
    }
    return stdResult;
}

/**
 * Name stdfnDiagHandler_RemoveFromQueue
 * @brief Function that removes messages from the queue
 * 
 * @param u8SelectedChannel: which diagnostic channel to insert
 * 
 * @return std_ReturnType returns E_OK if message is removed
 */
FUNC(std_ReturnType,AUTOMATIC) stdfnDiagHandler_RemoveFromQueue(uint8_t u8SelectedChannel)
{
    std_ReturnType stdResult;

    if( SELECT_INTERNAL_CHANNEL ==u8SelectedChannel)
    {
        stdfnInternalDiag_Dequeue();
        stdResult= E_OK;

    }
    else if(SELECT_EXTERNAL_CHANNEL==u8SelectedChannel)
    {
        stdResult = E_OK;
    }
    else
    {
        stdResult = E_NOT_OK;
    }
    return stdResult;
}

/**
 * Name stdfnInternalDiag_Enqueue
 * @brief Function that increases the tail of the queue 
 * 
 * 
 * @param lcMessageVctr: array of characters to insert at tail
 * 
 * @return std_ReturnType returns E_OK if tail increased
 *         return E_NOT_OK if queue is full */
FUNC(std_ReturnType,AUTOMATIC) stdfnInternalDiag_Enqueue(char_t lcMessageVctr[])
{
    std_ReturnType stdResult = E_NOT_OK;
    
    
    if(MAX_NUM_INT_INSERTIONS >= u8OverallQueueInsertion)
    {
        for(uint8_t u8MsgCounter = 0u; u8MsgCounter< INT_DIAG_MSG_BUFFER; u8MsgCounter++)
        {
            cxNodeQueue[u8OverallQueueInsertion].chMessage[u8MsgCounter] = lcMessageVctr[u8MsgCounter];
        }
            cxNodeQueue[u8OverallQueueInsertion].cxNextQueueNode = NULLPTR;
        
        if(NULLPTR == cxHeadQueuePtr)
        {
            cxHeadQueuePtr = &cxNodeQueue[u8OverallQueueInsertion];
            u8OverallQueueInsertion++;
            u8QueueCounter++;;
        }
        else
        {
            cxTailQueuePtr->cxNextQueueNode = &cxNodeQueue[u8OverallQueueInsertion];
            u8OverallQueueInsertion++;
            u8QueueCounter++;
        }
        stdResult = E_OK;
    }
    else if((MAX_NUM_INT_INSERTIONS > u8OverallQueueInsertion) && (MAX_NUM_INT_INSERTIONS >=  u8QueueCounter))
    {
        stdResult = E_NOT_OK;
    }
    else
    {
        u8OverallQueueInsertion =0u;
        for(uint8_t u8MsgCounter = 0u; u8MsgCounter< INT_DIAG_MSG_BUFFER; u8MsgCounter++)
        {
            cxNodeQueue[u8OverallQueueInsertion].chMessage[u8MsgCounter] = lcMessageVctr[u8MsgCounter];
        }
            cxNodeQueue[u8OverallQueueInsertion].cxNextQueueNode = NULLPTR;
        if(NULLPTR == cxHeadQueuePtr)
        {
            cxHeadQueuePtr = &cxNodeQueue[u8OverallQueueInsertion];
            u8OverallQueueInsertion++;
            u8QueueCounter++;;
        }
        stdResult = E_OK;
    }
    return stdResult;
}


/**
 * Name stdfnInternalDiag_Enqueue
 * @brief Function that removes from head of queue
 * 
 * 
 * @param none
 * 
 * @return std_ReturnType returns E_OK if message was remove
 */
FUNC(std_ReturnType,AUTOMATIC) stdfnInternalDiag_Dequeue(void)
{
    std_ReturnType stdResult = E_NOT_OK;
    
    if(NULLPTR == cxHeadQueuePtr)
    {
        cxTailQueuePtr = NULLPTR;
    }
    else
    {
        for(uint8_t u8MsgCounter = 0u; u8MsgCounter< INT_DIAG_MSG_BUFFER; u8MsgCounter++)
        {
            cxHeadQueuePtr->chMessage[u8MsgCounter] = 0u;
        }
        cxHeadQueuePtr = cxHeadQueuePtr->cxNextQueueNode;
        u8QueueCounter--;
        stdResult = E_OK;
    }
    return stdResult;
}


/**
 * Name vfnBuildFrameForIntDiag
 * @brief Function that fromats the message from the upper layers
 * 
 * 
 * @param Layer_t cxlayerSource parameter to indicate from what layer is message sent
 * @param Module_t cxModuleSource which module is the one sending a message
 * @param char_t RawInput[] the actual unformatted message)
 * 
 * @return void
 */
FUNC(void,AUTOMATIC) vfnBuildFrameForIntDiag(Layer_t cxlayerSource, Module_t cxModuleSource, char_t RawInput[])
{
    uint8_t u8StartingIndex = 0u;
    uint8_t u8RawInputSize;
    char_t chInternalDiagFormattedMsg[INT_DIAG_MSG_BUFFER];
    switch (cxlayerSource)
    {
        case(MCAL):
        {
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'M';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'C';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'A';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'L';
            chInternalDiagFormattedMsg[u8StartingIndex++] = ':';
        }
        break;
        case(HAL):
        {
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'H';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'A';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'L';
            chInternalDiagFormattedMsg[u8StartingIndex++] = ':';
        }
        break;
        case(MAL):
        {
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'M';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'A';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'L';
            chInternalDiagFormattedMsg[u8StartingIndex++] = ':';
        }
        break;

        case(APP):
        {
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'A';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'P';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'P';
            chInternalDiagFormattedMsg[u8StartingIndex++] = ':';
        }
        break;
    
    default:
        break;
    }
    switch (cxModuleSource)
    {
        
        case(PORT):
        {
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'P';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'O';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'R';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'T';
            chInternalDiagFormattedMsg[u8StartingIndex++] = ':';
        }
        break;
        case(UART):
        {
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'U';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'A';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'R';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'T';
            chInternalDiagFormattedMsg[u8StartingIndex++] = ':';
        }
        break;
        case(I2C):
        {
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'I';
            chInternalDiagFormattedMsg[u8StartingIndex++] = '2';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'C';
            chInternalDiagFormattedMsg[u8StartingIndex++] = ':';
        }
        break;

        case(ETH):
        {
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'E';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'T';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'H';
            chInternalDiagFormattedMsg[u8StartingIndex++] = ':';
        }
        break;
        case(CAN):
        {
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'C';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'A';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'N';
            chInternalDiagFormattedMsg[u8StartingIndex++] = ':';
        }
        break;
        case(DIO):
        {
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'D';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'I';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'O';
            chInternalDiagFormattedMsg[u8StartingIndex++] = ':';
        }
        break;
        case(ADC):
        {
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'A';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'D';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'C';
            chInternalDiagFormattedMsg[u8StartingIndex++] = ':';
        }
        break;
        case(PIT):
        {
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'P';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'I';
            chInternalDiagFormattedMsg[u8StartingIndex++] = 'T';
            chInternalDiagFormattedMsg[u8StartingIndex++] = ':';
        }
        break;
    
    default:
        break;
    }
    /*Search where is the message ending*/
    u8RawInputSize = u8fnSearchIntDiagMsg(RawInput,(char_t)'\0');

    /*concatenate buffer of internal diag with raw message*/
    for(uint8_t u8Parser = 0u; u8Parser <= u8RawInputSize; u8Parser++)
    {
        chInternalDiagFormattedMsg[u8StartingIndex + u8Parser] = RawInput[u8Parser];
    }
    /*Insert the concatenated message to queue*/
    stdfnDiagHandler_InsertToQueue(&chInternalDiagFormattedMsg[0],SELECT_INTERNAL_CHANNEL);

}

/**
 * Name u8fnSearchIntDiagMsg
 * @brief Function that searchs an array of characters for specific character
 * 
 * 
 * @param char_t* chSourceBufferPtr the message to search
 * @param char_t chCharacterToSearch the character to search
 * 
 * @return uint8_t index of where the character is located
 */
FUNC(uint8_t, AUTOMATIC) u8fnSearchIntDiagMsg(char_t* chSourceBufferPtr, char_t chCharacterToSearch)
{
    boolean bIsSeparatorFound = FALSE;
    uint8_t u8IndexCounter = 0u;

    while(bIsSeparatorFound != TRUE)
    {
        u8IndexCounter++;
        if((chSourceBufferPtr[u8IndexCounter] == chCharacterToSearch) || ( INT_DIAG_MSG_BUFFER <=u8IndexCounter))
        {   
            bIsSeparatorFound = TRUE;
        }
    }
    return u8IndexCounter;
    
}