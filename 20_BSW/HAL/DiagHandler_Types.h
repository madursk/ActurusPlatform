#ifndef DIAGHANDLER_TYPES_H
#define DIAGHANDLER_TYPES_H

#include "Datatypes.h"

typedef uint8_t IntDiagChannel_t;
typedef uint8_t ExtDiagChannel_t;
typedef uint8_t ComHwUnit_t;
typedef enum
{
    COM_UART_DRIVER,
    COM_I2C_DRIVER,
    COM_CAN_DRIVER,
    COM_SPI_DRIVER,
    COM_ETHERNET_DRIVER
}ComDrivers_t;



typedef enum 
{
    PORT,
    UART,
    I2C,
    SPI,
    ETH,
    CAN,
    DIO,
    ADC,
    PIT,
    FTM,
    NVIC,
    SIM,
    DEVH,
    DIAGH,
    ECUM,
    IOH,
    BSWM,
    NA
}Module_t;


typedef enum
{
    MCAL,
    HAL,
    MAL,
    APP
}Layer_t;

typedef struct
{
    uint32_t u32Timestamp;
    Layer_t cxLayer;
    Module_t cxModule;
    char_t* cMessagePtr;
}InternalDiag_Msg_t;



#define SELECT_INTERNAL_CHANNEL (1u)
#define SELECT_EXTERNAL_CHANNEL (2u)
#define INTERNAL_DIAGNOSTIC_CFG_ST_NP_E 0x02u
#define INTERNAL_DIAGNOSTIC_NO_CHN_E    0x03u
#define EXTERNAL_DIAGNOSTIC_CFG_ST_NP_E 0x02u
#define EXTERNAL_DIAGNOSTIC_NO_CHN_E    0x03u

#define INTERNAL_DIAGNOSTIC_MSG_MCAL_SIZE 6u

#endif