#ifndef DEV_HANDLER_TYPES_H
#define DEV_HANDLER_TYPES_H

#include "Datatypes.h"



typedef enum
{
    OLED,
    ACCELEROMETER,
    TEMPERATURE,
    LCD,
    JOYSTICK,
    USB,
    SDHC

}DeviceType_en;

typedef enum
{
    UARTIf,
    I2CIf,
    SPIIf,
    USBIf,
    SDHCIf,
    OneWireIf,
    CANIf,
    EthernetIf


}InterfaceType_en;

typedef DeviceType_en DeviceType_t;
typedef InterfaceType_en InterfaceType_t;
typedef uint8_t InterfaceChannel_t;
#endif