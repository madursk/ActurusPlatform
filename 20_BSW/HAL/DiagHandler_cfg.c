
#include "DiagHandler_cfg.h"
#include "UART.h"
;

/**
 * Name stdfnDiagnosticLoadout
 * @brief Loads all the configurations to
 * internal variables and diagnostic channels
 * 
 * @param None
 * 
 * @return stdResult returns E_OK if all channels were
 * set correctly
 */
FUNC(std_ReturnType,AUTOMATIC) stdfnDiagnosticLoadout(void)
{
    std_ReturnType stdResult = E_NOT_OK;
    stdfnSetInternalDiagnosticChannel();
#if(INTERNAL_DIAG_CHANNEL==UART_INT_DIAG)
    cxDiagChannels.cxInternalDiagChannel.cxComDriver = INTERNAL_DIAG_CHANNEL;
    cxDiagChannels.cxInternalDiagChannel.cxComDriverId = INT_DIAG_HWUNIT;
    cxDiagChannels.cxInternalDiagChannel.cxInternalDiagnosticChannelPtr =fnGetInternalDiagnosticChannel();
    stdResult = E_OK;
#endif
    return stdResult;
}

/**
 * Name stdfnSetInternalDiagnosticChannel
 * @brief Maps the internal diagnostic channel 
 * to the hardware channel
 * 
 * @param None
 * 
 * @return stdResult returns E_OK if hardware channel was
 * mapped correctly
 */
FUNC(std_ReturnType,AUTOMATIC) stdfnSetInternalDiagnosticChannel(void)
{
    std_ReturnType stdResult = E_NOT_OK;

#if(INTERNAL_DIAG_CHANNEL==UART_INT_DIAG)
    cxComInternalChannelPtr = (UartHwUnits_t*)cxGetUartDriversPtr(INT_DIAG_HWUNIT);
#endif

    return stdResult;
}

/**
 * Name fnGetInternalDiagnosticChannel
 * @brief returns a pointer the structure of the driver
 * 
 * @param None
 * 
 * @return stdResult returns E_OK if hardware channel was
 * mapped correctly
 */
FUNC_P2VAR(void,AUTOMATIC) fnGetInternalDiagnosticChannel(void)
{
    return cxComInternalChannelPtr;
}

/**
 * Name stdfnInternalDiagnosticChannel_Send
 * @brief calls the mapped hardware chanel Write function
 * tp send the message
 * 
 * @param None
 * 
 * @return stdResult returns E_OK if hardware channel was
 * mapped correctly
 */
FUNC(std_ReturnType,AUTOMATIC) stdfnInternalDiagnosticChannel_Send(char_t* chQueueJobPtr)
{
    std_ReturnType stdResult = E_NOT_OK;

    if(NULLPTR != cxDiagChannels.cxInternalDiagChannel.cxInternalDiagnosticChannelPtr)
    {
        cxDiagChannels.cxInternalDiagChannel.cxInternalDiagnosticChannelPtr->Write(chQueueJobPtr);
        stdResult = E_OK;
    }
    else
    {
        stdResult = E_NOT_OK;
    }
    
    return stdResult;
}

/**
 * Name bGetTxConfirmationIntDiag
 * @brief Gets the status of the transmission in the lower layer
 * 
 * 
 * @param None
 * 
 * @return boolean returns TRUE if transmission is in course
 */
FUNC(boolean, AUTOMATIC) bGetTxConfirmationIntDiag()
{
    return bfnGetUartTxConfirmationFlag();
}