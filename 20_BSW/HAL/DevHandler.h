#ifndef DEV_HANDLER_H
#define DEV_HANDLER_H

#include "DevHandler_Types.h"


FUNC(void, AUTOMATIC)vfnDevHandler_Init();
FUNC(void,AUTOMATIC)vfnDevHandler_Main();



#endif