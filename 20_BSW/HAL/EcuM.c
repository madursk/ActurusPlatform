
#include "EcuM.h"
#include "UART.h"
#include "Port.h"
#include "I2C.h"
#include "NVIC.h"
#include "SIM.h"
#include "DiagM.h"

#define PRINT(x)     vfnDiagnosticConsole_Print(HAL,ECUM,x)

FUNC(void,AUTOMATIC)vfnEcuM_Init()
{
    uint8_t u8ErrCounter = 0;
    std_ReturnType sResult;
/* Port and Clocks configurations */
    sResult = stdfnSIM_Init();
    u8ErrCounter += (uint8_t)sResult;
    sResult = stdfnPort_Init();
    u8ErrCounter += (uint8_t)sResult;
    /*u8fnDMA_Init();*/
    if(0u != u8ErrCounter)
    {
        /*proceed no further since there is already an error
        an error in port or clock configuration will be fatal
        for the rest of the modules*/
        while(1);
    }

/*Communication and External diagnostic follows*/
    sResult = stdfnUART_Init();
    u8ErrCounter += (uint8_t)sResult;
    /*u8fnCAN_Init();*/
    //vfnI2C_Init();
    /*u8fnSPI_Init();*/
    /*u8fnETH_Init();*/
    /*u8fnSDHC_Init();*/
    /*u8fnUSB_Init();*/
    //vfnI2C_TestSend();
/*IO peripherals */
    /*u8fnADC();*/
    /*u8fnDIO();*/
    /*u8fnDAC();*/
    /*u8fnPDB();*/
    /*u8fnCMP();*/
    /*u8fnFTM();*/

/*Flash & memory drivers NOTE: this might be moved to first place TBD*/
    /*u8fnFMC();*/
    /*u8fnFTFE();*/

/*  MCU peripherals*/
    /*u8fnRCM_Init();*/
    /*u8fnAXBS_Init();*/
    /*u8fnRTC_Init();*/
    /*u8fnPMC_Init();*/
    /*u8fnSMC_Init();*/
    /*u8fnMCM_Init();*/

/*  Cryptography peripherals*/
    /*u8fnMMCAU_Init();*/
    /*u8fnRNGA_Init();*/
    /*u8fnCRC_Init();*/
    /*u8fnWDOG_Init();*/
    /*u8fnEWM_Init();*/

    vfnNVIC_Init();
}