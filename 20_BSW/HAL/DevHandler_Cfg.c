#include "DevHandler_Cfg.h"


DevHandler_Cfg_stPtr devices_at[MAX_NUMBER_DEVICES];
DevHandler_Cfg_st gkOled_t;
DevHandler_Cfg_st gkAcc_t;



FUNC(void,AUTOMATIC)vfnSetDevices()
{
    gkOled_t.device_t = OLED;
    gkOled_t.interface_t = I2CIf;
    gkOled_t.interfaceChannel_t = 0;

    devices_at[0] = &gkOled_t;
}