/***************************************************/
/* Startup.s
 *
 *  Created on: Dic 05, 2022
 *  Author: Madursk
 *  Brief: Startup file for the Core
 */
/************************************************/
    .text
    .code 16
    .syntax unified

    .global vectorInterruptTable
    .global Default_Handler

    .word _sdata
    .word _edata
    .word _sidata
    .word _eidata
    .word _sbss
    .word _ebss
    .word _estack

    .section .text.Reset_Handler
    .weak Reset_Handler
    .type Reset_Handler,%function
Reset_Handler:
  ldr   r0, =_estack
  mov   sp, r0          /* set stack pointer */
/* Copy the data segment initializers from flash to SRAM */
  ldr r0, =_sdata //loads _sdata memory address to R0
  ldr r1, =_edata //loads _edata memory address to R1
  ldr r2, =_sidata //loads _sidata memory address to R2
  movs r3, #0 //moves 0s to R3
  b LoopCopyDataInit //Branches to LoopCopyDataInit

CopyDataInit:
  ldr r4, [r2, r3]//load R4 with _sidata adding R3 offset (the first loop R3 has _sdata with 0s)
  str r4, [r0, r3]//store R4 in an address that is the sum of R0 (_sdata) plus R3 THIS COPIES THE MEMORY
  adds r3, r3, #4// adds to R3, R3 plus #4 moving the _sidata pointer

LoopCopyDataInit:
  adds r4, r0, r3 //adds to R0(_sdata first iteration) R3 and stores it in R4
  cmp r4, r1 //  compare the result in R4, substracting R1 from (_sdata firs iteration) R4 
  bcc CopyDataInit  //if carry flag is clear then R4 (_sdata value) is less than R1(_edata value) 
                    //branch to CopyDataInit otherwise continue to fill the bss with zeros.
/* Zero fill the bss segment. */
  ldr r2, =_sbss //loads _sbss to R2
  ldr r4, =_ebss //loads _ebss to R4
  movs r3, #0 //load R3 with zeros
  b LoopFillZerobss //ranch to LoopFillZeroBss

FillZerobss:
  str  r3, [r2] //store R3 in the address contained in R2
  adds r2, r2, #4 //moves to next address 

LoopFillZerobss:
  cmp r2, r4      //substract from R2 R4
  bcc FillZerobss //if Carry flag is clear branch to FillZeros
  ldr r0,=_estack
  msr psp, r0
  ISB
  mrs r0, CONTROL
  orr r0, r0, #0x2
  msr CONTROL, R0
  ISB
  b vfnFBL_Init

LoopForever:
    b LoopForever

  .section .text.Default_Handler,"ax",%progbits
Default_Handler:
Infinite_Loop:
  b Infinite_Loop
  .size Default_Handler, .-Default_Handler

.section .isr_vector,"a",%progbits
.type vectorInterruptTable, %object
.size vectorInterruptTable, .-vectorInterruptTable

vectorInterruptTable:
.word _estack
.word Reset_Handler
.word NMI_Handler
.word HardFault_Handler
.word MemManageFault_Handler
.word BusFault_Handler
.word UsageFault_Handler
.word 0
.word 0
.word 0
.word 0
.word SVC_Handler
.word DebugMon_Handler
.word 0
.word PendSRV_Handler
.word SysStick_Handler
.word DMA_Channel0_IRQHandler
.word DMA_Channel1_IRQHandler
.word DMA_Channel2_IRQHandler
.word DMA_Channel3_IRQHandler
.word DMA_Channel4_IRQHandler
.word DMA_Channel5_IRQHandler
.word DMA_Channel6_IRQHandler
.word DMA_Channel7_IRQHandler
.word DMA_Channel8_IRQHandler
.word DMA_Channel9_IRQHandler
.word DMA_Channel10_IRQHandler
.word DMA_Channel11_IRQHandler
.word DMA_Channel12_IRQHandler
.word DMA_Channel13_IRQHandler
.word DMA_Channel14_IRQHandler
.word DMA_Channel15_IRQHandler
.word DMA_Error_IRQHandler
.word MCM_IRQHandler
.word FLS_CC_IRQHandler
.word FLS_RC_IRQHandler
.word MODCON_LV_IRQHandler
.word LLWU_IRQHandler
.word WDG_IRQHandler
.word CRY_RNG_IRQHandler
.word I2C0_IRQHandler
.word I2C1_IRQHandler
.word SPI0_IRQHandler
.word SPI1_IRQHandler
.word I2S0_TX_IRQHandler
.word I2S0_RX_IRQHandler
.word 0
.word UART0_IRQHandler
.word UART0_Error_IRQHandler
.word UART1_IRQHandler
.word UART1_Error_IRQHandler
.word UART2_IRQHandler
.word UART2_Error_IRQHandler
.word UART3_IRQHandler
.word UART3_Error_IRQHandler
.word ADC0_IRQHandler
.word CMP0_IRQHandler
.word CMP1_IRQHandler
.word FTM0_IRQHandler
.word FTM1_IRQHandler
.word FTM2_IRQHandler
.word CMT_IRQHandler
.word RTC_Alarm_IRQHanlder
.word RTC_Sec_IRQHandler
.word PIT0_IRQHandler
.word PIT1_IRQHandler
.word PIT2_IRQHandler
.word PIT3_IRQHandler
.word PTB_IRQHandler
.word USB_OTG_IRQHandler
.word USB_CHRG_IRQHandler
.word 0
.word DAC0_IRQHandler
.word MCG_IRQHandler
.word LP_TIMER_IRQHandler
.word PCMA_IRQHandler
.word PCMB_IRQHandler
.word PCMC_IRQHandler
.word PCMD_IRQHandler
.word PCME_IRQHandler
.word SW_IRQHandler
.word SPI2_IRQHandler
.word UART4_IRQHandler
.word UART4_Error_IRQHandler
.word UART5_IRQHandler
.word UART5_Error_IRQHandler
.word CMP2_IRQHandler
.word FTM3_IRQHandler
.word DAC1_IRQHandler
.word ADC1_IRQHandler
.word I2C2_IRQHandler
.word CAN0_BUFF_IRQHandler
.word CAN0_BUSOFF_IRQHandler
.word CAN0_Error_IRQHandler
.word CAN0_TXW_IRQHandler
.word CAN0_RXW_IRQHandler
.word CAN0_WU_IRQHandler
.word SDHC_IRQHandler
.word ETH_TMR_IRQHandler
.word ETH_TX_IRQHandler
.word ETH_RX_IRQHandler
.word ETH_Error_IRQHandler

    .weak DMA_Channel0_IRQHandler
    .thumb_set DMA_Channel0_IRQHandler,Default_Handler

    .weak DMA_Channel1_IRQHandler
    .thumb_set DMA_Channel1_IRQHandler,Default_Handler

    .weak DMA_Channel2_IRQHandler
    .thumb_set DMA_Channel2_IRQHandler,Default_Handler

    .weak DMA_Channel3_IRQHandler
    .thumb_set DMA_Channel3_IRQHandler,Default_Handler

    .weak DMA_Channel4_IRQHandler
    .thumb_set DMA_Channel4_IRQHandler,Default_Handler

    .weak DMA_Channel5_IRQHandler
    .thumb_set DMA_Channel5_IRQHandler,Default_Handler

    .weak DMA_Channel6_IRQHandler
    .thumb_set DMA_Channel6_IRQHandler,Default_Handler

    .weak DMA_Channel7_IRQHandler
    .thumb_set DMA_Channel7_IRQHandler,Default_Handler

    .weak DMA_Channel8_IRQHandler
    .thumb_set DMA_Channel8_IRQHandler,Default_Handler

    .weak DMA_Channel9_IRQHandler
    .thumb_set DMA_Channel9_IRQHandler,Default_Handler

    .weak DMA_Channel10_IRQHandler
    .thumb_set DMA_Channel10_IRQHandler,Default_Handler

    .weak DMA_Channel11_IRQHandler
    .thumb_set DMA_Channel11_IRQHandler,Default_Handler
    
    .weak DMA_Channel12_IRQHandler
    .thumb_set DMA_Channel12_IRQHandler,Default_Handler
    
    .weak DMA_Channel13_IRQHandler
    .thumb_set DMA_Channel13_IRQHandler,Default_Handler
    
    .weak DMA_Channel14_IRQHandler
    .thumb_set DMA_Channel14_IRQHandler,Default_Handler
    
    .weak DMA_Channel15_IRQHandler
    .thumb_set DMA_Channel15_IRQHandler,Default_Handler
    
    .weak DMA_Error_IRQHandler
    .thumb_set DMA_Error_IRQHandler,Default_Handler

    .weak MCM_IRQHandler
    .thumb_set MCM_IRQHandler,Default_Handler

    .weak FLS_CC_IRQHandler
    .thumb_set FLS_CC_IRQHandler,Default_Handler

    .weak FLS_RC_IRQHandler
    .thumb_set FLS_RC_IRQHandler,Default_Handler

    .weak MODCON_LV_IRQHandler
    .thumb_set MODCON_LV_IRQHandler,Default_Handler

    .weak LLWU_IRQHandler
    .thumb_set LLWU_IRQHandler,Default_Handler

    .weak WDG_IRQHandler
    .thumb_set WDG_IRQHandler,Default_Handler
    
    .weak CRY_RNG_IRQHandler
    .thumb_set CRY_RNG_IRQHandler,Default_Handler

    .weak I2C1_IRQHandler
    .thumb_set I2C1_IRQHandler,Default_Handler

    .weak SPI0_IRQHandler
    .thumb_set SPI0_IRQHandler,Default_Handler

    .weak SPI1_IRQHandler
    .thumb_set SPI1_IRQHandler,Default_Handler

    .weak I2S0_TX_IRQHandler
    .thumb_set I2S0_TX_IRQHandler,Default_Handler

    .weak I2S0_RX_IRQHandler
    .thumb_set I2S0_RX_IRQHandler,Default_Handler

    .weak UART0_IRQHandler
    .thumb_set UART0_IRQHandler,Default_Handler

    .weak UART0_Error_IRQHandler
    .thumb_set UART0_Error_IRQHandler,Default_Handler

    .weak UART1_IRQHandler
    .thumb_set UART1_IRQHandler,Default_Handler

    .weak UART1_Error_IRQHandler
    .thumb_set UART1_Error_IRQHandler,Default_Handler

    .weak UART2_IRQHandler
    .thumb_set UART2_IRQHandler,Default_Handler

    .weak UART2_Error_IRQHandler
    .thumb_set UART2_Error_IRQHandler,Default_Handler

    .weak UART3_Error_IRQHandler
    .thumb_set UART3_Error_IRQHandler,Default_Handler

    .weak ADC0_IRQHandler
    .thumb_set ADC0_IRQHandler,Default_Handler

    .weak CMP0_IRQHandler
    .thumb_set CMP0_IRQHandler,Default_Handler

    .weak CMP1_IRQHandler
    .thumb_set CMP1_IRQHandler,Default_Handler

    .weak FTM0_IRQHandler
    .thumb_set FTM0_IRQHandler,Default_Handler

    .weak FTM1_IRQHandler
    .thumb_set FTM1_IRQHandler,Default_Handler

    .weak FTM2_IRQHandler
    .thumb_set FTM2_IRQHandler,Default_Handler

    .weak CMT_IRQHandler
    .thumb_set CMT_IRQHandler,Default_Handler

    .weak RTC_Alarm_IRQHanlder
    .thumb_set RTC_Alarm_IRQHanlder,Default_Handler

    .weak RTC_Sec_IRQHandler
    .thumb_set RTC_Sec_IRQHandler,Default_Handler

    .weak PIT0_IRQHandler
    .thumb_set PIT0_IRQHandler,Default_Handler

    .weak PIT1_IRQHandler
    .thumb_set PIT1_IRQHandler,Default_Handler

    .weak PIT2_IRQHandler
    .thumb_set PIT2_IRQHandler,Default_Handler

    .weak PIT3_IRQHandler
    .thumb_set PIT3_IRQHandler,Default_Handler

    .weak PTB_IRQHandler
    .thumb_set PTB_IRQHandler,Default_Handler

    .weak USB_OTG_IRQHandler
    .thumb_set USB_OTG_IRQHandler,Default_Handler

    .weak USB_CHRG_IRQHandler
    .thumb_set USB_CHRG_IRQHandler,Default_Handler

    .weak DAC0_IRQHandler
    .thumb_set DAC0_IRQHandler,Default_Handler

    .weak MCG_IRQHandler
    .thumb_set MCG_IRQHandler,Default_Handler

    .weak LP_TIMER_IRQHandler
    .thumb_set LP_TIMER_IRQHandler,Default_Handler

    .weak PCMA_IRQHandler
    .thumb_set PCMA_IRQHandler,Default_Handler

    .weak PCMB_IRQHandler
    .thumb_set PCMB_IRQHandler,Default_Handler

    .weak PCMC_IRQHandler
    .thumb_set PCMC_IRQHandler,Default_Handler

    .weak PCMD_IRQHandler
    .thumb_set PCMD_IRQHandler,Default_Handler

    .weak PCME_IRQHandler
    .thumb_set PCME_IRQHandler,Default_Handler

    .weak SW_IRQHandler
    .thumb_set SW_IRQHandler,Default_Handler

    .weak SPI2_IRQHandler
    .thumb_set SPI2_IRQHandler,Default_Handler

    .weak UART4_IRQHandler
    .thumb_set UART4_IRQHandler,Default_Handler

    .weak UART4_Error_IRQHandler
    .thumb_set UART4_Error_IRQHandler,Default_Handler

    .weak UART5_IRQHandler
    .thumb_set UART5_IRQHandler,Default_Handler

    .weak UART5_Error_IRQHandler
    .thumb_set UART5_Error_IRQHandler,Default_Handler

    .weak CMP2_IRQHandler
    .thumb_set CMP2_IRQHandler,Default_Handler

    .weak FTM3_IRQHandler
    .thumb_set FTM3_IRQHandler,Default_Handler

    .weak DAC1_IRQHandler
    .thumb_set DAC1_IRQHandler,Default_Handler

    .weak ADC1_IRQHandler
    .thumb_set ADC1_IRQHandler,Default_Handler

    .weak I2C2_IRQHandler
    .thumb_set I2C2_IRQHandler,Default_Handler

    .weak CAN0_BUFF_IRQHandler
    .thumb_set CAN0_BUFF_IRQHandler,Default_Handler

    .weak CAN0_BUSOFF_IRQHandler
    .thumb_set CAN0_BUSOFF_IRQHandler,Default_Handler

    .weak CAN0_Error_IRQHandler
    .thumb_set CAN0_Error_IRQHandler,Default_Handler

    .weak CAN0_TXW_IRQHandler
    .thumb_set CAN0_TXW_IRQHandler,Default_Handler

    .weak CAN0_RXW_IRQHandler
    .thumb_set CAN0_RXW_IRQHandler,Default_Handler

    .weak CAN0_WU_IRQHandler
    .thumb_set CAN0_WU_IRQHandler,Default_Handler

    .weak SDHC_IRQHandler
    .thumb_set SDHC_IRQHandler,Default_Handler

    .weak ETH_TMR_IRQHandler
    .thumb_set ETH_TMR_IRQHandler,Default_Handler

    .weak ETH_TX_IRQHandler
    .thumb_set ETH_TX_IRQHandler,Default_Handler

    .weak ETH_RX_IRQHandler
    .thumb_set ETH_RX_IRQHandler,Default_Handler

    .weak ETH_Error_IRQHandler
    .thumb_set ETH_Error_IRQHandler,Default_Handler

    .weak I2C0_IRQHandler
    .thumb_set I2C0_IRQHandler,Default_Handler

    .weak UART3_IRQHandler
    .thumb_set UART3_IRQHandler,Default_Handler