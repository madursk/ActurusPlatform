#ifndef PROJECT_DEFINITIONS_H
#define PROJECT_DEFINITIONS_H


/***************************************************** HARDWARE CONFIGURATION ************************************************************************************************/
#define ARCTURUS_PLATFORM 0x10
#define FRDM_K64F_PLATFORM 0x20

#define HARDWARE_PLATFORM ARCTURUS_PLATFORM
/************************************************************************************************************************************************************************/

/***************************************************** FBL DIAG CONFIGURATION ************************************************************************************************/
#define RPG_REQUEST 0xCCu
#define APPL_VALID 0xBBu
#define DIAG_SESS 0xEAu
/*****************************************************************************************************************************************************************************/

/***************************************************** PORT CONFIGURATION ************************************************************************************************/
#define PORTA STD_OFF
#define PORTB STD_OFF
#define PORTC STD_ON
#define PORTD STD_OFF
#define PORTE STD_ON
#define MAX_HWPORT_NUM 5u
/************************************************************************************************************************************************************************/

/***************************************************** UART CONFIGURATION ************************************************************************************************/
#define UART0 STD_OFF
#define UART1 STD_OFF
#define UART2 STD_OFF
#define UART3 STD_ON
#define UART4 STD_OFF
#define UART5 STD_OFF
#define MAX_HWUART_NUM 6u
/************************************************************************************************************************************************************************/
/***************************************************** SIM  CONFIGURATION ************************************************************************************************/
#define SOPT1_CFG   STD_OFF
#define SOPT2_CFG   STD_ON
#define SOPT4_CFG   STD_OFF
#define SOPT5_CFG   STD_OFF
#define SOPT7_CFG   STD_OFF
#define SOPT1CG_CFG STD_OFF
#define SCGC1_CFG   STD_OFF
#define SCGC2_CFG   STD_OFF
#define SCGC3_CFG   STD_OFF
#define SCGC4_CFG   STD_ON
#define SCGC5_CFG   STD_ON
#define SCGC6_CFG   STD_OFF
#define SCGC7_CFG   STD_OFF
#define SIDD_API    STD_OFF
#define CLKDIV1_CFG STD_ON
#define CLKDIV2_CFG STD_OFF
#define FCFG1_CFG   STD_OFF
#define FCFG2_CFG   STD_OFF
#define UIDH_CFG    STD_OFF
#define UIDMH_CFG   STD_OFF
#define UIDML_CFG   STD_OFF
#define UIDL_CFG    STD_OFF
/************************************************************************************************************************************************************************/

/***************************************************************Hardware Abstraction Layer*******************************************************************************/
/*
*
*
*
*
*
*
*
*/
/************************************************************************************************************************************************************************/

/***************************************************** DiagHandler ******************************************************************************************************/
#define UART_INT_DIAG 1u
#define INTERNAL_DIAG_CHANNEL UART_INT_DIAG
#define INT_DIAG_UART3
#define INT_DIAG_MSG_BUFFER (64u)
#define INT_QUEUE_SIZE (1024u)
#define MAX_NUM_INT_INSERTIONS (INT_QUEUE_SIZE/INT_DIAG_MSG_BUFFER)
/************************************************************************************************************************************************************************/


#endif