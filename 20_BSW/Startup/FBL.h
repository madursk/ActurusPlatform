/***************************************************/
/* FBL.h
 *
 *  Created on: Dic 05, 2022
 *  Author: Madursk
 *  Brief: FBL header
 */
/************************************************/
#ifndef FBL_H_
#define FBL_H_
#include "Datatypes.h"
#include "Compiler.h"
#include "Compiler_cfg.h"
#include "ProjectDefinitions.h"

typedef struct
{
    uint8_t u8SID;
    uint8_t u8Resp;
    uint8_t u8AdditionalParam;
    uint8_t u8AdditionalParam2;
}UdsServiceResp_t;

#define UART_FBL_BUFFER_LENGTH   (40u)
#define UART_END_TRANSMISSION (13u) /*You can change this to the end you desire */
#define UART_END_RECEPTION (13u)


FUNC(void,FBL) vfnFBL_Init(void);
FUNC(void,FBL) vfnFBL_LlUARTConsoleWrite(char_t s8Message[]);
FUNC(void,FBL) vfnFBL_LlUARTUdsWrite(UdsServiceResp_t* cxudsResponsePtr);
FUNC(void,FBL) vfnFBL_LlUARTRead(char_t s8Message[], uint8_t u8RxBufferSize);
extern int main(void);
#endif /* FBL_H_ */
