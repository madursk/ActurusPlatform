#ifndef COMPILER_CFG_HEADER_H
#define COMPILER_CFG_HEADER_H

#define DEFAULT
#define STACK_VAR __attribute__((section(".stack")))
#define AUTOMATIC __attribute__((section(".text")))
#define FBL __attribute__((section(".fblText")))
#define FBLFLGS __attribute__((section(".fblFlSec")))
#define OS_CODE_INTVECT __attribute__((section(".os_code_intvector")))
#define OS_TASK_INIT_TCB __attribute__((section(".TaskInitTCB")))
#define OS_TASK_BSW_TCB __attribute__((section(".TaskBswTCB")))
#define OS_TASK_APP_TCB __attribute__((section(".TaskAppTCB")))
#define OS_TASK_IDLE_TCB __attribute__((section(".TaskIdleTCB")))
#endif