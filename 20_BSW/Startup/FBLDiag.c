/***************************************************/
/* FBLDiag.c
 *
 *  Created on: Dic 05, 2022
 *  Author: Madursk
 *  Brief: FBL Diagnostic services
 */
/************************************************/
#include "FBL.h"

/*******************************************************************Structs, Enums, bitfields*****************************************************/
typedef struct
{
    char_t  s8BootloaderV[11];
    char_t s8MainVersion[1];
    char_t s8Subversion[1];
    uint8_t RESERVED[4];
}FblDIDVersion_t;
/***********************************************************************************************************************************************/

/*******************************************************************Function Declarations*******************************************************/
extern inline FUNC(void,FBL) vfnFBLDiag_Main(void);
FUNC(void,FBL) vfnFBLDiag_CheckForCmd(void);
/**********************************************************************************************************************************************/

/*******************************************************************Macro Definitions**********************************************************/

/**********************************************************************************************************************************************/

/*******************************************************************Variable Declarations*****************************************************/
VAR(FblDIDVersion_t,FBLFLGS) cxdidF180_Data;
/*********************************************************************************************************************************************/

/*******************************************************************START CODE****************************************************************/


/**
 * Name vfnFBLDiag_Main
 * @brief Main function to process diagnostic requests
 *
 * 
 * @param None
 * 
 * @return None, inline function
 */
inline FUNC(void,FBL) vfnFBLDiag_Main(void)
{
    vfnFBLDiag_CheckForCmd();
    

}

/**
 * Name vfnFBLDiag_CheckForCmd
 * @brief Check if something has been received in the UART buffer,
 * depending on the command it will process the diagnostic request
 * and send an answer.
 *
 * 
 * @param None
 * 
 * @return None
 */
FUNC(void,FBL) vfnFBLDiag_CheckForCmd(void)
{
    char_t s8UartRawRxBuffer[UART_FBL_BUFFER_LENGTH];
    UdsServiceResp_t cxResponseBuffer;

    vfnFBL_LlUARTRead(s8UartRawRxBuffer,UART_FBL_BUFFER_LENGTH);
    switch ((uint8_t)s8UartRawRxBuffer[0])
    {
        case((uint8_t)0x10u):
        {
            /*Diagnostic Session Control*/
            if(((uint8_t)0x01u)==((uint8_t)s8UartRawRxBuffer[1]))
            {
                /* validity of application has to be checked*/
                /*if yes then send positive response before jump to appl*/
                cxResponseBuffer.u8Resp = 0x40;
                cxResponseBuffer.u8SID = 0x50;
                cxResponseBuffer.u8AdditionalParam = 0x02;
                cxResponseBuffer.u8AdditionalParam2 = 0x00;
                vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
                __asm__ volatile("B main"::);    /*Jump to application*/
                
            }
            else if(((uint8_t)0x02u)==((uint8_t)s8UartRawRxBuffer[1]))
            {
                /*We are already in programming session send positive response*/
                cxResponseBuffer.u8Resp = 0x40;
                cxResponseBuffer.u8SID = 0x50;
                cxResponseBuffer.u8AdditionalParam = 0x02;
                cxResponseBuffer.u8AdditionalParam2 = 0x00;
                vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
            }
            else if(((uint8_t)0x03u)==((uint8_t)s8UartRawRxBuffer[1]))
            {
                /*Extended diagnostic sessionn is not possible in programming session, always send negative*/
                cxResponseBuffer.u8Resp = 0x7F;
                cxResponseBuffer.u8SID = 0x10;
                cxResponseBuffer.u8AdditionalParam = 0x03;
                cxResponseBuffer.u8AdditionalParam2 = 0x22;
                vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
            }
        }break;

        case((uint8_t)0x11):
        {
            /*ECU Reset*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x51;
            cxResponseBuffer.u8AdditionalParam = 0x01;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;

        case((uint8_t)0x14):
        {
            /*Clear Diagnostic information*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x14;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;

        case((uint8_t)0x19):
        {
            /*Read DTC information*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x19;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;

        case((uint8_t)0x22):
        {
            /*Read data by identifier*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x22;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;

        case((uint8_t)0x23):
        {
            /*Read memory by address*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x23;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;

        case((uint8_t)0x27):
        {
            /*Security Access*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x27;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;

        case((uint8_t)0x28):
        {
            /*Communication control*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x28;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;

        case((uint8_t)0x2A):
        {
            /*Read Data by periodic ID*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x2A;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;
    
        case((uint8_t)0x2E):
        {
            /*Write data by identifier*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x2E;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;

        case((uint8_t)0x2F):
        {
            /*IO control*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x2F;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;

        case((uint8_t)0x31):
        {
            /*Routine Control*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x31;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;

        case((uint8_t)0x34):
        {
            /*Request Download*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x34;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;
        case((uint8_t)0x35):
        {
            /*Request upload*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x35;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;
        case((uint8_t)0x36):
        {
            /*Transfer Data*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x36;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;
        case((uint8_t)0x37):
        {
            /*Transfer Exit*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x36;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;
        case((uint8_t)0x3D):
        {
            /*Write memory by address*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x3D;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;
        case((uint8_t)0x3E):
        {
            /*Tester present*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x3E;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;
        case((uint8_t)0x85):
        {
            /*Control DTC setting*/
            cxResponseBuffer.u8Resp = 0x7F;
            cxResponseBuffer.u8SID = 0x85;
            cxResponseBuffer.u8AdditionalParam = 0x00;
            cxResponseBuffer.u8AdditionalParam2 = 0x12;
            vfnFBL_LlUARTUdsWrite(&cxResponseBuffer);
        }break;

        default:
        break;
    }

}
/*********************************************************************************************************************************************/