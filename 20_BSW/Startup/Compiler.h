#ifndef COMPILER_HEADER_H
#define COMPILER_HEADER_H

#define FUNC(rettype,memclass) memclass rettype
#define FUNC_P2CONST(rettype)const ptrclass rettype*
#define FUNC_P2VAR(rettype,memclass)memclass rettype*

#define P2VAR(ptrtype,memclass)memclass ptrtype*
#define P2CONST(ptrtype)const ptrtype
#define CONSTP2VAR(ptrtype)ptrtype * const
#define CONSTP2CONST(ptrtype)const ptrtype* const
#define P2FUNC(rettype,fctname) rettype(*fctname)

#define CONST(consttype)const consttype
#define VAR(vartype,memclass)memclass vartype

#endif