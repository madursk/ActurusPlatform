/***************************************************/
/* FBL.c
 *
 *  Created on: Dic 05, 2022
 *  Author: Madursk
 *  Brief: FBL Main file
 */
/************************************************/

#include "FBL.h"
#include "Os_Hal_Isr_cfg.h"
#include "Os_tasks.h"
/*******************************************************************Structs, Enums, bitfields*****************************************************/
/** SCB - Peripheral register structure to access VTOR*/
typedef struct SCB_MemMap {
  uint8_t RESERVED_0[8];
  uint32_t ACTLR;                                  /**< Auxiliary Control Register,, offset: 0x8 */
  uint8_t RESERVED_1[3316];
  uint32_t CPUID;                                  /**< CPUID Base Register, offset: 0xD00 */
  uint32_t ICSR;                                   /**< Interrupt Control and State Register, offset: 0xD04 */
  uint32_t VTOR;                                   /**< Vector Table Offset Register, offset: 0xD08 */
  uint32_t AIRCR;                                  /**< Application Interrupt and Reset Control Register, offset: 0xD0C */
  uint32_t SCR;                                    /**< System Control Register, offset: 0xD10 */
  uint32_t CCR;                                    /**< Configuration and Control Register, offset: 0xD14 */
  uint32_t SHPR1;                                  /**< System Handler Priority Register 1, offset: 0xD18 */
  uint32_t SHPR2;                                  /**< System Handler Priority Register 2, offset: 0xD1C */
  uint32_t SHPR3;                                  /**< System Handler Priority Register 3, offset: 0xD20 */
  uint32_t SHCSR;                                  /**< System Handler Control and State Register, offset: 0xD24 */
  uint32_t CFSR;                                   /**< Configurable Fault Status Registers, offset: 0xD28 */
  uint32_t HFSR;                                   /**< HardFault Status register, offset: 0xD2C */
  uint32_t DFSR;                                   /**< Debug Fault Status Register, offset: 0xD30 */
  uint32_t MMFAR;                                  /**< MemManage Address Register, offset: 0xD34 */
  uint32_t BFAR;                                   /**< BusFault Address Register, offset: 0xD38 */
  uint32_t AFSR;                                   /**< Auxiliary Fault Status Register, offset: 0xD3C */
  uint8_t RESERVED_2[72];
  uint32_t CPACR;                                  /**< Debug Fault Status Register, offset: 0xD88 */
} volatile *SCB_MemMapPtr;

typedef struct
{
    uint8_t u8ReprogrammingFlag;
    uint8_t u8ApplValidProgramFlag;
    uint8_t u8DiagnosticSessionCtrl;
    uint8_t RESERVED;

}FblConditions_t;

/***********************************************************************************************************************************************/

/*******************************************************************Function Declarations********************************************************/
extern inline FUNC(void,FBL) vfnFBL_Main(void);
extern inline FUNC(void,FBL) vfnFBL_LlInit(void);
extern inline FUNC(void,FBL) vfnFBL_LlDeInit(void);
extern inline FUNC(void,FBL) vfnFBLDiag_Main(void);
/**********************************************************************************************************************************************/

/*******************************************************************Macro Definitions**********************************************************/
#define SCB_VTOR_REG(base)                       ((base)->VTOR)
#define SystemControl_BASE_PTR                   ((SCB_MemMapPtr)0xE000E000u)

/* SCB - VTOR Register Macro */
#define SCB_VTOR                                 SCB_VTOR_REG(SystemControl_BASE_PTR)

/*********************************************************************************************************************************************/

/*******************************************************************Variable Declarations*****************************************************/

extern uint32_t _sOsCodeIntVector; /*!< Linker symbol of the RAM Interrupt Vector Table location */
extern volatile uint32_t _taskInitStart;
extern volatile uint32_t _taskBswStart;
extern volatile uint32_t _taskApplStart;
extern volatile uint32_t _taskIdleStart;
extern volatile uint32_t* taskInitStart = &_taskInitStart;
extern volatile uint32_t* taskBswStart = &_taskBswStart;
extern volatile uint32_t* taskApplStart = &_taskApplStart;
extern volatile uint32_t* taskIdleStart = &_taskIdleStart;
extern volatile TaskControlBlock TCB_initTsk OS_TASK_INIT_TCB;
extern volatile TaskControlBlock TCB_bswTsk OS_TASK_BSW_TCB;
extern volatile TaskControlBlock TCB_ApplTsk OS_TASK_APP_TCB;
extern volatile TaskControlBlock TCB_IdleTsk OS_TASK_IDLE_TCB;
VAR(FblConditions_t,FBLFLGS) cxPgmConditionsFlags; /*!< Variable of the FBL specific flags which are going to be read from memory */

/*********************************************************************************************************************************************/

/*******************************************************************START CODE****************************************************************/
/**
 * Name vfnFBL_Init
 * @brief Initialize the bootloader coming after a reset, reads programming condition flags to know if there is reprogram request,
 * a diagnostic session request or just a reset.
 *
 * 
 * @param None
 * 
 * @return None
 */
FUNC(void,FBL) vfnFBL_Init(void)
{
    /*START of initialization of inital stack frames of tasks*/ 
    __asm__ volatile("MOV r3, sp"::); /*Store stack pointer before manipulating tasks stack pointers*/
    /*Task Init stack frame*/
    __asm__ volatile("LDR r0,=taskInitStart"::);
    __asm__ volatile("LDR r1, [r0]"::);
    __asm__ volatile("LDR r2,=TASK_INIT"::);
    __asm__ volatile("MOV r7, r1"::);
    __asm__ volatile("MOV sp, r1"::);
    __asm__ volatile("MOV lr, r2"::);
    __asm__ volatile("STMDB sp!, {r4-r11, r14}"::);
    __asm__ volatile("STR sp, [r0]"::);
    TCB_initTsk.StackTop = taskInitStart;
    /*Task Bsw stack frame*/
    __asm__ volatile("LDR r0,=taskBswStart"::);
    __asm__ volatile("LDR r1, [r0]"::);
    __asm__ volatile("LDR r2,=TASK_BSW"::);
    __asm__ volatile("MOV r7, r1"::);
    __asm__ volatile("MOV sp, r1"::);
    __asm__ volatile("MOV lr, r2"::);
    __asm__ volatile("STMDB sp!, {r4-r11, r14}"::);
    __asm__ volatile("STR sp, [r0]"::);
    TCB_bswTsk.StackTop = taskBswStart;
    /*Task Appl stack frame*/
    __asm__ volatile("LDR r0,=taskApplStart"::);
    __asm__ volatile("LDR r1, [r0]"::);
    __asm__ volatile("LDR r2,=TASK_APPL"::);
    __asm__ volatile("MOV r7, r1"::);
    __asm__ volatile("MOV sp, r1"::);
    __asm__ volatile("MOV lr, r2"::);
    __asm__ volatile("STMDB sp!, {r4-r11, r14}"::);
    __asm__ volatile("STR sp, [r0]"::);
    TCB_ApplTsk.StackTop = taskApplStart;
    /*Task Idle stakc frame*/
    __asm__ volatile("LDR r0,=taskIdleStart"::);
    __asm__ volatile("LDR r1, [r0]"::);
    __asm__ volatile("LDR r2,=TASK_IDLE"::);
    __asm__ volatile("MOV r7, r1"::);
    __asm__ volatile("MOV sp, r1"::);
    __asm__ volatile("MOV lr, r2"::);
    __asm__ volatile("STMDB sp!, {r4-r11, r14}"::);
    __asm__ volatile("STR sp, [r0]"::);
    TCB_IdleTsk.StackTop = taskIdleStart;
    /*END of initialization of inital stack frames of tasks*/ 
    __asm__ volatile("MOV sp, r3"::);
    /*Relocate vector table*/
    SCB_VTOR = (uint32_t*)&_sOsCodeIntVector;
    /*Preinitialize Harware*/
    vfnFBL_LlInit();
     /*Go to main function of the FBL*/
    vfnFBL_Main();

}

/**
 * Name vfnFBL_Main
 * @brief Main Function of the Bootloader waits for commands if its in diagnostic session
 * otherwise just jump to application, other states are not implemented TBD
 *
 * 
 * @param None
 * 
 * @return None, inline function
 */
inline FUNC(void,FBL) vfnFBL_Main(void)
{
    while(1)
    {
        if((DIAG_SESS == cxPgmConditionsFlags.u8DiagnosticSessionCtrl) && (RPG_REQUEST != cxPgmConditionsFlags.u8DiagnosticSessionCtrl))
        {
            vfnFBLDiag_Main();
        }
        else if((DIAG_SESS != cxPgmConditionsFlags.u8DiagnosticSessionCtrl) && (RPG_REQUEST == cxPgmConditionsFlags.u8DiagnosticSessionCtrl))
        {
            /*To Be Implemented*/
        }
        else
        {
            /*To Be Implemented*/
            vfnFBL_LlDeInit();
            __asm__ volatile("B main"::);    /*Jump to application*/
        }
    }
}

/*********************************************************************************************************************************************/