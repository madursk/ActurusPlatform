/***************************************************/
/* FBLHw.c
 *
 *  Created on: Dic 05, 2022
 *  Author: Madursk
 *  Brief: FBL Hardware preinitializations
 */
/************************************************/

#include "FBL.h"

/*******************************************************************Structs, Enums, bitfields*****************************************************/
typedef struct UARTMemMap
{
    uint8_t BDH;
    uint8_t BDL;
    uint8_t C1;
    uint8_t C2;
    uint8_t S1;
    uint8_t S2;
    uint8_t C3;
    uint8_t D;
    uint8_t MA1;
    uint8_t MA2;
    uint8_t C4;
    uint8_t C5;
    uint8_t ED;
    uint8_t MODEM;
    uint8_t IR;
    uint8_t RESERVED_0[1];
    uint8_t PFIFO;
    uint8_t CFIFO;
    uint8_t SFIFO;
    uint8_t TWFIFO;
    uint8_t TCFIFO;
    uint8_t RWFIFO;
    uint8_t RCFIFO;
    uint8_t RESERVED_1[1];
    uint8_t C7816;
    uint8_t IE7816;
    uint8_t IS7816;
    union
    {
    uint8_t WP7816T0;
    uint8_t WP7816T1;
    };
    
    uint8_t WN7816;
    uint8_t WF7816;
    uint8_t ET7826;
    uint8_t TL7816;
}volatile *UARTMemMapPtr;


/** NVIC - Peripheral register structure */
typedef struct NVIC_MemMap {
  uint32_t ISER[4];                                /**< Interrupt Set Enable Register n, array offset: 0x0, array step: 0x4 */
  uint8_t RESERVED_0[112];
  uint32_t ICER[4];                                /**< Interrupt Clear Enable Register n, array offset: 0x80, array step: 0x4 */
  uint8_t RESERVED_1[112];
  uint32_t ISPR[4];                                /**< Interrupt Set Pending Register n, array offset: 0x100, array step: 0x4 */
  uint8_t RESERVED_2[112];
  uint32_t ICPR[4];                                /**< Interrupt Clear Pending Register n, array offset: 0x180, array step: 0x4 */
  uint8_t RESERVED_3[112];
  uint32_t IABR[4];                                /**< Interrupt Active bit Register n, array offset: 0x200, array step: 0x4 */
  uint8_t RESERVED_4[240];
  uint8_t IP[106];                                 /**< Interrupt Priority Register n, array offset: 0x300, array step: 0x1 */
  uint8_t RESERVED_5[2710];
  uint32_t STIR[1];                                /**< Software Trigger Interrupt Register, array offset: 0xE00, array step: 0x4 */
} volatile *NVIC_MemMapPtr;



typedef struct PORT_MemMap {
  uint32_t PCR[32];                                /**< Pin Control Register n, array offset: 0x0, array step: 0x4 */
  uint32_t GPCLR;                                  /**< Global Pin Control Low Register, offset: 0x80 */
  uint32_t GPCHR;                                  /**< Global Pin Control High Register, offset: 0x84 */
  uint8_t RESERVED_0[24];
  uint32_t ISFR;                                   /**< Interrupt Status Flag Register, offset: 0xA0 */
  uint8_t RESERVED_1[28];
  uint32_t DFER;                                   /**< Digital Filter Enable Register, offset: 0xC0 */
  uint32_t DFCR;                                   /**< Digital Filter Clock Register, offset: 0xC4 */
  uint32_t DFWR;                                   /**< Digital Filter Width Register, offset: 0xC8 */
} volatile *PORT_MemMapPtr;



typedef struct SIM_MemMap {
  uint32_t SOPT1;                                  /**< System Options Register 1, offset: 0x0 */
  uint32_t SOPT1CFG;                               /**< SOPT1 Configuration Register, offset: 0x4 */
  uint8_t RESERVED_0[4092];
  uint32_t SOPT2;                                  /**< System Options Register 2, offset: 0x1004 */
  uint8_t RESERVED_1[4];
  uint32_t SOPT4;                                  /**< System Options Register 4, offset: 0x100C */
  uint32_t SOPT5;                                  /**< System Options Register 5, offset: 0x1010 */
  uint8_t RESERVED_2[4];
  uint32_t SOPT7;                                  /**< System Options Register 7, offset: 0x1018 */
  uint8_t RESERVED_3[8];
  uint32_t SDID;                                   /**< System Device Identification Register, offset: 0x1024 */
  uint32_t SCGC1;                                  /**< System Clock Gating Control Register 1, offset: 0x1028 */
  uint32_t SCGC2;                                  /**< System Clock Gating Control Register 2, offset: 0x102C */
  uint32_t SCGC3;                                  /**< System Clock Gating Control Register 3, offset: 0x1030 */
  uint32_t SCGC4;                                  /**< System Clock Gating Control Register 4, offset: 0x1034 */
  uint32_t SCGC5;                                  /**< System Clock Gating Control Register 5, offset: 0x1038 */
  uint32_t SCGC6;                                  /**< System Clock Gating Control Register 6, offset: 0x103C */
  uint32_t SCGC7;                                  /**< System Clock Gating Control Register 7, offset: 0x1040 */
  uint32_t CLKDIV1;                                /**< System Clock Divider Register 1, offset: 0x1044 */
  uint32_t CLKDIV2;                                /**< System Clock Divider Register 2, offset: 0x1048 */
  uint32_t FCFG1;                                  /**< Flash Configuration Register 1, offset: 0x104C */
  uint32_t FCFG2;                                  /**< Flash Configuration Register 2, offset: 0x1050 */
  uint32_t UIDH;                                   /**< Unique Identification Register High, offset: 0x1054 */
  uint32_t UIDMH;                                  /**< Unique Identification Register Mid-High, offset: 0x1058 */
  uint32_t UIDML;                                  /**< Unique Identification Register Mid Low, offset: 0x105C */
  uint32_t UIDL;                                   /**< Unique Identification Register Low, offset: 0x1060 */
} volatile *SIM_MemMapPtr;



typedef struct 
 {
    uint8_t u8TransmissionMode     :1;
    uint8_t u8ReceptionMode        :1;
    uint8_t                        :6;
   
 }UARTCommMode_t;
/***********************************************************************************************************************************************/


/*******************************************************************Function Declarations********************************************************/
extern inline FUNC(void,FBL) vfnFBL_LlInit(void);
extern inline FUNC(void,FBL) vfnFBL_LlDeInit(void);
extern inline FUNC(void,FBL) vfnFBL_LlClock(void);
extern inline FUNC(void,FBL) vfnFBL_LlUARTInit(void);
extern inline FUNC(void,FBL) vfnFBL_LlUARTDeInit(void);
/**********************************************************************************************************************************************/

/*******************************************************************Macro Definitions**********************************************************/
#define SIM_BASE_PTR                             ((SIM_MemMapPtr)0x40047000u)

#define SIM_SCGC4_REG(base)                      ((base)->SCGC4)
#define SIM_SCGC5_REG(base)                      ((base)->SCGC5)


#define SIM_SCGC4                                SIM_SCGC4_REG(SIM_BASE_PTR)
#define SIM_SCGC5                                SIM_SCGC5_REG(SIM_BASE_PTR)

#define SIM_SCGC5_PORTC_MASK                     0x800u
#define SIM_SCGC4_UART3_MASK                     0x2000u

#define LL_SIM_CLKDIV1 ((volatile uint32_t*)0x40048044)
#define LL_SIM_CLKDIV2 ((volatile uint32_t*)0x40048048)
#define LL_SIM_SCGC4 ((volatile uint32_t*)0x40048034)

#define LL_MCG_C1 ((volatile uint8_t*)0x40064000)
#define LL_OSC_BASE ((volatile uint8_t*)0x40065000)
#define LL_MCG_C2 ((volatile uint8_t*)0x40064001)
#define LL_MCG_S ((volatile uint8_t*)0x40064006)
#define LL_MCG_C7 ((volatile uint8_t*)0x4006400C)
#define LL_MCG_C6 ((volatile uint8_t*)0x40064005)
#define LL_MCG_C5 ((volatile uint8_t*)0x40064004)
#define LL_SIM_OPT2 ((volatile uint32_t*)0x40048004)
#define LL_USB_IRC_EN ((volatile uint8_t*)0x40072144)
#define LL_USB_RCVRY_CTRL ((volatile uint8_t*)0x40072140)

#define PORT_PCR_REG(base,index)                 ((base)->PCR[index])

/** Peripheral PORTC base pointer */
#define PORTC_BASE_PTR                           ((PORT_MemMapPtr)0x4004B000u)

#define PORTB_ISFR                               PORT_ISFR_REG(PORTB_BASE_PTR)

/* PORTC */

#define PORTC_PCR16                              PORT_PCR_REG(PORTC_BASE_PTR,16)
#define PORTC_PCR17                              PORT_PCR_REG(PORTC_BASE_PTR,17)
#define PORT_PCR_MUX_MASK                        0x700u
#define PORT_PCR_MUX_SHIFT                       8
#define PORT_PCR_MUX(x)                          (((uint32_t)(((uint32_t)(x))<<PORT_PCR_MUX_SHIFT))&PORT_PCR_MUX_MASK)

#define NVIC_BASE_PTR                            ((NVIC_MemMapPtr)0xE000E100u)
#define NVIC_ISER_REG(base,index)                ((base)->ISER[index])
#define NVIC_ICER_REG(base,index)                ((base)->ICER[index])
#define NVIC_ISPR_REG(base,index)                ((base)->ISPR[index])
#define NVIC_ICPR_REG(base,index)                ((base)->ICPR[index])
#define NVIC_IABR_REG(base,index)                ((base)->IABR[index])
#define NVIC_IP_REG(base,index)                  ((base)->IP[index])
#define NVIC_STIR_REG(base,index)                ((base)->STIR[index])

#define NVICISER1                                NVIC_ISER_REG(NVIC_BASE_PTR,1)
#define NVICICER1                                NVIC_ICER_REG(NVIC_BASE_PTR,1)


#define UART_BDH_SBR_MASK                        0x1Fu
#define UART_BDH_SBR_SHIFT                       0
#define UART_BDH_SBR(x)                          (((uint8_t)(((uint8_t)(x))<<UART_BDH_SBR_SHIFT))&UART_BDH_SBR_MASK)
#define UART_C4_BRFA_MASK                        0x1Fu
#define UART_C4_BRFA_SHIFT                       0
#define UART_C4_BRFA(x)                          (((uint8_t)(((uint8_t)(x))<<UART_C4_BRFA_SHIFT))&UART_C4_BRFA_MASK)
#define UART_BDL_SBR_MASK                        0xFFu
#define UART_BDL_SBR_SHIFT                       0
#define UART_BDL_SBR(x)                          (((uint8_t)(((uint8_t)(x))<<UART_BDL_SBR_SHIFT))&UART_BDL_SBR_MASK)

#define UART3_REG_BASE_PTR   ((UARTMemMapPtr)0x4006D000)

#define UART3_BDH_REG(base)       ((base)->BDH)
#define UART3_BDL_REG(base)       ((base)->BDL)
#define UART3_C1_REG(base)       ((base)->C1)
#define UART3_C2_REG(base)       ((base)->C2)
#define UART3_S1_REG(base)       ((base)->S1)
#define UART3_S2_REG(base)       ((base)->S2)
#define UART3_C3_REG(base)       ((base)->C3)
#define UART3_D_REG(base)       ((base)->D)
#define UART3_C4_REG(base)       ((base)->C4)

#define UART3_BDH                UART3_BDH_REG(UART3_REG_BASE_PTR)
#define UART3_BDL                UART3_BDL_REG(UART3_REG_BASE_PTR)
#define UART3_C1                UART3_C1_REG(UART3_REG_BASE_PTR)
#define UART3_C2                UART3_C2_REG(UART3_REG_BASE_PTR)
#define UART3_S1                UART3_S1_REG(UART3_REG_BASE_PTR)
#define UART3_S2                UART3_S2_REG(UART3_REG_BASE_PTR)
#define UART3_C3                UART3_C3_REG(UART3_REG_BASE_PTR)
#define UART3_D                UART3_D_REG(UART3_REG_BASE_PTR)
#define UART3_C4                UART3_C4_REG(UART3_REG_BASE_PTR)

#define UART_S1_TDRE_MASK 0x80u
#define UART_C2_TIE_MASK                         0x80u
#define UART_S1_RDRF_MASK                        0x20u
#define UART_C2_TE_MASK                          0x8u
#define UART_C2_RE_MASK                          0x4u
#define UART_C2_RIE_MASK                         0x20u

#define UART3_MUX_MODE 3u
#define UART3_IRQ_HANDLER 5u

#define LL_ERCLKEN_MASK 0x80
#define LL_EREFS_MASK  0x4
#define LL_HGO_MASK 0x8
#define LL_RANGE_MASK 0x30
#define LL_EXTERNAL_MASK 0x00
#define LL_INTERNAL_MASK 0x01
#define LL_OSC_INIT 0x02
#define LL_IRCS_MASK 0x00
#define LL_OCSEL_OSC0_MASK 0x00
#define LL_OCSEL_IRC48_MASK 0x02
#define LL_LPM_MASK 0x02
#define LL_CLKS_EXT_MASK 0x80
#define LL_CLKS_INT_MASK 0x01
#define LL_CLKS_PLL_MASK 0x3F
#define LL_MCG_S_IREFST 0x10
#define LL_MCG_S_CLKST_EXT_MASK 0x08
#define LL_MCG_S_CLKST_INT_MASK 0x04
#define LL_PLL_MASK 0x40
#define LL_PLLST_MASK 0x20
#define LL_PRDIV_MASK 0x13
#define LL_VDIV_MASK 0x18
#define LL_PLLCLCK_MASK 0x40
#define LL_MCG_S_LOCK0_MASK 0x40
#define LL_DIVIDERS_MASK 0x12400000
#define LL_MCGPLLCLK_MASK 0x10000000
#define LL_USBSRC_MASK 0x40000
#define LL_PLLFLLSEL 0x03
#define LL_USBOTG_MASK 0x40000
#define LL_IRC_EN_MASK 0x02
#define LL_USB_CLCK_RCVRY_MASK 0x80

/**********************************************************************************************************************************************/

/*******************************************************************Variable Declarations*****************************************************/
extern VAR(UARTCommMode_t,DEFAULT) cxUartComFlgs;
extern VAR(char_t,DEFAULT) s8UartTxBuffer[40u];
extern VAR(uint8_t,DEFAULT) u8UartTxBuffer[40u];
extern VAR(char_t,DEFAULT) s8UartRxBuffer[40u];
extern VAR(uint8_t,DEFAULT) u8UartTxBufferIndex;
extern VAR(uint8_t,DEFAULT) u8UartRxBufferIndex;
/*********************************************************************************************************************************************/

/*******************************************************************START CODE****************************************************************/
/**
 * Name vfnFBL_LlInit
 * @brief Initialization function for low level drivers for FBL
 *Initializes the clock to the main clock system 120Mhz
 *Call a primitive UART initialization for diagnostics
 * 
 * @param None
 * 
 * @return None, inline function
 */
inline FUNC(void,FBL) vfnFBL_LlInit(void)
{
    vfnFBL_LlClock();

    vfnFBL_LlUARTInit();

    vfnFBL_LlUARTConsoleWrite("Flash Bootloader Initialized\n");
}

/**
 * Name vfnFBL_LlDeInit
 * @brief Denitialization function for low level drivers for FBL
 * since there will be a jump to application that will reinitialize
 * the drivers and things likle badurate or other configurations may change.
 * 
 * @param None
 * 
 * @return None, inline function
 */
inline FUNC(void,FBL) vfnFBL_LlDeInit(void)
{
    vfnFBL_LlUARTDeInit();
}

/**
 * Name vfnFBL_LlClock
 * @brief Clock is configured to 120Mhz,further
 * modifications may be done to slave prescalers however
 * main clock will always be 120Mhz as sepecified in
 * project definitions
 * 
 * @param None
 * 
 * @return None, inline function
 */
inline FUNC(void,FBL) vfnFBL_LlClock(void)
{
#if (HARDWARE_PLATFORM == FRDM_K64F_PLATFORM)
    ///* Set SIM clock divider */
    *LL_SIM_CLKDIV1 |= 0x01240000;
    // /*Set external oscillator as source*/
    *LL_OSC_BASE |= (uint8_t) LL_ERCLKEN_MASK;
    /*Set External Reference Select, High Gain, Range */
    *LL_MCG_C2 |= (uint8_t) (LL_EXTERNAL_MASK | LL_HGO_MASK | LL_RANGE_MASK);

    // while(NULL == (*LL_MCG_S & LL_OSC_INIT))
    // {
        
    // }
    *LL_MCG_C2 |= (uint8_t) LL_IRCS_MASK;
    
    //FRDIV is 0x00
    *LL_MCG_C7 |= (uint8_t) LL_OCSEL_OSC0_MASK;
    /*Disable low power mode*/
    *LL_MCG_C2 &= (uint8_t) (~LL_LPM_MASK);
    /*Change to use external clock first*/
    *LL_MCG_C1 |= (uint8_t) LL_CLKS_EXT_MASK;
    
    while((*LL_MCG_S & LL_MCG_S_IREFST) && (!(*LL_MCG_S & LL_MCG_S_CLKST_EXT_MASK)))
    {

    }

    *LL_MCG_C6 &= (~LL_PLL_MASK);
    while(NULL != (*LL_MCG_S & LL_PLLST_MASK))
    {

    }
    *LL_MCG_C5 |= LL_PRDIV_MASK;

    *LL_MCG_C6 |= LL_VDIV_MASK;

    *LL_MCG_C5 |= LL_PLLCLCK_MASK;

    while(NULL == (*LL_MCG_S & LL_MCG_S_LOCK0_MASK))
    {

    }

    *LL_MCG_C6 |= (LL_PLL_MASK);

    while(NULL == (*LL_MCG_S & LL_PLLST_MASK))
    {

    }

    *LL_MCG_C1 &= (LL_CLKS_PLL_MASK);

    *LL_SIM_CLKDIV1 = LL_DIVIDERS_MASK;

    *LL_SIM_OPT2 |= LL_MCGPLLCLK_MASK;

#elif (HARDWARE_PLATFORM == ARCTURUS_PLATFORM)

    *LL_SIM_CLKDIV2 = 0x00u;
    *LL_SIM_OPT2 |= LL_USBSRC_MASK | LL_PLLFLLSEL;
    *LL_SIM_SCGC4 |= LL_USBOTG_MASK;
    *LL_MCG_C2 |= (uint8_t) (LL_INTERNAL_MASK);
    *LL_USB_IRC_EN |= LL_IRC_EN_MASK;
    *LL_USB_RCVRY_CTRL |= LL_USB_CLCK_RCVRY_MASK;
    *LL_SIM_CLKDIV1 |= 0x01240000;
    // while(NULL == (*LL_MCG_S & LL_OSC_INIT))
    // {
        
    // }
    *LL_MCG_C2 |= (uint8_t) LL_IRCS_MASK;
    
    //FRDIV is 0x00
    *LL_MCG_C7 |= (uint8_t) LL_OCSEL_IRC48_MASK;
    /*Disable low power mode*/
    *LL_MCG_C2 &= (uint8_t) (~LL_LPM_MASK);
    /*Change to use internal clock first*/
    *LL_MCG_C1 |= (uint8_t) LL_CLKS_INT_MASK;
    
    while((!(*LL_MCG_S & LL_MCG_S_IREFST)) && (!(*LL_MCG_S & LL_MCG_S_CLKST_INT_MASK)))
    {

    }

    *LL_MCG_C6 &= (~LL_PLL_MASK);
    while(NULL != (*LL_MCG_S & LL_PLLST_MASK))
    {

    }
    *LL_MCG_C5 |= LL_PRDIV_MASK;

    *LL_MCG_C6 |= LL_VDIV_MASK;

    *LL_MCG_C5 |= LL_PLLCLCK_MASK;

    while(NULL == (*LL_MCG_S & LL_MCG_S_LOCK0_MASK))
    {

    }

    *LL_MCG_C6 |= (LL_PLL_MASK);

    while(NULL == (*LL_MCG_S & LL_PLLST_MASK))
    {

    }

    *LL_MCG_C1 &= (LL_CLKS_PLL_MASK);

    *LL_SIM_CLKDIV1 = LL_DIVIDERS_MASK;

    *LL_SIM_OPT2 |= LL_MCGPLLCLK_MASK;
    /*NOTE: the clock configuration is intended for 120 Mhz however
    *There is an error as noted in the UART bus clock, the bus clock 
    *should be 60 Mhz but its actually close to 57,211,173.8213 or 57.2 Mhz
    *take this error in account to clock calculus for drivers*/
#endif
}

/**
 * Name vfnFBL_LlUARTInit
 * @brief Denitialization function for UART that will  be used
 * for FBL diagnostics
 * 
 * @param None
 * 
 * @return None, inline function
 */
inline FUNC(void,FBL) vfnFBL_LlUARTInit(void)
{
    SIM_SCGC5 |= SIM_SCGC5_PORTC_MASK;
	SIM_SCGC4 |= SIM_SCGC4_UART3_MASK;
	/*PCR16 and PCR17 GPIO multiplexing as Tx and Rx*/
	PORTC_PCR16=PORT_PCR_MUX(UART3_MUX_MODE);
	PORTC_PCR17=PORT_PCR_MUX(UART3_MUX_MODE);
    UART3_C2 &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK );
    /*UART uses bus clock which should be 60Mhz but due error 
    bus clock is actually 57.2 so SBR(12 bits) is 377.60*/
    UART3_BDH|= UART_BDH_SBR(1u); 
    /*The closest value to .602 is 19/32 so BRFA is 19*/
    UART3_C4 |= UART_C4_BRFA(19u);
    /*The lower part of 377 is 121*/
	UART3_BDL|= UART_BDL_SBR(121u);
    UART3_C2 |=  (UART_C2_TE_MASK | UART_C2_RE_MASK );
    UART3_C2 |= UART_C2_RIE_MASK;
    NVICICER1 = 1u<<(UART3_IRQ_HANDLER);
    NVICISER1 = 1u<<(UART3_IRQ_HANDLER);
}

/**
 * Name vfnFBL_LlUARTDeInit
 * @brief Denitialization function for UART that is 
 * being used for diagnostics, since the Application
 * may need change configuration of these registers
 * @param None
 * 
 * @return None, inline function
 */
inline FUNC(void,FBL) vfnFBL_LlUARTDeInit(void)
{
    /*Disable interrupts*/
    NVICICER1 = 0u<<(UART3_IRQ_HANDLER);
    NVICISER1 = 0u<<(UART3_IRQ_HANDLER);
    /*Disable transmision and reception*/
    UART3_C2 &= ~(UART_C2_RIE_MASK);
    UART3_C2 &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK );
    /*Reset BDL values*/
    UART3_BDL|= 0u;
    /*Reset C4 and BDH values*/
    UART3_C4 |= UART_C4_BRFA(0u);
    UART3_BDH|= UART_BDH_SBR(0u);
    /*PCR16 and PCR17 alternative mode removed*/
    SIM_SCGC5 |= 0u;
    SIM_SCGC4 |= 0u;
}

/**
 * Name vfnFBL_LlUARTConsoleWrite
 * @brief UART primitive API that writes ASCII characters
 * 
 * @param char_t s8Message[] string casted as character arrays due
 * C constraints, local buffer is copied to global buffer
 * 
 * @return None
 */
FUNC(void,FBL) vfnFBL_LlUARTConsoleWrite(char_t s8Message[])
{
    uint8_t u8MailboxInterchangeCounter = 0;
	
	if(0 == cxUartComFlgs.u8TransmissionMode)
	{
        /*Since there is no transmission proceed to write in buffer*/
		do
		{
			s8UartTxBuffer[u8MailboxInterchangeCounter] = s8Message[u8MailboxInterchangeCounter];
			
		}while('\0' != s8Message[u8MailboxInterchangeCounter++]);
        /*Flag to wait until transmission finishes*/
		cxUartComFlgs.u8TransmissionMode = 1;
        /*Buffer is full start transmission*/
		UART3_C2 |= UART_C2_TIE_MASK;
	 }

     while(1 == cxUartComFlgs.u8TransmissionMode)
     {
         /*block the comm*/
     }
}

/**
 * Name vfnFBL_LlUARTUdsWrite
 * @brief UART primitive API that writes UDS services response
 * 
 * @param UdsServiceResp_t cxudsResponsePtr Pointer to structure contained
 * the UDS response for the requested service
 * 
 * @return None
 */
FUNC(void,FBL) vfnFBL_LlUARTUdsWrite(UdsServiceResp_t* cxudsResponsePtr)
{
    if(0 == cxUartComFlgs.u8TransmissionMode)
    {

        u8UartTxBuffer[0] = cxudsResponsePtr->u8Resp;
        u8UartTxBuffer[1] = cxudsResponsePtr->u8SID;
        u8UartTxBuffer[2] = cxudsResponsePtr->u8AdditionalParam;
        u8UartTxBuffer[3] = cxudsResponsePtr->u8AdditionalParam2;
        u8UartTxBuffer[4] = UART_END_TRANSMISSION;
        cxUartComFlgs.u8TransmissionMode = 1;
        UART3_C2 |= UART_C2_TIE_MASK;
    }

     while(1 == cxUartComFlgs.u8TransmissionMode)
     {
         /*block the comm*/
     }
}

/**
 * Name vfnFBL_LlUARTRead
 * @brief UART primitive API that writes UDS services response
 * 
 * @param char_t s8Message[] Buffer where the response will be stored if any
 * @param uint8_t u8RxBufferSize size of the buffer to know whhere to stop 
 * since it can be stream of characters or uds numbers
 * 
 * @return None
 */
FUNC(void,FBL) vfnFBL_LlUARTRead(char_t s8Message[], uint8_t u8RxBufferSize)
{
	uint8_t u8IndexCounter = 0;
	if( 1 == cxUartComFlgs.u8ReceptionMode)
	{
		do
		{
			s8Message[u8IndexCounter] = s8UartRxBuffer[u8IndexCounter];
			
		}while(u8IndexCounter++ <= u8RxBufferSize);
		cxUartComFlgs.u8ReceptionMode= 0;
	}
}

/*********************************************************************************************************************************************/