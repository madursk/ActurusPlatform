
#include "Port.h"
#include "Port_cfg.h"
#include "DiagM.h"

#define PRINT(x)     vfnDiagnosticConsole_Print(MCAL,PORT,x)

FUNC(std_ReturnType,AUTOMATIC)stdfnPort_Init()
{
    std_ReturnType sResult = E_NOT_OK;
    sResult = stdfnPort_SetCfg();
    return sResult;
}