#ifndef I2C_IRQ_H
#define I2C_IRQ_H

void I2C0_IRQHandler(void);
typedef void (*I2C0_IRQHandler_Ptr)(void);
#endif