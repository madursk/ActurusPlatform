#include "I2C_cfg.h"

I2C_cfg_st_t I2C_cfgSt_ast[I2C_HW_UNITS];
I2C_SequenceType_t i2cSeq_1[I2C_SEQ_JOBS_LENGTH];
I2C_SequenceType_t i2cSeq_2[I2C_SEQ_JOBS_LENGTH];
I2C_SequenceType_t i2cSeq_3[I2C_SEQ_JOBS_LENGTH];
I2C_SequenceType_t i2cSeq_4[I2C_SEQ_JOBS_LENGTH];
I2C_SequenceType_t i2cSeq_5[I2C_SEQ_JOBS_LENGTH];
I2C_SequenceType_t i2cSeq_6[I2C_SEQ_JOBS_LENGTH];
I2C_SequenceType_t i2cSeq_7[I2C_SEQ_JOBS_LENGTH];
I2C_SequenceType_t i2cSeq_8[I2C_SEQ_JOBS_LENGTH];
I2C_SequenceType_t i2cSeq_9[I2C_SEQ_JOBS_LENGTH];
I2C_SequenceType_t i2cSeq_10[I2C_SEQ_JOBS_LENGTH];
I2C_SequenceType_t i2cSeq_11[I2C_SEQ_JOBS_LENGTH];
I2C_SequenceType_t i2cSeq_12[I2C_SEQ_JOBS_LENGTH];
I2C_SequenceType_t i2cSeq_13[I2C_SEQ_JOBS_LENGTH];
I2C_SequenceType_t i2cSeq_14[I2C_SEQ_JOBS_LENGTH];
I2C_SequenceType_t i2cSeq_15[I2C_SEQ_JOBS_LENGTH];

I2C_SequenceType_t i2cEmptySeqIndex;

FUNC_P2VAR(I2C_cfg_stPtr,AUTOMATIC)pfnI2C_SetCfg()
{

    I2C_cfgSt_ast[I2C0].I2C_A1_cfg = 0x00u;
    I2C_cfgSt_ast[I2C0].I2C_C1_cfg = 0x80u;
    I2C_cfgSt_ast[I2C0].I2C_C2_cfg = 0x00u;
    I2C_cfgSt_ast[I2C0].I2C_FLT_cfg = 0x00u;
    I2C_cfgSt_ast[I2C0].I2C_RA_cfg = 0x00u;
    I2C_cfgSt_ast[I2C0].I2C_SMB_cfg = 0x00u;
    I2C_cfgSt_ast[I2C0].I2C_SLTH_cfg = 0x00u;
    I2C_cfgSt_ast[I2C0].I2C_SLTL_cfg = 0x00u;

    I2C_cfgSt_ast[I2C1].I2C_A1_cfg = 0x00u;
    I2C_cfgSt_ast[I2C1].I2C_C1_cfg = 0x80u;
    I2C_cfgSt_ast[I2C1].I2C_C2_cfg = 0x00u;
    I2C_cfgSt_ast[I2C1].I2C_FLT_cfg = 0x00u;
    I2C_cfgSt_ast[I2C1].I2C_RA_cfg = 0x00u;
    I2C_cfgSt_ast[I2C1].I2C_SMB_cfg = 0x00u;
    I2C_cfgSt_ast[I2C1].I2C_SLTH_cfg = 0x00u;
    I2C_cfgSt_ast[I2C1].I2C_SLTL_cfg = 0x00u;

    I2C_cfgSt_ast[I2C2].I2C_A1_cfg = 0x00u;
    I2C_cfgSt_ast[I2C2].I2C_C1_cfg = 0x00u;
    I2C_cfgSt_ast[I2C2].I2C_C2_cfg = 0x00u;
    I2C_cfgSt_ast[I2C2].I2C_FLT_cfg = 0x00u;
    I2C_cfgSt_ast[I2C2].I2C_RA_cfg = 0x00u;
    I2C_cfgSt_ast[I2C2].I2C_SMB_cfg = 0x00u;
    I2C_cfgSt_ast[I2C2].I2C_SLTH_cfg = 0x00u;
    I2C_cfgSt_ast[I2C2].I2C_SLTL_cfg = 0x00u;

    return &I2C_cfgSt_ast[I2C0];
}

FUNC(std_ReturnType,AUTOMATIC)stdfnI2C_SetSequence(I2C_JobType_t jobID_lt)
{
    std_ReturnType fnStatus_lt;
    I2C_SequenceType_t seqIndex_lt = jobID_lt % I2C_SEQ_JOBS_LENGTH;
    I2C_SequenceType_t sequence_lt = tfnGetSequenceAvail(jobID_lt);
    if(seqIndex_lt > I2C_SEQ_JOBS_LENGTH || seqIndex_lt > I2C_MAX_SEQ_NUMBER)
    {
        fnStatus_lt = E_NOT_OK;
    }
    else
    {
        fnStatus_lt = E_OK;
        switch(sequence_lt)
        {
            case(I2C_SEQ_1):
            {
                i2cSeq_1[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;

            case(I2C_SEQ_2):
            {
                i2cSeq_2[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;
            
            case(I2C_SEQ_3):
            {
                i2cSeq_3[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;
            
            case(I2C_SEQ_4):
            {
                i2cSeq_4[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;

            case(I2C_SEQ_5):
            {
                i2cSeq_5[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;

            case(I2C_SEQ_6):
            {
                i2cSeq_6[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;

            case(I2C_SEQ_7):
            {
                i2cSeq_7[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;

            case(I2C_SEQ_8):
            {
                i2cSeq_8[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;

            case(I2C_SEQ_9):
            {
                i2cSeq_9[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;

            case(I2C_SEQ_10):
            {
                i2cSeq_10[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;

            case(I2C_SEQ_11):
            {
                i2cSeq_11[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;

            case(I2C_SEQ_12):
            {
                i2cSeq_12[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;

            case(I2C_SEQ_13):
            {
                i2cSeq_13[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;
            case(I2C_SEQ_14):
            {
                i2cSeq_14[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;
            
            case(I2C_SEQ_15):
            {
                i2cSeq_15[seqIndex_lt] = jobID_lt;
                i2cEmptySeqIndex++;
            }break;

            default:
            {
                fnStatus_lt = E_NOT_OK;
            }

        }/*End Switch*/
    }/*End Else*/
}/*End Function*/

FUNC(std_ReturnType,AUTOMATIC)stdfnI2C_GetSequence(I2C_SequenceType_Ptr i2cSeq_Ptr, I2C_SequenceType_t  i2cSeq_t)
{
    std_ReturnType status_lstd;
    if(I2C_CFG_STRUCT_NULL_E == i2cSeq_Ptr)
    {
        status_lstd = E_NOT_OK;
    }
    else if( I2C_MAX_SEQ_NUMBER < i2cSeq_t  || NULL == i2cSeq_t)
    {
        status_lstd = E_NOT_OK;
    }
    else
    {
        status_lstd = E_OK;
        switch(i2cSeq_t)
        {
            case(I2C_SEQ_1):
            {
                i2cSeq_Ptr = i2cSeq_1;
            }break;

            case(I2C_SEQ_2):
            {
                i2cSeq_Ptr = i2cSeq_2;
            }break;
            
            case(I2C_SEQ_3):
            {
                i2cSeq_Ptr = i2cSeq_3;
            }break;
            
            case(I2C_SEQ_4):
            {
                i2cSeq_Ptr = i2cSeq_4;
            }break;

            case(I2C_SEQ_5):
            {
                i2cSeq_Ptr = i2cSeq_5;
            }break;

            case(I2C_SEQ_6):
            {
                i2cSeq_Ptr = i2cSeq_6;
            }break;

            case(I2C_SEQ_7):
            {
                i2cSeq_Ptr = i2cSeq_7;
            }break;

            case(I2C_SEQ_8):
            {
                i2cSeq_Ptr = i2cSeq_8;
            }break;

            case(I2C_SEQ_9):
            {
                i2cSeq_Ptr = i2cSeq_9;
            }break;

            case(I2C_SEQ_10):
            {
                i2cSeq_Ptr = i2cSeq_10;
            }break;

            case(I2C_SEQ_11):
            {
                i2cSeq_Ptr = i2cSeq_11;
            }break;

            case(I2C_SEQ_12):
            {
                i2cSeq_Ptr = i2cSeq_12;
            }break;

            case(I2C_SEQ_13):
            {
                i2cSeq_Ptr = i2cSeq_13;
            }break;
            case(I2C_SEQ_14):
            {
                i2cSeq_Ptr = i2cSeq_14;
            }break;
            
            case(I2C_SEQ_15):
            {
                i2cSeq_Ptr = i2cSeq_15;
            }break;

            default:
            {
                status_lstd = E_NOT_OK;
            }
        }/*End switch*/
    }/*End Else*/
    return status_lstd;
}/*End Function*/

FUNC(I2C_SequenceType_t,AUTOMATIC)tfnGetSequenceAvail(I2C_JobType_t jobID_lt)
{
    I2C_SequenceType_t seqRet_lt;

    if(I2C_SEQ_1_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_1;
    }
    else if (I2C_SEQ_1_END < jobID_lt && I2C_SEQ_2_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_2;
    }
    else if (I2C_SEQ_2_END < jobID_lt && I2C_SEQ_3_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_3;
    }
    else if (I2C_SEQ_3_END < jobID_lt && I2C_SEQ_4_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_4;
    }
    else if (I2C_SEQ_4_END < jobID_lt && I2C_SEQ_5_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_5;
    }
    else if (I2C_SEQ_5_END < jobID_lt && I2C_SEQ_6_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_6;
    }
    else if (I2C_SEQ_6_END < jobID_lt && I2C_SEQ_7_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_7;
    }
    else if (I2C_SEQ_7_END < jobID_lt && I2C_SEQ_8_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_8;
    }
    else if (I2C_SEQ_8_END < jobID_lt && I2C_SEQ_9_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_9;
    }
    else if (I2C_SEQ_9_END < jobID_lt && I2C_SEQ_10_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_10;
    }
    else if (I2C_SEQ_10_END < jobID_lt && I2C_SEQ_11_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_11;
    }
    else if (I2C_SEQ_11_END < jobID_lt && I2C_SEQ_12_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_12;
    }
    else if (I2C_SEQ_12_END < jobID_lt && I2C_SEQ_13_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_13;
    }
    else if (I2C_SEQ_13_END < jobID_lt && I2C_SEQ_14_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_14;
    }
    else if (I2C_SEQ_14_END < jobID_lt && I2C_SEQ_15_END >= jobID_lt )
    {
        seqRet_lt = I2C_SEQ_15;
    }
    else
    {

    }
    return seqRet_lt;
    
    
}