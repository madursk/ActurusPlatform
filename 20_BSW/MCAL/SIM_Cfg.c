#include "SIM_Cfg.h"

FUNC(std_ReturnType,AUTOMATIC)stdfnSIM_SetCfg(void)
{
    std_ReturnType sResult = E_NOT_OK;
    uint32_t u32RegMask;

#if(SOPT1_CFG==STD_ON)
    SIM_SOPT1|= SOPT1_CFG_MASK;
    sResult = E_OK;
#endif

#if(SOPT2_CFG==STD_ON)
    SIM_SOPT2|= SOPT2_CFG_MASK;
    sResult = E_OK;
#endif

#if(SOPT4_CFG==STD_ON)
    SIM_SOPT4|= SOPT4_CFG_MASK;
    sResult = E_OK;
#endif

#if(SOPT5_CFG==STD_ON)
     SIM_SOPT5|= SOPT5_CFG_MASK;
    sResult = E_OK;
#endif

#if(SOPT7_CFG==STD_ON)
    SIM_SOPT7|= SOPT7_CFG_MASK;
    sResult = E_OK;
#endif

#if(SOPT1CG_CFG==STD_ON)
    SIM_SOPT1CFG|= SOPT1CG_CFG_MASK;
    sResult = E_OK;
#endif

#if(CLKDIV1_CFG==STD_ON)
    SIM_CLKDIV1|= CLKDIV1_CFG_MASK;
    sResult = E_OK;
#endif

#if(CLKDIV2_CFG==STD_ON)
    SIM_CLKDIV2|= CLKDIV2_CFG_MASK;
    sResult = E_OK;
#endif

#if(SCGC1_CFG==STD_ON)
    u32RegMask = SCGC1_CFG_MASK;
    vfnSIM_SetSCGC1();
    sResult = E_OK;
#endif

#if(SCGC2_CFG==STD_ON)
    u32RegMask = SCGC2_CFG_MASK;
    vfnSIM_SetSCGC2();
    sResult = E_OK;
#endif

#if(SCGC3_CFG==STD_ON)
    u32RegMask = SCGC3_CFG_MASK;
    vfnSIM_SetSCGC3();
    sResult = E_OK;
#endif

#if(SCGC4_CFG==STD_ON)
    u32RegMask = SCGC4_CFG_MASK;
    vfnSIM_SetSCGC4(u32RegMask);
    sResult = E_OK;
#endif

#if(SCGC5_CFG==STD_ON)
    u32RegMask = SCGC5_CFG_MASK;
    vfnSIM_SetSCGC5(u32RegMask);
    sResult = E_OK;
#endif

#if(SCGC6_CFG==STD_ON)
    u32RegMask = SCGC6_CFG_MASK;
    vfnSIM_SetSCGC6();
    sResult = E_OK;
#endif

#if(SCGC7_CFG==STD_ON)
    u32RegMask = SCGC7_CFG_MASK;
    vfnSIM_SetSCGC7();
    sResult = E_OK;
#endif

#if(FCFG1_CFG==STD_ON)
    SIM_FCFG2|= FCFG1_CFG_MASK;
    sResult = E_OK;
#endif

#if(FCFG1_CFG==STD_ON)
    SIM_FCFG2|= FCFG2_CFG_MASK;
    sResult = E_OK;
#endif
    return sResult;
}

FUNC(void,AUTOMATIC)vfnSIM_SetSCGC1(uint32_t u32SCGC1Mask)
{
    SIM_SCGC1 |= u32SCGC1Mask;
}

FUNC(void,AUTOMATIC)vfnSIM_SetSCGC2(uint32_t u32SCGC2Mask)
{
    SIM_SCGC2 |= u32SCGC2Mask;
}

FUNC(void,AUTOMATIC)vfnSIM_SetSCGC3(uint32_t u32SCGC3Mask)
{
    SIM_SCGC3 |= u32SCGC3Mask;
}

FUNC(void,AUTOMATIC)vfnSIM_SetSCGC4(uint32_t u32SCGC4Mask)
{
    SIM_SCGC4 |= u32SCGC4Mask;
}

FUNC(void,AUTOMATIC)vfnSIM_SetSCGC5(uint32_t u32SCGC5Mask)
{
    SIM_SCGC5 |= u32SCGC5Mask;
}

FUNC(void,AUTOMATIC)vfnSIM_SetSCGC6(uint32_t u32SCGC6Mask)
{
    SIM_SCGC6 |= u32SCGC6Mask;
}

FUNC(void,AUTOMATIC)vfnSIM_SetSCGC7(uint32_t u32SCGC7Mask)
{
    SIM_SCGC6 |= u32SCGC7Mask;
}