#include "I2C_irq.h"
#include "I2C.h"
#include "I2C_cfg.h"

extern I2C_InterruptStatus_st i2cMode_st;
extern uint8_t i2cTxBufferCH0_a[I2C_TX_MAILBOX];
extern uint8_t i2cTxBufferIndexCH0_u8;
extern uint8_t i2cTxBufferLoadedJCH0_u8;
extern uint8_t i2cTxBufferCH1_a[I2C_TX_MAILBOX];
extern uint8_t i2cTxBufferIndexCH1_u8;
extern uint8_t i2cTxBufferCH2_a[I2C_TX_MAILBOX];
extern uint8_t i2cTxBufferIndexCH2_u8;
extern uint8_t i2cRxBufferCH0_au8[I2C_RX_MAILBOX];
extern uint8_t i2cRxBufferIndexCH0_u8;
extern I2C_JobResultType_e i2cJobState;
extern I2C_Err_t i2cError;
I2C0_IRQHandler_Ptr i2c0_IRQ = I2C0_IRQHandler;

void I2C0_IRQHandler(void)
{
     if((i2cMode_st.TXMODE == TRANSMISSION_ON ) &&(I2C0_S & I2C_TCF_MASK)&& ((I2C0_S & I2C_RXAK_MASK) == NULL))
     {
         if(i2cTxBufferIndexCH0_u8 < i2cTxBufferLoadedJCH0_u8)
         {
             I2C0_S |= I2C_IICIF_MASK;
            ++i2cTxBufferIndexCH0_u8;
            i2cJobState = I2C_JOB_QUEUED;
            I2C0_D = i2cTxBufferCH0_a[i2cTxBufferIndexCH0_u8];

         }
         else
         {
             i2cTxBufferIndexCH0_u8 = NULL;
             i2cMode_st.TXMODE = TRANSMISSION_OFF;
             i2cMode_st.CHANNEL = 0u;
             I2C0_C1 ^= I2C_MST_MODE_MASK;
             i2cJobState = I2C_JOB_OK;
         }
         
     }
     else if((i2cMode_st.TXMODE == TRANSMISSION_ON ) &&(I2C0_S & I2C_TCF_MASK)&& (I2C0_S & I2C_RXAK_MASK))
     {
         i2cMode_st.RXAK = 1u;
         I2C0_S |= I2C_IICIF_MASK;
         i2cMode_st.CHANNEL = 0u;
         i2cJobState = I2C_JOB_FAILED;
         i2cError = I2C_0_NO_ACK_RX_E;
     }
     else if((i2cMode_st.TXMODE == TRANSMISSION_OFF) &&(I2C0_S & I2C_TCF_MASK))
     {
         ++i2cTxBufferIndexCH0_u8;
         if((i2cMode_st.RXMODE == RECEPTION_OFF) && (i2cTxBufferIndexCH0_u8 < i2cTxBufferLoadedJCH0_u8)\
             && (i2cTxBufferCH0_a[i2cTxBufferIndexCH0_u8] != READ_REG_END))
         {
             i2cTxBufferCH0_a[i2cTxBufferIndexCH0_u8] = I2C0_D;
         }
         else if ((i2cMode_st.RXMODE == RECEPTION_OFF) && (i2cTxBufferIndexCH0_u8 < i2cTxBufferLoadedJCH0_u8)\
             && (i2cTxBufferCH0_a[i2cTxBufferIndexCH0_u8] == READ_REG_END))
         {
             i2cMode_st.RXMODE == RECEPTION_ON;
             I2C0_C1 ^= I2C_TX_RX_SLCT_MASK;
         }
         else if(i2cMode_st.RXMODE == RECEPTION_ON)
         {
            i2cRxBufferCH0_au8[i2cRxBufferIndexCH0_u8];
            i2cRxBufferIndexCH0_u8++;
         }
         else
         {
             /* code */
         }
         
     }
}