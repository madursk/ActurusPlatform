
#include "NVIC_Cfg.h"

extern IRQ_ConfigType_Ptr irqCfgStructs_aPtr[NVIC_CONFIGURED_IRQ_UNITS];
IRQ_ConfigType_t UART_irqCfgStruct;
IRQ_ConfigType_t I2C_irqCfgStruct;

FUNC(std_ReturnType,AUTOMATIC)vfnSetConfig_NVIC()
{
    std_ReturnType nvicCfgStatus_lstd;

    if(NULLPTR == irqCfgStructs_aPtr)
    {
        nvicCfgStatus_lstd = E_NOT_OK;
    }
    else
    {
        UART_irqCfgStruct.IRQ_u8 = 37u;
        I2C_irqCfgStruct.IRQ_u8 = 24u;
        irqCfgStructs_aPtr[0] = &UART_irqCfgStruct;
        irqCfgStructs_aPtr[1] = &I2C_irqCfgStruct;
        nvicCfgStatus_lstd = E_OK;

    }
    return nvicCfgStatus_lstd;
}

