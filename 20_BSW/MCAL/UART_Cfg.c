#include "UART_Cfg.h"
#include "UART.h"


#if(UART0==STD_ON)
VAR(UartHwUnits_t,DEFAULT) cxUart0Loadout;
#endif
#if(UART1==STD_ON)
VAR(UartHwUnits_t,DEFAULT) cxUart1Loadout;
#endif
#if(UART2==STD_ON)
VAR(UartHwUnits_t,DEFAULT) cxUart2Loadout;
#endif
#if(UART3==STD_ON)
VAR(UartHwUnits_t,DEFAULT) cxUart3Loadout;
#endif
#if(UART4==STD_ON)
VAR(UartHwUnits_t,DEFAULT) cxUart4Loadout;
#endif
#if(UART5==STD_ON)
VAR(UartHwUnits_t,DEFAULT) cxUart5Loadout;
#endif

UartHwUnits_t* cxUartDriversPtrVct[MAX_HWUART_NUM] = 
{
#if(UART0==STD_ON)
    &cxUart0Loadout,
#else
    0u,
#endif
#if(UART1==STD_ON)
    &cxUart1Loadout,
#else
    0u,
#endif
#if(UART2==STD_ON)
    &cxUart2Loadout,
#else
    0u,
#endif
#if(UART3==STD_ON)
    &cxUart3Loadout,
#else
    0u,
#endif
#if(UART4==STD_ON)
    &cxUart4Loadout
#else
    0u
#endif
};

/**
 * Name stdfnUart_setCfg
 * @brief Function that loads configuration
 * of the UART macros
 * 
 * @param None
 * 
 * @return std_ReturnType, returns E_NOT_OK
 * if one of the configured channels is not set properly
 * returns E_OK if everything was configured
 */
FUNC(std_ReturnType, AUTOMATIC) stdfnUart_setCfg(void)
{
    std_ReturnType stdResult = E_NOT_OK;
    for(uint8_t u8DriverCounter = 0u; u8DriverCounter < MAX_HWUART_NUM; u8DriverCounter++)
    {
        if(NULLPTR != cxUartDriversPtrVct[u8DriverCounter])
        {
            switch (u8DriverCounter)
            {
            case(0u):{}break;
            case(1u):{}break;
            case(3u):
            {
                /* Disable Transmitter and Receiver*/
                UART3_C2 &= ~(UART_C2_TE_MASK | UART_C2_RE_MASK );
                /* Configure Baudrate High part first*/
                UART3_BDH |= UART3_BDH_CFG_VALUES;
                /* Configure Baudrate Low part */
                UART3_BDL |= UART3_BDL_CFG_VALUES;
                /* Configure C2 register*/
                UART3_C2 |= UART3_C2_CFG_VALUES;
                /* Enable Receptor */
                UART3_C2 |= UART_C2_RIE_MASK;
                cxUart3Loadout.cxStatus = UART_INIT;
                cxUart3Loadout.u8DriverId = 3u;
                cxUart3Loadout.Write =stdfnUART3Write; 
                stdResult = E_OK;
            }break;
            case(2u):{}break;
            case(4u):{}break;
            case(5u):{}break;
            
            default:
                break;
            }
        }
    }
    return stdResult;
}