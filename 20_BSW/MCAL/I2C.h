#ifndef I2C_DRIVER_H
#define I2C_DRIVER_H

#include "I2C_Types.h"

/**********************Driver HSI************************************/
typedef struct I2C_MemMap
{
    uint8_t I2C_A1      ;
    uint8_t I2C_F       ;
    uint8_t I2C_C1      ;
    uint8_t I2C_S       ;
    uint8_t I2C_D       ;
    uint8_t I2C_C2      ;
    uint8_t I2C_FLT     ;
    uint8_t I2C_RA      ;
    uint8_t I2C_SMB     ;
    uint8_t I2C_SLTH    ;
    uint8_t I2C_SLTL    ;

}*I2C_MemMapPtr;


#define I2C_A1_REG(base)        ((base)->I2C_A1)
#define I2C_F_REG(base)         ((base)->I2C_F)
#define I2C_C1_REG(base)        ((base)->I2C_C1)
#define I2C_S_REG(base)         ((base)->I2C_S)
#define I2C_D_REG(base)         ((base)->I2C_D)
#define I2C_C2_REG(base)        ((base)->I2C_C2)
#define I2C_FLT_REG(base)       ((base)->I2C_FLT)
#define I2C_RA_REG(base)        ((base)->I2C_RA)
#define I2C_SMB_REG(base)       ((base)->I2C_SMB)
#define I2C_SLTH_REG(base)      ((base)->I2C_SLTH)
#define I2C_SLTL_REG(base)      ((base)->I2C_SLTL)

#define I2C0_BASE_PTR       ((I2C_MemMapPtr)0x40066000)
#define I2C1_BASE_PTR       ((I2C_MemMapPtr)0x40067000)
#define I2C2_BASE_PTR       ((I2C_MemMapPtr)0x400E6000)

#define I2C0_A1             I2C_A1_REG(I2C0_BASE_PTR)
#define I2C0_F               I2C_F_REG(I2C0_BASE_PTR)
#define I2C0_C1              I2C_C1_REG(I2C0_BASE_PTR)
#define I2C0_D               I2C_D_REG(I2C0_BASE_PTR)
#define I2C0_S               I2C_S_REG(I2C0_BASE_PTR)
#define I2C0_C2              I2C_C2_REG(I2C0_BASE_PTR)
#define I2C0_FLT             I2C_FLT_REG(I2C0_BASE_PTR)
#define I2C0_RA              I2C_RA_REG(I2C0_BASE_PTR)
#define I2C0_SMB             I2C_SMB_REG(I2C0_BASE_PTR)
#define I2C0_SLTH            I2C_SLTH_REG(I2C0_BASE_PTR)
#define I2C0_SLTL             I2C_SLTL_REG(I2C0_BASE_PTR)

#define I2C1_A1             I2C_A1_REG(I2C1_BASE_PTR)
#define I2C1_F               I2C_F_REG(I2C1_BASE_PTR)
#define I2C1_C1              I2C_C1_REG(I2C1_BASE_PTR)
#define I2C1_S               I2C_S_REG(I2C1_BASE_PTR)
#define I2C1_D               I2C_D_REG(I2C1_BASE_PTR)
#define I2C1_C2              I2C_C2_REG(I2C1_BASE_PTR)
#define I2C1_FLT             I2C_FLT_REG(I2C1_BASE_PTR)
#define I2C1_RA              I2C_RA_REG(I2C1_BASE_PTR)
#define I2C1_SMB             I2C_SMB_REG(I2C1_BASE_PTR)
#define I2C1_SLTH            I2C_SLTH_REG(I2C1_BASE_PTR)
#define I2C1_SLTL             I2C_SLTL_REG(I2C1_BASE_PTR)

#define I2C2_A1             I2C_A1_REG(I2C2_BASE_PTR)
#define I2C2_F               I2C_F_REG(I2C2_BASE_PTR)
#define I2C2_C1              I2C_C1_REG(I2C2_BASE_PTR)
#define I2C2_S               I2C_S_REG(I2C2_BASE_PTR)
#define I2C2_D               I2C_D_REG(I2C2_BASE_PTR)
#define I2C2_C2              I2C_C2_REG(I2C2_BASE_PTR)
#define I2C2_FLT             I2C_FLT_REG(I2C2_BASE_PTR)
#define I2C2_RA              I2C_RA_REG(I2C2_BASE_PTR)
#define I2C2_SMB             I2C_SMB_REG(I2C2_BASE_PTR)
#define I2C2_SLTH            I2C_SLTH_REG(I2C2_BASE_PTR)
#define I2C2_SLTL             I2C_SLTL_REG(I2C2_BASE_PTR)



/**********************Driver HSI End*********************************/



/************************Macros ************************************/

#define ALT1 0x00000100u
#define ALT2 0x00000200u
#define ALT3 0x00000300u
#define ALT4 0x00000400u
#define ALT5 0x00000500u
#define ALT6 0x00000600u
#define ALT7 0x00000700u

#define I2C_0_SCL_PTE24 ALT5
#define I2C_0_SDA_PTE25 ALT5
#define I2C_0_SCL_PTB0  ALT2
#define I2C_0_SDA_PTB1  ALT2
#define I2C_0_SCL_PTB2  ALT2
#define I2C_0_SDA_PTB3  ALT2
#define I2C_0_SCL_PTD2  ALT7
#define I2C_0_SDA_PTD3  ALT7
#define I2C_0_SCL_PTD8  ALT2
#define I2C_0_SDA_PTD9  ALT2

#define I2C_0_SIM_SCGC4 0x00000040u
#define I2C_1_SIM_SCGC4 0x00000020u
#define I2C_2_SIM_SCGC1 0x00000040u

#define I2C_1_SDA_PTE0  ALT6
#define I2C_1_SCL_PTE1  ALT6
#define I2C_1_SCL_PTC10 ALT2
#define I2C_1_SDA_PTC11 ALT2

#define I2C_2_SDA_PTA11 ALT5
#define I2C_2_SCL_PTA12 ALT5
#define I2C_2_SDA_PTA13 ALT5
#define I2C_2_SCL_PTA14 ALT5

#define I2C_IICIE_MASK 0x40u
#define I2C_MST_MODE_MASK 0x20u
#define I2C_TX_RX_SLCT_MASK 0x10u
#define I2C_TCF_MASK 0x80
#define I2C_RXAK_MASK 0x01
#define I2C_IICIF_MASK 0x02

#define TRANSMISSION_ON 1u
#define TRANSMISSION_OFF 0u
#define RECEPTION_ON 1u
#define RECEPTION_OFF 0u
#define READ_REG_MASK 0x01
#define READ_REG_END 204u
#define I2C_JOBHASH_DELETED 65535u
#define I2C_SEQ_OFFSET 1u
#define I2C_FIRST_JOB 1u
// #define I2C_CH1_CH2_INIT_CH0_F 200u
// #define I2C_CH2_INIT_CH0_F 201u
// #define I2C_CH1_INIT_CH0_F 202u
// #define I2C_CH2_CH0_INIT_CH1_F 203u
// #define I2C_CH2_INIT_CH1_F 204u
// #define I2C_CH0_INIT_CH1_F 205u
// #define I2C_CH1_CH0_INIT_CH2_F 206u
// #define I2C_CH1_INIT_CH2_F 207u
// #define I2C_CH0_INIT_CH2_F 208u

/********************** Macros End *************************/

/********************** Structs & Enums ********************/


/********************** Structs & Enums End *****************/

/********************** Typedefs ***************************/



/********************** Typedefs End************************/

/**********************Protoypes***************************/
FUNC(void,AUTOMATIC)vfnI2C_Init();
FUNC(void,AUTOMATIC)vfnSetBaudrate();
FUNC(void,AUTOMATIC)vfnI2C_InterruptsEnable();
FUNC(void,AUTOMATIC)vfnI2C_DMAEnable();
FUNC(void,AUTOMATIC)vfnI2C_WriteIB(I2C_channel_e ei2cChannel, uint8_t txData_lu8);
FUNC(void,AUTOMATIC)vfnI2C_Send();
FUNC(void,AUTOMATIC)vfnI2C_SetSMB(I2C_channel_e ei2cChannel);
FUNC(void,AUTOMATIC)vfnI2C_Read();
FUNC(void,AUTOMATIC)vfnI2C_JobDecode(I2C_JobType_t jobID_lt,I2C_channel_e* i2cChannel_lePtr,I2C_Command_t* i2cCommand_ltPtr);
FUNC(void,AUTOMATIC)vfnGroupSequences();
I2C_JobType_t hashCode(I2C_JobType_t i2cJobID_lt );
I2C_Job_st* search(I2C_JobType_t i2cJobID_lt);
void insert(I2C_Job_st* tempInsertStrct_Ptr);
I2C_Job_st* delete(I2C_Job_st* tempDeleteStrct_Ptr);
FUNC(std_ReturnType,AUTOMATIC)stdfnSortJobsSeq();
FUNC(I2C_ChannelState_t,AUTOMATIC) u8fnI2C_GetChannelState(I2C_channel_e i2cChannel_e);
FUNC(I2C_StatusType_e,AUTOMATIC) efnI2C_GetStatus();
FUNC(void,AUTOMATIC)vfnI2C_TestSend();
FUNC(I2C_Err_t,AUTOMATIC)tfnGetError();
FUNC(void,AUTOMATIC)vfnInitSMBChannel(I2C_Channel_t i2cSMBChannel_lt);
/**********************Prototype End*************************/
#endif