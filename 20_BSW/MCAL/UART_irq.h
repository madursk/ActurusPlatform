#ifndef UART_IRQ_H
#define UART_IRQ_H

void UART3_IRQHandler();
typedef void (*UART3_IRQHandler_Ptr)(void);
#endif