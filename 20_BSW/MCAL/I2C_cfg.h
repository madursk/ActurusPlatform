#include "I2C_Types.h"

#define I2C_HW_UNITS 3u
#define I2C0 0u
#define I2C1 1u
#define I2C2 2u

#define I2C0_Channel STD_ON
#define I2C1_Channel STD_ON
#define I2C2_Channel STD_OFF

#define I2C0_SCL_DIVIDER_ICR 0x05u /*Bus clock is 20 MHz, assuming 20% error  24 MHZ*/
#define I2C0_MULT            0x04u /* (MUL * ICR(SCL DIVIDER) * 24 MHz) =  200 kHz*/

#define I2C1_SCL_DIVIDER_ICR 0x05u /*Bus clock is 20 MHz, assuming 20% error  24 MHZ*/
#define I2C1_MULT            0x04u /* (MUL * ICR(SCL DIVIDER) * 24 MHz) =  200 kHz*/

#define I2C2_SCL_DIVIDER_ICR 
#define I2C2_MULT            

#define I2C_INTERRUPT_H STD_ON
#define I2C_DMA_H STD_OFF


#define I2C_MAX_SEQ_NUMBER 15u
#define I2C_SEQ_JOBS_LENGTH 8u
#define I2C_TX_MAILBOX I2C_SEQ_JOBS_LENGTH
#define I2C_RX_MAILBOX I2C_SEQ_JOBS_LENGTH
#define I2C_MAX_JOBS ((uint16_t)I2C_MAX_SEQ_NUMBER * I2C_SEQ_JOBS_LENGTH)

#define I2C_SEQ_1_END 8u
#define I2C_SEQ_2_END 16u
#define I2C_SEQ_3_END 24u
#define I2C_SEQ_4_END 32u
#define I2C_SEQ_5_END 40u
#define I2C_SEQ_6_END 48u
#define I2C_SEQ_7_END 56u
#define I2C_SEQ_8_END 64u
#define I2C_SEQ_9_END 72u
#define I2C_SEQ_10_END 80u
#define I2C_SEQ_11_END 88u
#define I2C_SEQ_12_END 96u
#define I2C_SEQ_13_END 104u
#define I2C_SEQ_14_END 112u
#define I2C_SEQ_15_END 120u

FUNC_P2VAR(I2C_cfg_stPtr,AUTOMATIC) pfnI2C_SetCfg();
FUNC(std_ReturnType,AUTOMATIC)stdfnI2C_SetSequence(I2C_JobType_t jobID_lt);
FUNC(std_ReturnType,AUTOMATIC)stdfnI2C_GetSequence(I2C_SequenceType_Ptr i2cSeq_Ptr, I2C_SequenceType_t  i2cSeq_t);
FUNC(I2C_SequenceType_t,AUTOMATIC)tfnGetSequenceAvail(I2C_JobType_t jobID_lt);