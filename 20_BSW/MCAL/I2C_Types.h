#ifndef I2C_TYPES_HEADER_H
#define I2C_TYPES_HEADER_H

#include "Datatypes.h"

#define I2C_CFG_STRUCT_NULL_E   0x02
#define I2C_0_CFG_MISMATCH_E      0x03
#define I2C_1_CFG_MISMATCH_E      0x04
#define I2C_2_CFG_MISMATCH_E      0x05
#define I2C_0_PORT_PINS_CFG_MISMATCH_E 200u
#define I2C_1_PORT_PINS_CFG_MISMATCH_E 201u
#define I2C_2_PORT_PINS_CFG_MISMATCH_E 202u
#define I2C_0_SIM_CFG_MISMATCH_E 220u
#define I2C_1_SIM_CFG_MISMATCH_E 221u
#define I2C_2_SIM_CFG_MISMATCH_E 222u
#define I2C_0_NO_ACK_RX_E           223u

#define I2C_PARAM_CHANNEL_E     0x0A
#define I2C_PARAM_JOB_E         0x0B
#define I2C_PARAM_SEQ_E           0x0C
#define I2C_PARAM_LENGTH_E        0x0D
#define I2C_PARAM_UNIT_E          0x0E
#define I2C_E_PARAM_POINTER_E     0x10

#define I2C_SEQ_1 1u
#define I2C_SEQ_2 2u
#define I2C_SEQ_3 3u
#define I2C_SEQ_4 4u
#define I2C_SEQ_5 5u
#define I2C_SEQ_6 6u
#define I2C_SEQ_7 7u
#define I2C_SEQ_8 8u
#define I2C_SEQ_9 9u
#define I2C_SEQ_10 10u
#define I2C_SEQ_11 11u
#define I2C_SEQ_12 12u
#define I2C_SEQ_13 13u
#define I2C_SEQ_14 14u
#define I2C_SEQ_15 15u

/**********************Types for structs******************************/
typedef uint8_t I2C_Status_t;
typedef uint8_t I2C_Channel_t;
typedef uint8_t I2C_DataType_t;
typedef uint16_t I2C_JobType_t;
typedef uint8_t I2C_SlaveAddress_t;
typedef uint8_t I2C_Command_t;
typedef uint8_t I2C_SequenceLength_t;
typedef uint8_t I2C_SequenceType_t;
typedef uint8_t I2C_ChannelState_t;
/**********************Types for structs End***************************/
/**********************Enum & Struct Types****************************/
typedef enum
{
    I2C0,
    I2C1,
    I2C2

}I2C_channel_e;

 typedef struct 
 {
    uint8_t TXMODE     :1;
    uint8_t CHANNEL    :1;
    uint8_t RXMODE     :1;
    uint8_t TCF        :1;
    uint8_t IAAS       :1;
    uint8_t BUSBSSY    :1;
    uint8_t ARBL       :1;
    uint8_t RAM        :1;
    uint8_t SRW        :1;
    uint8_t IICIF      :1;
    uint8_t RXAK       :1;
    uint8_t RESERVED1  :5;

 }I2C_InterruptStatus_st;
 
 typedef struct
{
    uint8_t I2C_A1_cfg ;
    uint8_t I2C_C1_cfg ;
    uint8_t I2C_S_cfg ;
    uint8_t I2C_C2_cfg ;
    uint8_t I2C_FLT_cfg ;
    uint8_t I2C_RA_cfg ;
    uint8_t I2C_SMB_cfg ;
    uint8_t I2C_SLTH_cfg ;
    uint8_t I2C_SLTL_cfg ;

}I2C_cfg_st_t;

typedef enum
{
    I2C_UNINIT,
    I2C_IDLE,
    I2C_BUSY,
    I2C_FAILURE
    
}I2C_StatusType_e;

typedef enum
{
    I2C_JOB_OK,
    I2C_JOB_PENDING,
    I2C_JOB_FAILED,
    I2C_JOB_QUEUED

}I2C_JobResultType_e;

typedef enum 
{
    I2C_SEQ_OK,
    I2C_SEQ_PENDING,
    I2C_SEQ_FAILED,
    SPI_SEQ_CANCELLED

}I2C_SeqResultType_e;

typedef struct
{
    I2C_channel_e i2cChannel_e;
    I2C_JobType_t i2cJobID_t;
    I2C_Command_t i2cCommand_t;
}I2C_Job_st;



/**********************Enum & Struct Types End************************/

/**********************Aliases******************************/
typedef uint8_t I2C_Err_t;
typedef I2C_cfg_st_t* I2C_cfg_stPtr;
typedef I2C_SequenceType_t* I2C_SequenceType_Ptr;
typedef I2C_Job_st* I2C_Job_strct_Ptr;
/**********************Aliases End*************************/
#endif