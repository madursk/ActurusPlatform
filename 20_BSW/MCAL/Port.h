#ifndef PORT_H
#define PORT_H

#include "Port_Types.h"



typedef enum
{
    PORT_INIT = 0x0A,
    PORT_UNINIT =0x14,
    PORT_ERROR = 0x1E

}PORT_STATUS_E;
#define PORT_CFG_STRUCTURE_NULL_ERROR 0x02u
#define PORT_CFG_MISMATCH_PORTA_ERROR 0x03u
#define PORT_CFG_MISMATCH_PORTB_ERROR 0x04u
#define PORT_CFG_MISMATCH_PORTC_ERROR 0x05u
#define PORT_CFG_MISMATCH_PORTD_ERROR 0x06u
#define PORT_CFG_MISMATCH_PORTE_ERROR 0x07u

typedef uint8_t portStatus_u8;

FUNC(std_ReturnType,AUTOMATIC)stdfnPort_Init(void);


#endif