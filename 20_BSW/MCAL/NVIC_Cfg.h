#ifndef NVIC_CFG_H
#define NVIC_CFG_H

#include "NVIC_Types.h"

#define NVIC_ISER_SECTIONS_MASK 32u
#define NVIC_ISER_INDEX_MASK 32u
#define NVIC_ISER_0 0u
#define NVIC_ISER_1 1u
#define NVIC_ISER_2 2u
#define NVIC_CONFIGURED_IRQ_UNITS 2u

FUNC(std_ReturnType,AUTOMATIC)vfnSetConfig_NVIC();

#endif