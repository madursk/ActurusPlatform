
#include "Port_Cfg.h"


#if (PORTA == STD_ON)
VAR(PortRegisters_t,DEFAULT) cxPortALoadout;
#endif
#if(PORTB == STD_ON)
VAR(PortRegisters_t,DEFAULT) cxPortBLoadout;
#endif
#if(PORTC == STD_ON)
VAR(PortRegisters_t,DEFAULT) cxPortCLoadout;
#endif
#if(PORTD==STD_ON)
VAR(PortRegisters_t,DEFAULT) cxPortDLoadout;
#endif
#if(PORTE==STD_ON)
VAR(PortRegisters_t,DEFAULT) cxPortELoadout;
#endif

PortRegisters_t* cxPortCfgPtrVct[MAX_HWPORT_NUM] = 
    {
#if (PORTA == STD_ON)
        &cxPortALoadout,
#else
        0u,
#endif
#if(PORTB == STD_ON)
        cxPortBLoadout,
#else
        0u,
#endif
#if(PORTC == STD_ON)
        &cxPortCLoadout,
#else
        0u,
#endif
#if(PORTD==STD_ON)
        &cxPortDLoadout,
#else
        0u,
#endif
#if(PORTE==STD_ON)
        &cxPortELoadout
#else
        0u
#endif
    };

FUNC(std_ReturnType,AUTOMATIC) stdfnPort_SetCfg(void)
{
    std_ReturnType sResult = E_NOT_OK;
    for(uint8_t u8PortCounter = 0u; u8PortCounter <= MAX_HWPORT_NUM; u8PortCounter++)
    {
        if((uint32_t*)0u != cxPortCfgPtrVct[u8PortCounter])
        {
            switch (u8PortCounter)
            {
                case(PORTA_INDEX):
                {
                    sResult = E_OK;
#if(PORTA_PCR0==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR0 = PORTA_PCR0_CFG_VALUES;
#endif
#if(PORTA_PCR1==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR1 = PORTA_PCR1_CFG_VALUES;
#endif
#if(PORTA_PCR2==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR2 = PORTA_PCR2_CFG_VALUES;
#endif
#if(PORTA_PCR3==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR3 = PORTA_PCR3_CFG_VALUES;
#endif
#if(PORTA_PCR4==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR4 = PORTA_PCR4_CFG_VALUES;
#endif
#if(PORTA_PCR5==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR5 = PORTA_PCR5_CFG_VALUES;
#endif
#if(PORTA_PCR6==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR6 = PORTA_PCR6_CFG_VALUES;
#endif
#if(PORTA_PCR7==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR7 = PORTA_PCR7_CFG_VALUES;
#endif
#if(PORTA_PCR8==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR8 = PORTA_PCR8_CFG_VALUES;
#endif
#if(PORTA_PCR9==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR9 = PORTA_PCR9_CFG_VALUES;
#endif
#if(PORTA_PCR10==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR10= PORTA_PCR10_CFG_VALUES;
#endif
#if(PORTA_PCR11==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR11= PORTA_PCR11_CFG_VALUES;
#endif
#if(PORTA_PCR12==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR12= PORTA_PCR12_CFG_VALUES;
#endif
#if(PORTA_PCR13==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR13= PORTA_PCR13_CFG_VALUES;
#endif
#if(PORTA_PCR14==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR14= PORTA_PCR14_CFG_VALUES;
#endif
#if(PORTA_PCR15==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR15= PORTA_PCR15_CFG_VALUES;
#endif
#if(PORTA_PCR16==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR16= PORTA_PCR16_CFG_VALUES;
#endif
#if(PORTA_PCR17==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR17= PORTA_PCR17_CFG_VALUES;
#endif
#if(PORTA_PCR18==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR18= PORTA_PCR18_CFG_VALUES;
#endif
#if(PORTA_PCR19==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR18= PORTA_PCR19_CFG_VALUES;
#endif
#if(PORTA_PCR20==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR19 = PORTA_PCR20_CFG_VALUES;
#endif
#if(PORTA_PCR21==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR20 = PORTA_PCR21_CFG_VALUES;
#endif
#if(PORTA_PCR22==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR21 = PORTA_PCR22_CFG_VALUES;
#endif
#if(PORTA_PCR23==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR22= PORTA_PCR23_CFG_VALUES;
#endif
#if(PORTA_PCR24==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR23= PORTA_PCR24_CFG_VALUES;
#endif
#if(PORTA_PCR25==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR24= PORTA_PCR25_CFG_VALUES;
#endif
#if(PORTA_PCR26==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR25 = PORTA_PCR26_CFG_VALUES;
#endif
#if(PORTA_PCR27==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR26= PORTA_PCR27_CFG_VALUES;
#endif
#if(PORTA_PCR28==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR27= PORTA_PCR28_CFG_VALUES;
#endif
#if(PORTA_PCR29==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR28= PORTA_PCR29_CFG_VALUES;
#endif
#if(PORTA_PCR30==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR29= PORTA_PCR30_CFG_VALUES;
#endif
#if(PORTA_PCR31==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR29= PORTA_PCR31_CFG_VALUES;
#endif
#if(PORTA_PCRGPCLR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR30= PORTA_PCRGPCLR_CFG_VALUES;
#endif
#if(PORTA_PCRGPCHR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR31= PORTA_PCRGPCHR_CFG_VALUES;
#endif
#if(PORTA_ISFR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_GPCHR= PORTA_ISFR_CFG_VALUES;
#endif
#if(PORTA_DFER==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFER= PORTA_DFER_CFG_VALUES;
#endif
#if(PORTA_DFCR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFCR= PORTA_DFCR_CFG_VALUES;
#endif
#if(PORTA_DFCR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFWR = PORTA_DFWR_CFG_VALUES;
#endif
                }break;
                case(PORTB_INDEX):
                {
                    sResult = E_OK;
#if(PORTB_PCR0==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR0 = PORTB_PCR0_CFG_VALUES;
#endif
#if(PORTB_PCR1==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR1 = PORTB_PCR1_CFG_VALUES;
#endif
#if(PORTB_PCR2==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR2 = PORTB_PCR2_CFG_VALUES;
#endif
#if(PORTB_PCR3==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR3 = PORTB_PCR3_CFG_VALUES;
#endif
#if(PORTB_PCR4==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR4 = PORTB_PCR4_CFG_VALUES;
#endif
#if(PORTB_PCR5==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR5 = PORTB_PCR5_CFG_VALUES;
#endif
#if(PORTB_PCR6==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR6 = PORTB_PCR6_CFG_VALUES;
#endif
#if(PORTB_PCR7==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR7 = PORTB_PCR7_CFG_VALUES;
#endif
#if(PORTB_PCR8==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR8 = PORTB_PCR8_CFG_VALUES;
#endif
#if(PORTB_PCR9==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR9 = PORTB_PCR9_CFG_VALUES;
#endif
#if(PORTB_PCR10==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR10= PORTB_PCR10_CFG_VALUES;
#endif
#if(PORTB_PCR11==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR11= PORTB_PCR11_CFG_VALUES;
#endif
#if(PORTB_PCR12==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR12= PORTB_PCR12_CFG_VALUES;
#endif
#if(PORTB_PCR13==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR13= PORTB_PCR13_CFG_VALUES;
#endif
#if(PORTB_PCR14==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR14= PORTB_PCR14_CFG_VALUES;
#endif
#if(PORTB_PCR15==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR15= PORTB_PCR15_CFG_VALUES;
#endif
#if(PORTB_PCR16==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR16= PORTB_PCR16_CFG_VALUES;
#endif
#if(PORTB_PCR17==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR17= PORTB_PCR17_CFG_VALUES;
#endif
#if(PORTB_PCR18==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR18= PORTB_PCR18_CFG_VALUES;
#endif
#if(PORTA_PCR19==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR18= PORTB_PCR19_CFG_VALUES;
#endif
#if(PORTB_PCR20==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR19 = PORTB_PCR20_CFG_VALUES;
#endif
#if(PORTB_PCR21==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR20 = PORTB_PCR21_CFG_VALUES;
#endif
#if(PORTB_PCR22==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR21 = PORTB_PCR22_CFG_VALUES;
#endif
#if(PORTB_PCR23==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR22= PORTB_PCR23_CFG_VALUES;
#endif
#if(PORTB_PCR24==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR23= PORTB_PCR24_CFG_VALUES;
#endif
#if(PORTB_PCR25==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR24= PORTB_PCR25_CFG_VALUES;
#endif
#if(PORTB_PCR26==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR25 = PORTB_PCR26_CFG_VALUES;
#endif
#if(PORTB_PCR27==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR26= PORTB_PCR27_CFG_VALUES;
#endif
#if(PORTB_PCR28==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR27= PORTB_PCR28_CFG_VALUES;
#endif
#if(PORTB_PCR29==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR28= PORTB_PCR29_CFG_VALUES;
#endif
#if(PORTB_PCR30==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR29= PORTB_PCR30_CFG_VALUES;
#endif
#if(PORTB_PCR31==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR29= PORTB_PCR31_CFG_VALUES;
#endif
#if(PORTB_PCRGPCLR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR30= PORTB_PCRGPCLR_CFG_VALUES;
#endif
#if(PORTB_PCRGPCHR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR31= PORTB_PCRGPCHR_CFG_VALUES;
#endif
#if(PORTB_ISFR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_GPCHR= PORTB_ISFR_CFG_VALUES;
#endif
#if(PORTB_DFER==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFER= PORTB_DFER_CFG_VALUES;
#endif
#if(PORTB_DFCR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFCR= PORTB_DFCR_CFG_VALUES;
#endif
#if(PORTB_DFCR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFWR = PORTB_DFWR_CFG_VALUES;
#endif
                }break;
                case(PORTC_INDEX):
                {
                    sResult = E_OK;
#if(PORTC_PCR0==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR0 = PORTC_PCR0_CFG_VALUES;
#endif
#if(PORTC_PCR1==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR1 = PORTC_PCR1_CFG_VALUES;
#endif
#if(PORTC_PCR2==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR2 = PORTC_PCR2_CFG_VALUES;
#endif
#if(PORTC_PCR3==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR3 = PORTC_PCR3_CFG_VALUES;
#endif
#if(PORTC_PCR4==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR4 = PORTC_PCR4_CFG_VALUES;
#endif
#if(PORTC_PCR5==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR5 = PORTC_PCR5_CFG_VALUES;
#endif
#if(PORTC_PCR6==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR6 = PORTC_PCR6_CFG_VALUES;
#endif
#if(PORTC_PCR7==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR7 = PORTC_PCR7_CFG_VALUES;
#endif
#if(PORTC_PCR8==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR8 = PORTC_PCR8_CFG_VALUES;
#endif
#if(PORTC_PCR9==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR9 = PORTC_PCR9_CFG_VALUES;
#endif
#if(PORTC_PCR10==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR10= PORTC_PCR10_CFG_VALUES;
#endif
#if(PORTC_PCR11==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR11= PORTC_PCR11_CFG_VALUES;
#endif
#if(PORTC_PCR12==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR12= PORTC_PCR12_CFG_VALUES;
#endif
#if(PORTC_PCR13==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR13= PORTC_PCR13_CFG_VALUES;
#endif
#if(PORTC_PCR14==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR14= PORTC_PCR14_CFG_VALUES;
#endif
#if(PORTC_PCR15==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR15= PORTC_PCR15_CFG_VALUES;
#endif
#if(PORTC_PCR16==STD_ON)
                    // cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR16= PORTC_PCR16_CFG_VALUES;
                    // PORTC_PCR16_R|= cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR16;
                    PORTC_PCR16_R=PORT_PCR_MUX(3u);
#endif
#if(PORTC_PCR17==STD_ON)
                    // cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR17= PORTC_PCR17_CFG_VALUES;
                    // PORTC_PCR17_R|= cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR17;
                    PORTC_PCR17_R=PORT_PCR_MUX(3u);
#endif
#if(PORTC_PCR18==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR18= PORTC_PCR18_CFG_VALUES;
#endif
#if(PORTC_PCR19==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR18= PORTC_PCR19_CFG_VALUES;
#endif
#if(PORTC_PCR20==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR19 = PORTC_PCR20_CFG_VALUES;
#endif
#if(PORTC_PCR21==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR20 = PORTC_PCR21_CFG_VALUES;
#endif
#if(PORTC_PCR22==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR21 = PORTC_PCR22_CFG_VALUES;
#endif
#if(PORTC_PCR23==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR22= PORTC_PCR23_CFG_VALUES;
#endif
#if(PORTC_PCR24==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR23= PORTC_PCR24_CFG_VALUES;
#endif
#if(PORTC_PCR25==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR24= PORTC_PCR25_CFG_VALUES;
#endif
#if(PORTC_PCR26==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR25 = PORTC_PCR26_CFG_VALUES;
#endif
#if(PORTC_PCR27==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR26= PORTC_PCR27_CFG_VALUES;
#endif
#if(PORTC_PCR28==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR27= PORTC_PCR28_CFG_VALUES;
#endif
#if(PORTC_PCR29==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR28= PORTC_PCR29_CFG_VALUES;
#endif
#if(PORTC_PCR30==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR29= PORTC_PCR30_CFG_VALUES;
#endif
#if(PORTC_PCR31==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR29= PORTC_PCR31_CFG_VALUES;
#endif
#if(PORTC_PCRGPCLR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR30= PORTC_PCRGPCLR_CFG_VALUES;
#endif
#if(PORTC_PCRGPCHR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR31= PORTC_PCRGPCHR_CFG_VALUES;
#endif
#if(PORTC_ISFR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_GPCHR= PORTC_ISFR_CFG_VALUES;
#endif
#if(PORTC_DFER==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFER= PORTC_DFER_CFG_VALUES;
#endif
#if(PORTC_DFCR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFCR= PORTC_DFCR_CFG_VALUES;
#endif
#if(PORTC_DFCR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFWR = PORTC_DFWR_CFG_VALUES;
#endif
                }break;
                case(PORTD_INDEX):
                {
                    sResult = E_OK;
#if(PORTD_PCR0==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR0 = PORTC_PCR0_CFG_VALUES;
#endif
#if(PORTD_PCR1==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR1 = PORTC_PCR1_CFG_VALUES;
#endif
#if(PORTD_PCR2==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR2 = PORTC_PCR2_CFG_VALUES;
#endif
#if(PORTD_PCR3==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR3 = PORTC_PCR3_CFG_VALUES;
#endif
#if(PORTD_PCR4==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR4 = PORTC_PCR4_CFG_VALUES;
#endif
#if(PORTD_PCR5==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR5 = PORTC_PCR5_CFG_VALUES;
#endif
#if(PORTD_PCR6==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR6 = PORTC_PCR6_CFG_VALUES;
#endif
#if(PORTD_PCR7==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR7 = PORTC_PCR7_CFG_VALUES;
#endif
#if(PORTD_PCR8==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR8 = PORTC_PCR8_CFG_VALUES;
#endif
#if(PORTD_PCR9==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR9 = PORTC_PCR9_CFG_VALUES;
#endif
#if(PORTD_PCR10==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR10= PORTC_PCR10_CFG_VALUES;
#endif
#if(PORTD_PCR11==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR11= PORTC_PCR11_CFG_VALUES;
#endif
#if(PORTD_PCR12==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR12= PORTC_PCR12_CFG_VALUES;
#endif
#if(PORTD_PCR13==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR13= PORTC_PCR13_CFG_VALUES;
#endif
#if(PORTD_PCR14==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR14= PORTC_PCR14_CFG_VALUES;
#endif
#if(PORTD_PCR15==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR15= PORTC_PCR15_CFG_VALUES;
#endif
#if(PORTD_PCR16==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR16= PORTC_PCR16_CFG_VALUES;
#endif
#if(PORTD_PCR17==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR17= PORTC_PCR17_CFG_VALUES;
#endif
#if(PORTD_PCR18==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR18= PORTC_PCR18_CFG_VALUES;
#endif
#if(PORTD_PCR19==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR18= PORTC_PCR19_CFG_VALUES;
#endif
#if(PORTD_PCR20==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR19 = PORTC_PCR20_CFG_VALUES;
#endif
#if(PORTD_PCR21==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR20 = PORTC_PCR21_CFG_VALUES;
#endif
#if(PORTD_PCR22==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR21 = PORTC_PCR22_CFG_VALUES;
#endif
#if(PORTD_PCR23==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR22= PORTC_PCR23_CFG_VALUES;
#endif
#if(PORTD_PCR24==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR23= PORTC_PCR24_CFG_VALUES;
#endif
#if(PORTD_PCR25==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR24= PORTC_PCR25_CFG_VALUES;
#endif
#if(PORTD_PCR26==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR25 = PORTC_PCR26_CFG_VALUES;
#endif
#if(PORTD_PCR27==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR26= PORTC_PCR27_CFG_VALUES;
#endif
#if(PORTD_PCR28==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR27= PORTC_PCR28_CFG_VALUES;
#endif
#if(PORTD_PCR29==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR28= PORTC_PCR29_CFG_VALUES;
#endif
#if(PORTD_PCR30==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR29= PORTC_PCR30_CFG_VALUES;
#endif
#if(PORTD_PCR31==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR29= PORTC_PCR31_CFG_VALUES;
#endif
#if(PORTD_PCRGPCLR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR30= PORTC_PCRGPCLR_CFG_VALUES;
#endif
#if(PORTD_PCRGPCHR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR31= PORTC_PCRGPCHR_CFG_VALUES;
#endif
#if(PORTD_ISFR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_GPCHR= PORTC_ISFR_CFG_VALUES;
#endif
#if(PORTD_DFER==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFER= PORTC_DFER_CFG_VALUES;
#endif
#if(PORTD_DFCR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFCR= PORTC_DFCR_CFG_VALUES;
#endif
#if(PORTD_DFCR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFWR = PORTC_DFWR_CFG_VALUES;
#endif
                }break;
                case(PORTE_INDEX):
                {
                    sResult = E_OK;
#if(PORTE_PCR0==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR0 = PORTC_PCR0_CFG_VALUES;
#endif
#if(PORTE_PCR1==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR1 = PORTC_PCR1_CFG_VALUES;
#endif
#if(PORTE_PCR2==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR2 = PORTC_PCR2_CFG_VALUES;
#endif
#if(PORTE_PCR3==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR3 = PORTC_PCR3_CFG_VALUES;
#endif
#if(PORTE_PCR4==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR4 = PORTC_PCR4_CFG_VALUES;
#endif
#if(PORTE_PCR5==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR5 = PORTC_PCR5_CFG_VALUES;
#endif
#if(PORTE_PCR6==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR6 = PORTC_PCR6_CFG_VALUES;
#endif
#if(PORTE_PCR7==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR7 = PORTC_PCR7_CFG_VALUES;
#endif
#if(PORTE_PCR8==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR8 = PORTC_PCR8_CFG_VALUES;
#endif
#if(PORTE_PCR9==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR9 = PORTC_PCR9_CFG_VALUES;
#endif
#if(PORTE_PCR10==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR10= PORTC_PCR10_CFG_VALUES;
#endif
#if(PORTE_PCR11==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR11= PORTC_PCR11_CFG_VALUES;
#endif
#if(PORTE_PCR12==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR12= PORTC_PCR12_CFG_VALUES;
#endif
#if(PORTE_PCR13==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR13= PORTC_PCR13_CFG_VALUES;
#endif
#if(PORTE_PCR14==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR14= PORTC_PCR14_CFG_VALUES;
#endif
#if(PORTE_PCR15==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR15= PORTC_PCR15_CFG_VALUES;
#endif
#if(PORTE_PCR16==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR16= PORTC_PCR16_CFG_VALUES;
#endif
#if(PORTE_PCR17==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR17= PORTC_PCR17_CFG_VALUES;
#endif
#if(PORTE_PCR18==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR18= PORTC_PCR18_CFG_VALUES;
#endif
#if(PORTE_PCR19==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR19= PORTC_PCR19_CFG_VALUES;
#endif
#if(PORTE_PCR20==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR20 = PORTC_PCR20_CFG_VALUES;
#endif
#if(PORTE_PCR21==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR21 = PORTC_PCR21_CFG_VALUES;
#endif
#if(PORTE_PCR22==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR22 = PORTC_PCR22_CFG_VALUES;
#endif
#if(PORTE_PCR23==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR23= PORTC_PCR23_CFG_VALUES;
#endif
#if(PORTE_PCR24==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR24= PORTE_PCR24_CFG_VALUES;
                    PORTE_PCR24_R |= cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR24;
#endif
#if(PORTE_PCR25==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR25= PORTE_PCR25_CFG_VALUES;
                    PORTE_PCR25_R |= cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR25;
#endif
#if(PORTE_PCR26==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR26 = PORTC_PCR26_CFG_VALUES;
#endif
#if(PORTE_PCR27==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR27= PORTC_PCR27_CFG_VALUES;
#endif
#if(PORTE_PCR28==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR28= PORTC_PCR28_CFG_VALUES;
#endif
#if(PORTE_PCR29==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR29= PORTC_PCR29_CFG_VALUES;
#endif
#if(PORTE_PCR30==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR30= PORTC_PCR30_CFG_VALUES;
#endif
#if(PORTE_PCR31==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCR31= PORTC_PCR31_CFG_VALUES;
#endif
#if(PORTE_PCRGPCLR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_PCRGPCLR= PORTC_PCRGPCLR_CFG_VALUES;
#endif
#if(PORTE_PCRGPCHR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_GPCHR= PORTC_PCRGPCHR_CFG_VALUES;
#endif
#if(PORTE_ISFR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_ISFR= PORTC_ISFR_CFG_VALUES;
#endif
#if(PORTE_DFER==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFCR= PORTC_DFER_CFG_VALUES;
#endif
#if(PORTE_DFCR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFWR= PORTC_DFCR_CFG_VALUES;
#endif
#if(PORTE_DFWR==STD_ON)
                    cxPortCfgPtrVct[u8PortCounter]->u32PORT_DFWR = PORTC_DFWR_CFG_VALUES;
#endif
                }break;
            default:
                break;
            }/*end switch*/
        }/*end if*/
    }/*end for*/
    return sResult;
}
FUNC(PortRegisters_t*,AUTOMATIC) cxfnPort_GetCfg(void)
{
    return cxPortCfgPtrVct;
}