#include "NVIC.h"
#include "NVIC_Cfg.h"
#include "DiagM.h"

#define PRINT(x)     vfnDiagnosticConsole_Print(MCAL,NVIC,x)


IRQ_ConfigType_Ptr irqCfgStructs_aPtr[NVIC_CONFIGURED_IRQ_UNITS];

FUNC(void,AUTOMATIC)vfnNVIC_Init()
{
    vfnSetConfig_NVIC();
    for(IRQ_t unitCounter_lu8=NULL; unitCounter_lu8 < NVIC_CONFIGURED_IRQ_UNITS; unitCounter_lu8++)
    {
        vfnNVIC_SetEnableReg(irqCfgStructs_aPtr[unitCounter_lu8]);
    }
}

FUNC(void,AUTOMATIC)vfnNVIC_SetEnableReg(IRQ_ConfigType_Ptr irqCfg_tPtr)
{
    IRQ_t irqn_t;
    IRQ_t iser_t;
    if(NULLPTR == irqCfg_tPtr )
    {

    }
    else
    {    
        iser_t = (irqCfg_tPtr-> IRQ_u8)/(NVIC_ISER_SECTIONS_MASK);
        irqn_t = (irqCfg_tPtr-> IRQ_u8)%(NVIC_ISER_INDEX_MASK);
        
        if(NVIC_ISER_0 == iser_t)
        {
            NVICICER0 |= IRQ_SELECTOR_MASK << irqn_t;
            NVICISER0 |= IRQ_SELECTOR_MASK << irqn_t;
        }
        else if(NVIC_ISER_1 == iser_t)
        {
            NVICICER1 |= IRQ_SELECTOR_MASK << irqn_t;
            NVICISER1 |= IRQ_SELECTOR_MASK << irqn_t;
        }
        else if(NVIC_ISER_2 == iser_t)
        {
            NVICICER2 |= IRQ_SELECTOR_MASK << irqn_t;
            NVICISER2 |= IRQ_SELECTOR_MASK << irqn_t;
        }
        else
        {

        }
    }
    // switch(irqCfg_tPtr->Priority_u8)
    // {

    // }
}

FUNC(void,AUTOMATIC)vfnNVIC_ClearEnableReg()
{

}

FUNC(void,AUTOMATIC)vfnNVIC_SetPendingReq()
{

}

FUNC(void,AUTOMATIC)vfnNVIC_ClearPendingReq()
{
    
}

FUNC(void,AUTOMATIC)vfnNVIC_ActiveBitReg()
{
    
}

FUNC(void,AUTOMATIC)vfnNVIC_SetIRQPriority()
{

}

FUNC(void,AUTOMATIC)vfnNVIC_SoftwareIRQ()
{

}