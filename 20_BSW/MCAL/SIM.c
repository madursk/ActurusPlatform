#include "SIM.h"
#include "SIM_Cfg.h"
#include "DiagM.h"

#define PRINT(x)     vfnDiagnosticConsole_Print(MCAL,SIM,x)

FUNC(std_ReturnType,AUTOMATIC) stdfnSIM_Init(void)
{
    std_ReturnType sResult = E_NOT_OK;
    sResult = stdfnSIM_SetCfg();
    return sResult;
}