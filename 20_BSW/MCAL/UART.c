/*******************************************/
 /* UART.c
 *
 *  Created on: Sep 22, 2018
 *  Author: Madursk
 *  Brief: Functions of UART communication
 */
/*****************************************/
#include "UART.h"
#include "UART_Cfg.h"
#include "UART_irq.h"
#include "DiagM.h"

#define PRINT(x)     vfnDiagnosticConsole_Print(MCAL,UART,x)
/************************VARIABLES*****************************/
 UART_Status_st UART_3_Flags_st;
 char_t  cRxBuffer[60];
extern VAR(UARTCommMode_t,DEFAULT) cxUartComFlgs;
extern VAR(char_t,DEFAULT) s8UartTxBuffer[UART_BUFFER_LENGTH];
extern UartHwUnits_t* cxUartDriversPtrVct[MAX_HWUART_NUM];
/************************************************************/

/**
 * Name stdfnUART_Init
 * @brief Initialization function for the UART drivers
 * configuration is loaded from the dynamic files
 * and result is returned
 * 
 * @param None
 * 
 * @return Std_ReturnType, returns E_NOT_OK if
 * one of the drivers is not loaded correctly
 * E_OK if every selected drivers initialized correctly
 */
FUNC(std_ReturnType, AUTOMATIC) stdfnUART_Init(void)
{
    std_ReturnType stdResult = E_NOT_OK;
    /* Set the Configuration for UART */
   stdResult = stdfnUart_setCfg();
   return stdResult;
}

/**
 * Name stdfnUART3Read
 * @brief Read Function for UART 3 channel
 * that stores the global buffer to a local one
 * 
 * @param None
 * 
 * @return Std_ReturnType, returns E_NOT_OK if
 * if the nothing is being received
 * implemention  needs to be updated
 */
FUNC(std_ReturnType,AUTOMATIC)stdfnUART3Read( char_t lcRxBuffer[], uint8_t lu8RxBufferSize)
{
	std_ReturnType lu8State = E_NOT_OK;
	uint8_t lu8IndexCounter = 0;
	if( 1 == UART_3_Flags_st.RXMODE)
	{
		do
		{
			lcRxBuffer[lu8IndexCounter] = cRxBuffer[lu8IndexCounter];
			
		}while(lu8IndexCounter++ <= lu8RxBufferSize  );
		UART_3_Flags_st.RXMODE= 0;
		lu8State = E_OK;
	}
	
	return lu8State;
}/*End Read UART Port method*/
 

/**
 * Name stdfnUART3Writed
 * @brief Write Function for UART 3 channel
 * that stores the lobal buffer to a gocal one
 * 
 * @param None
 * 
 * @return Std_ReturnType, returns E_OK if
 * if the nothing is being transmitted
 * implemention  needs to be updated
 */
FUNC(std_ReturnType,AUTOMATIC)stdfnUART3Write(char_t lcTxBuffer[])
{
	std_ReturnType lu8State = E_NOT_OK;
	uint8_t u8MailboxInterchangeCounter = 0;
	
	if(0 == cxUartComFlgs.u8TransmissionMode)
	{
        /*Since there is no transmission proceed to write in buffer*/
		do
		{
			s8UartTxBuffer[u8MailboxInterchangeCounter] = lcTxBuffer[u8MailboxInterchangeCounter];
			
		}while('\0' != lcTxBuffer[u8MailboxInterchangeCounter++]);
        /*Flag to wait until transmission finishes*/
		cxUartComFlgs.u8TransmissionMode = 1;
        /*Buffer is full start transmission*/
		UART3_C2 |= UART_C2_TIE_MASK;
	 }
	 return lu8State;
}/*End Write UART Port method */

/**
 * Name bfnGetUartTxConfirmationFlag
 * @brief Function that returns transmission state
 * of the UART interruption
 * 
 * @param None
 * 
 * @return boolean, returns FALSE if
 * Transmission is in progress
 * returns TRUE if  transmission done
 */
FUNC(boolean, AUTOMATIC) bfnGetUartTxConfirmationFlag(void)
{
    boolean bReturnValue;

    if(1u == cxUartComFlgs.u8TransmissionMode)
    {
        bReturnValue = FALSE;
    }
    else if(0u == cxUartComFlgs.u8TransmissionMode)
    {
        bReturnValue = TRUE;
    }

    return bReturnValue;
}

FUNC_P2VAR(UartHwUnits_t,AUTOMATIC) cxGetUartDriversPtr(uint8_t index)
{
    return cxUartDriversPtrVct[index];
}