#include "UART_Cfg.h"


VAR(UARTCommMode_t,DEFAULT) cxUartComFlgs;
VAR(char_t,DEFAULT) s8UartTxBuffer[UART_BUFFER_LENGTH];
VAR(uint8_t,DEFAULT) u8UartTxBuffer[UART_BUFFER_LENGTH];
VAR(char_t,DEFAULT) s8UartRxBuffer[UART_BUFFER_LENGTH];
VAR(uint8_t,DEFAULT) u8UartTxBufferIndex;
VAR(uint8_t,DEFAULT) u8UartRxBufferIndex;

void UART3_IRQHandler()
{

	if(((UART3_S1 & UART_S1_TDRE_MASK)) && (1==cxUartComFlgs.u8TransmissionMode))
	{
		 if(('\0'!= s8UartTxBuffer[u8UartTxBufferIndex]))
		 {
			 UART3_D = s8UartTxBuffer[u8UartTxBufferIndex];
	 		u8UartTxBufferIndex++;
			
		 }
		 else
		 {
			 u8UartTxBufferIndex = 0;
			 UART3_C2 ^= UART_C2_TIE_MASK;
			 cxUartComFlgs.u8TransmissionMode = 0;
		 }
	}	 
	if(UART3_S1 & UART_S1_RDRF_MASK)
	{
		 
			s8UartRxBuffer[u8UartRxBufferIndex] = UART3_D;
			
		if(UART_END_RECEPTION == s8UartRxBuffer[u8UartRxBufferIndex] )
		{
			u8UartRxBufferIndex = 0;
			cxUartComFlgs.u8ReceptionMode = 1;
		}
		else
		{
			u8UartRxBufferIndex++;
		}
	
		
	}
}

