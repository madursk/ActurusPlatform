#ifndef SIM_CFG_H
#define SIM_CFG_H

#include "SIM_Types.h"

#if(SOPT1_CFG==STD_ON)
#define RAM_SIZE 0x0000B000u
#define SOPT1_CFG_MASK (RAM_SIZE)
#endif

#if(SOPT2_CFG==STD_ON)
#define TRACECLK_MASK 0x00001000u
#define SOPT2_CFG_MASK (TRACECLK_MASK)
#endif

#if(SOPT4_CFG==STD_ON)
#define RAM_SIZE 0x0000B000u
#define SOPT4_CFG_MASK (RAM_SIZE)
#endif

#if(SOPT5_CFG==STD_ON)
#define RAM_SIZE 0x0000B000u
#define SOPT5_CFG_MASK (RAM_SIZE)
#endif

#if(SOPT7_CFG==STD_ON)
#define RAM_SIZE 0x0000B000u
#define SOPT7_CFG_MASK (RAM_SIZE)
#endif

#if(SOPT1CG_CFG==STD_ON)
#define RAM_SIZE 0x0000B000u
#define SOPT1CG_CFG_MASK (RAM_SIZE)
#endif

#if(CLKDIV1_CFG==STD_ON)
#define OUTDIV3_CFG_MASK (0x00100000u)
#define OUTDIV4_CFG_MASK (0x00010000u)
#define CLKDIV1_CFG_MASK (OUTDIV3_CFG_MASK|OUTDIV4_CFG_MASK)
#endif

#if(CLKDIV2_CFG==STD_ON)
#define RAM_SIZE 0x0000B000u
#define CLKDIV2_CFG_MASK (RAM_SIZE)
#endif

#if(FCFG1_CFG==STD_ON)
#define RAM_SIZE 0x0000B000u
#define FCFG1_CFG_MASK (RAM_SIZE)
#endif

#if(FCFG2_CFG==STD_ON)
#define RAM_SIZE 0x0000B000u
#define FCFG2_CFG_MASK (RAM_SIZE)
#endif

#if(SCGC1_CFG==STD_ON)
#define RAM_SIZE 0x0000B000u
#define SCGC1_CFG_MASK (RAM_SIZE)
#endif

#if(SCGC2_CFG==STD_ON)
#define RAM_SIZE 0x0000B000u
#define SCGC2_CFG_MASK (RAM_SIZE)
#endif

#if(SCGC3_CFG==STD_ON)
#define RAM_SIZE 0x0000B000u
#define SCGC3_CFG_MASK (RAM_SIZE)
#endif

#if(SCGC4_CFG==STD_ON)
#define UART3_CFG_MASK 0x00002000u
#define I2C0_CFG_MASK  0x00000040u
#define I2C1_CFG_MASK  0x00000080u
#define SCGC4_CFG_MASK (UART3_CFG_MASK|I2C0_CFG_MASK|I2C1_CFG_MASK)
#endif

#if(SCGC5_CFG==STD_ON)
#define PORTCCLK_CFG_MASK (0x00000800u)
#define PORTECLK_CFG_MASK (0x00002000u)
#define SCGC5_CFG_MASK (PORTCCLK_CFG_MASK|PORTECLK_CFG_MASK)
#endif

#if(SCGC6_CFG==STD_ON)
#define RAM_SIZE 0x0000B000u
#define SCGC6_CFG_MASK (RAM_SIZE)
#endif

#if(SCGC7_CFG==STD_ON)
#define RAM_SIZE 0x0000B000u
#define SCGC7_CFG_MASK (RAM_SIZE)
#endif

FUNC(void,AUTOMATIC)vfnSIM_SetSCGC1(uint32_t u32SCGC1Mask);
FUNC(void,AUTOMATIC)vfnSIM_SetSCGC2(uint32_t u32SCGC2Mask);
FUNC(void,AUTOMATIC)vfnSIM_SetSCGC3(uint32_t u32SCGC3Mask);
FUNC(void,AUTOMATIC)vfnSIM_SetSCGC4(uint32_t u32SCGC4Mask);
FUNC(void,AUTOMATIC)vfnSIM_SetSCGC5(uint32_t u32SCGC5Mask);
FUNC(void,AUTOMATIC)vfnSIM_SetSCGC6(uint32_t u32SCGC6Mask);
FUNC(void,AUTOMATIC)vfnSIM_SetSCGC7(uint32_t u32SCGC7Mask);
FUNC(std_ReturnType,AUTOMATIC)stdfnSIM_SetCfg(void);

#endif