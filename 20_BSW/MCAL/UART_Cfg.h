#ifndef UART_CFG_H
#define UART_CFG_H
#include "UART_Types.h"

#define UART_BUFFER_LENGTH (67u)
#define UART_END_TRANSMISSION (13u) /*You can change this to the end you desire */
#define UART_END_RECEPTION (13u)

#define UART0_CHANNEL STD_OFF
#define UART1_CHANNEL STD_OFF
#define UART2_CHANNEL STD_OFF
#define UART3_CHANNEL STD_ON
#define UART4_CHANNEL STD_OFF
#define UART5_CHANNEL STD_OFF

#if(UART0_CHANNEL==STD_ON)
#define UART0_LOOPS_MASK (0u)
#define UART0_UARTSWAI_MASK (0u)
#define UART0_RSRC_MASK (0u)
#define UART0_M_MASK (0u)
#define UART0_WAKE_MASK (0u)
#define UART0_ILT_MASK (0u)
#define UART0_PE_MASK (0u)
#define UART0_PT_MASK (0u)
#define UART0_C1_CFG_VALUES ()

#endif

#if(UART1_CHANNEL==STD_ON)
#define UART1_LOOPS_MASK (0u)
#define UART1_UARTSWAI_MASK (0u)
#define UART1_RSRC_MASK (0u)
#define UART1_M_MASK (0u)
#define UART1_WAKE_MASK (0u)
#define UART1_ILT_MASK (0u)
#define UART1_PE_MASK (0u)
#define UART1_PT_MASK (0u)
#define UART1_C1_CFG_VALUES ()
#endif

#if(UART2_CHANNEL==STD_ON)
#define UART2_LOOPS_MASK (0u)
#define UART2_UARTSWAI_MASK (0u)
#define UART2_RSRC_MASK (0u)
#define UART2_M_MASK (0u)
#define UART2_WAKE_MASK (0u)
#define UART2_ILT_MASK (0u)
#define UART2_PE_MASK (0u)
#define UART2_PT_MASK (0u)
#define UART2_C1_CFG_VALUES ()
#endif

#if(UART3_CHANNEL==STD_ON)
#define UART3_LOOPS_MASK (0u)
#define UART3_UARTSWAI_MASK (0u)
#define UART3_RSRC_MASK (0u)
#define UART3_M_MASK (0u)
#define UART3_WAKE_MASK (0u)
#define UART3_ILT_MASK (0u)
#define UART3_PE_MASK (0u)
#define UART3_PT_MASK (0u)
#define UART3_C1_CFG_VALUES (UART3_LOOPS_MASK|UART3_UARTSWAI_MASK\
                            |UART3_RSRC_MASK|UART3_M_MASK|UART3_WAKE_MASK\
                            |UART3_ILT_MASK|UART3_PE_MASK|UART3_PT_MASK)
#define UART3_TIE_MASK (0u)
#define UART3_TCIE_MASK (0u)
#define UART3_RIE_MASK (0u)
#define UART3_ILIE_MASK (0u)
#define UART3_TE_MASK (0x8u)
#define UART3_RE_MASK (0x4u)
#define UART3_RWU_MASK (0u)
#define UART3_SBK_MASK (0u)
#define UART3_C2_CFG_VALUES (UART3_TIE_MASK|UART3_TCIE_MASK\
                            |UART3_RIE_MASK|UART3_ILIE_MASK|UART3_TE_MASK\
                            |UART3_RE_MASK|UART3_RWU_MASK|UART3_SBK_MASK)
#define UART3_LBKDIE_MASK (0u)
#define UART3_RXEDGIE_MASK (0u)
#define UART3_SBNS_MASK (0u)
#define UART3_SBR_MASK (1u)
#define UART3_BDH_CFG_VALUES (UART3_LBKDIE_MASK|UART3_RXEDGIE_MASK\
                            |UART3_SBNS_MASK|UART3_SBR_MASK)
#define UART3_BDL_MASK (121u)
#define UART3_BDL_CFG_VALUES (UART3_BDL_MASK)
#endif


#if(UART4_CHANNEL==STD_ON)
#define UART4_LOOPS_MASK (0u)
#define UART4_UARTSWAI_MASK (0u)
#define UART4_RSRC_MASK (0u)
#define UART4_M_MASK (0u)
#define UART4_WAKE_MASK (0u)
#define UART4_ILT_MASK (0u)
#define UART4_PE_MASK (0u)
#define UART4_PT_MASK (0u)
#define UART4_C1_CFG_VALUES ()
#endif

#if(UART5_CHANNEL==STD_ON)
#define UART5_LOOPS_MASK (0u)
#define UART5_UARTSWAI_MASK (0u)
#define UART5_RSRC_MASK (0u)
#define UART5_M_MASK (0u)
#define UART5_WAKE_MASK (0u)
#define UART5_ILT_MASK (0u)
#define UART5_PE_MASK (0u)
#define UART5_PT_MASK (0u)
#define UART5_C1_CFG_VALUES ()
#endif

FUNC(std_ReturnType, AUTOMATIC) stdfnUart_setCfg(void);
#endif