#ifndef NVIC_TYPES_H
#define NVIC_TYPES_H

#include "Datatypes.h"
#define IRQ_SELECTOR_MASK 1u

typedef struct
{
    uint8_t IRQ_u8;
    uint8_t Priority_u8;

}IRQ_ConfigType_t;

typedef IRQ_ConfigType_t* IRQ_ConfigType_Ptr;
typedef uint8_t IRQ_t;
#endif