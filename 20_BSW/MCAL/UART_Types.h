#ifndef UART_TYPES_H
#define UART_TYPES_H

#include "Datatypes.h"
#include "Compiler.h"
#include "Compiler_cfg.h"
#include "ProjectDefinitions.h"
typedef struct UARTMemMap
{
    uint8_t BDH;
    uint8_t BDL;
    uint8_t C1;
    uint8_t C2;
    uint8_t S1;
    uint8_t S2;
    uint8_t C3;
    uint8_t D;
    uint8_t MA1;
    uint8_t MA2;
    uint8_t C4;
    uint8_t C5;
    uint8_t ED;
    uint8_t MODEM;
    uint8_t IR;
    uint8_t RESERVED_0[1];
    uint8_t PFIFO;
    uint8_t CFIFO;
    uint8_t SFIFO;
    uint8_t TWFIFO;
    uint8_t TCFIFO;
    uint8_t RWFIFO;
    uint8_t RCFIFO;
    uint8_t RESERVED_1[1];
    uint8_t C7816;
    uint8_t IE7816;
    uint8_t IS7816;
    union
    {
    uint8_t WP7816T0;
    uint8_t WP7816T1;
    };
    
    uint8_t WN7816;
    uint8_t WF7816;
    uint8_t ET7826;
    uint8_t TL7816;
}volatile *UARTMemMapPtr;

#define UART0_REG_BASE_PTR   ((UARTMemMapPtr)0x4006A000)

#define UART0_BDH_REG(base)       ((base)->BDH)
#define UART0_BDL_REG(base)       ((base)->BDL)
#define UART0_C1_REG(base)       ((base)->C1)
#define UART0_C2_REG(base)       ((base)->C2)
#define UART0_S1_REG(base)       ((base)->S1)
#define UART0_S2_REG(base)       ((base)->S2)
#define UART0_C3_REG(base)       ((base)->C3)
#define UART0_D_REG(base)       ((base)->D)
#define UART0_C4_REG(base)       ((base)->C4)

#define UART0_BDH                UART0_BDH_REG(UART0_REG_BASE_PTR)
#define UART0_BDL                UART0_BDL_REG(UART0_REG_BASE_PTR)
#define UART0_C1                UART0_C1_REG(UART0_REG_BASE_PTR)
#define UART0_C2                UART0_C2_REG(UART0_REG_BASE_PTR)
#define UART0_S1                UART0_S1_REG(UART0_REG_BASE_PTR)
#define UART0_S2                UART0_S2_REG(UART0_REG_BASE_PTR)
#define UART0_C3                UART0_C3_REG(UART0_REG_BASE_PTR)
#define UART0_D                UART0_D_REG(UART0_REG_BASE_PTR)
#define UART0_C4                UART0_C4_REG(UART0_REG_BASE_PTR)

#define UART1_REG_BASE_PTR   ((UARTMemMapPtr)0x4006B000)

#define UART1_BDH_REG(base)       ((base)->BDH)
#define UART1_BDL_REG(base)       ((base)->BDL)
#define UART1_C1_REG(base)       ((base)->C1)
#define UART1_C2_REG(base)       ((base)->C2)
#define UART1_S1_REG(base)       ((base)->S1)
#define UART1_S2_REG(base)       ((base)->S2)
#define UART1_C3_REG(base)       ((base)->C3)
#define UART1_D_REG(base)       ((base)->D)
#define UART1_C4_REG(base)       ((base)->C4)

#define UART1_BDH                UART1_BDH_REG(UART1_REG_BASE_PTR)
#define UART1_BDL                UART1_BDL_REG(UART1_REG_BASE_PTR)
#define UART1_C1                UART1_C1_REG(UART1_REG_BASE_PTR)
#define UART1_C2                UART1_C2_REG(UART1_REG_BASE_PTR)
#define UART1_S1                UART1_S1_REG(UART1_REG_BASE_PTR)
#define UART1_S2                UART1_S2_REG(UART1_REG_BASE_PTR)
#define UART1_C3                UART1_C3_REG(UART1_REG_BASE_PTR)
#define UART1_D                UART1_D_REG(UART1_REG_BASE_PTR)
#define UART1_C4                UART1_C4_REG(UART1_REG_BASE_PTR)

#define UART2_REG_BASE_PTR   ((UARTMemMapPtr)0x4006C000)

#define UART2_BDH_REG(base)       ((base)->BDH)
#define UART2_BDL_REG(base)       ((base)->BDL)
#define UART2_C1_REG(base)       ((base)->C1)
#define UART2_C2_REG(base)       ((base)->C2)
#define UART2_S1_REG(base)       ((base)->S1)
#define UART2_S2_REG(base)       ((base)->S2)
#define UART2_C3_REG(base)       ((base)->C3)
#define UART2_D_REG(base)       ((base)->D)
#define UART2_C4_REG(base)       ((base)->C4)

#define UART2_BDH                UART2_BDH_REG(UART2_REG_BASE_PTR)
#define UART2_BDL                UART2_BDL_REG(UART2_REG_BASE_PTR)
#define UART2_C1                UART2_C1_REG(UART2_REG_BASE_PTR)
#define UART2_C2                UART2_C2_REG(UART2_REG_BASE_PTR)
#define UART2_S1                UART2_S1_REG(UART2_REG_BASE_PTR)
#define UART2_S2                UART2_S2_REG(UART2_REG_BASE_PTR)
#define UART2_C3                UART2_C3_REG(UART2_REG_BASE_PTR)
#define UART2_D                UART2_D_REG(UART2_REG_BASE_PTR)
#define UART2_C4                UART2_C4_REG(UART2_REG_BASE_PTR)

#define UART3_REG_BASE_PTR   ((UARTMemMapPtr)0x4006D000)

#define UART3_BDH_REG(base)       ((base)->BDH)
#define UART3_BDL_REG(base)       ((base)->BDL)
#define UART3_C1_REG(base)       ((base)->C1)
#define UART3_C2_REG(base)       ((base)->C2)
#define UART3_S1_REG(base)       ((base)->S1)
#define UART3_S2_REG(base)       ((base)->S2)
#define UART3_C3_REG(base)       ((base)->C3)
#define UART3_D_REG(base)       ((base)->D)
#define UART3_C4_REG(base)       ((base)->C4)

#define UART3_BDH                UART3_BDH_REG(UART3_REG_BASE_PTR)
#define UART3_BDL                UART3_BDL_REG(UART3_REG_BASE_PTR)
#define UART3_C1                UART3_C1_REG(UART3_REG_BASE_PTR)
#define UART3_C2                UART3_C2_REG(UART3_REG_BASE_PTR)
#define UART3_S1                UART3_S1_REG(UART3_REG_BASE_PTR)
#define UART3_S2                UART3_S2_REG(UART3_REG_BASE_PTR)
#define UART3_C3                UART3_C3_REG(UART3_REG_BASE_PTR)
#define UART3_D                UART3_D_REG(UART3_REG_BASE_PTR)
#define UART3_C4                UART3_C4_REG(UART3_REG_BASE_PTR)

#define UART4_REG_BASE_PTR   ((UARTMemMapPtr)0x400EA000)

#define UART4_BDH_REG(base)       ((base)->BDH)
#define UART4_BDL_REG(base)       ((base)->BDL)
#define UART4_C1_REG(base)       ((base)->C1)
#define UART4_C2_REG(base)       ((base)->C2)
#define UART4_S1_REG(base)       ((base)->S1)
#define UART4_S2_REG(base)       ((base)->S2)
#define UART4_C3_REG(base)       ((base)->C3)
#define UART4_D_REG(base)       ((base)->D)
#define UART4_C4_REG(base)       ((base)->C4)

#define UART4_BDH                UART4_BDH_REG(UART4_REG_BASE_PTR)
#define UART4_BDL                UART4_BDL_REG(UART4_REG_BASE_PTR)
#define UART4_C1                UART4_C1_REG(UART4_REG_BASE_PTR)
#define UART4_C2                UART4_C2_REG(UART4_REG_BASE_PTR)
#define UART4_S1                UART4_S1_REG(UART4_REG_BASE_PTR)
#define UART4_S2                UART4_S2_REG(UART4_REG_BASE_PTR)
#define UART4_C3                UART4_C3_REG(UART4_REG_BASE_PTR)
#define UART4_D                UART4_D_REG(UART4_REG_BASE_PTR)
#define UART4_C4                UART4_C4_REG(UART4_REG_BASE_PTR)

#define UART5_REG_BASE_PTR   ((UARTMemMapPtr)0x400EB000)

#define UART5_BDH_REG(base)       ((base)->BDH)
#define UART5_BDL_REG(base)       ((base)->BDL)
#define UART5_C1_REG(base)       ((base)->C1)
#define UART5_C2_REG(base)       ((base)->C2)
#define UART5_S1_REG(base)       ((base)->S1)
#define UART5_S2_REG(base)       ((base)->S2)
#define UART5_C3_REG(base)       ((base)->C3)
#define UART5_D_REG(base)       ((base)->D)
#define UART5_C4_REG(base)       ((base)->C4)

#define UART5_BDH                UART5_BDH_REG(UART5_REG_BASE_PTR)
#define UART5_BDL                UART5_BDL_REG(UART5_REG_BASE_PTR)
#define UART5_C1                UART5_C1_REG(UART5_REG_BASE_PTR)
#define UART5_C2                UART5_C2_REG(UART5_REG_BASE_PTR)
#define UART5_S1                UART5_S1_REG(UART5_REG_BASE_PTR)
#define UART5_S2                UART5_S2_REG(UART5_REG_BASE_PTR)
#define UART5_C3                UART5_C3_REG(UART5_REG_BASE_PTR)
#define UART5_D                UART5_D_REG(UART5_REG_BASE_PTR)
#define UART5_C4                UART5_C4_REG(UART5_REG_BASE_PTR)

#define UART_S1_TDRE_MASK 0x80u
#define UART_C2_TIE_MASK                         0x80u
#define UART_S1_RDRF_MASK                        0x20u
#define UART_C2_TE_MASK                          0x8u
#define UART_C2_RE_MASK                          0x4u
#define UART_C2_RIE_MASK                         0x20u
#define UART_BDH_SBR_MASK                        0x1Fu
#define UART_BDH_SBR_SHIFT                       0
#define UART_BDH_SBR(x)                          (((uint8_t)(((uint8_t)(x))<<UART_BDH_SBR_SHIFT))&UART_BDH_SBR_MASK)
#define UART_C4_BRFA_MASK                        0x1Fu
#define UART_C4_BRFA_SHIFT                       0
#define UART_C4_BRFA(x)                          (((uint8_t)(((uint8_t)(x))<<UART_C4_BRFA_SHIFT))&UART_C4_BRFA_MASK)

#define UART_BDL_SBR_MASK                        0xFFu
#define UART_BDL_SBR_SHIFT                       0
#define UART_BDL_SBR(x)                          (((uint8_t)(((uint8_t)(x))<<UART_BDL_SBR_SHIFT))&UART_BDL_SBR_MASK)

typedef std_ReturnType (*stdFnWriteBuffer)(char_t lcTxBuffer[]);

typedef struct 
 {
    uint8_t u8TransmissionMode     :1;
    uint8_t u8ReceptionMode        :1;
    uint8_t                        :6;
   
 }UARTCommMode_t;

typedef enum
{
 UART_UNINIT,
 UART_INIT,
 UART_ERR
}DriverStatus_t;

 typedef struct
 {
    uint8_t u8DriverId;
    DriverStatus_t cxStatus;
    std_ReturnType* stdFnReadBufferPtr;
    stdFnWriteBuffer Write;
 }UartHwUnits_t;

#define UART_HW_UNIT0 (0u)
#define UART_HW_UNIT1 (1u)
#define UART_HW_UNIT2 (2u)
#define UART_HW_UNIT3 (3u)
#define UART_HW_UNIT4 (4u)
#define UART_HW_UNIT5 (5u)


#endif