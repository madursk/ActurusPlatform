#ifndef PORT_TYPES_H
#define PORT_TYPES_H

#include "Datatypes.h"
#include "Compiler.h"
#include "Compiler_cfg.h"

typedef struct PORT_MemMap {
  uint32_t PCR[32];                                /**< Pin Control Register n, array offset: 0x0, array step: 0x4 */
  uint32_t GPCLR;                                  /**< Global Pin Control Low Register, offset: 0x80 */
  uint32_t GPCHR;                                  /**< Global Pin Control High Register, offset: 0x84 */
  uint8_t RESERVED_0[24];
  uint32_t ISFR;                                   /**< Interrupt Status Flag Register, offset: 0xA0 */
  uint8_t RESERVED_1[28];
  uint32_t DFER;                                   /**< Digital Filter Enable Register, offset: 0xC0 */
  uint32_t DFCR;                                   /**< Digital Filter Clock Register, offset: 0xC4 */
  uint32_t DFWR;                                   /**< Digital Filter Width Register, offset: 0xC8 */
} volatile *PORT_MemMapPtr;

#define PORT_PCR_REG(base,index)                 ((base)->PCR[index])
#define PORT_GPCLR_REG(base)                     ((base)->GPCLR)
#define PORT_GPCHR_REG(base)                     ((base)->GPCHR)
#define PORT_ISFR_REG(base)                      ((base)->ISFR)
#define PORT_DFER_REG(base)                      ((base)->DFER)
#define PORT_DFCR_REG(base)                      ((base)->DFCR)
#define PORT_DFWR_REG(base)                      ((base)->DFWR)

/** Peripheral PORTA base pointer */
#define PORTA_BASE_PTR                           ((PORT_MemMapPtr)0x40049000u)
/** Peripheral PORTB base pointer */
#define PORTB_BASE_PTR                           ((PORT_MemMapPtr)0x4004A000u)
/** Peripheral PORTC base pointer */
#define PORTC_BASE_PTR                           ((PORT_MemMapPtr)0x4004B000u)
/** Peripheral PORTD base pointer */
#define PORTD_BASE_PTR                           ((PORT_MemMapPtr)0x4004C000u)
/** Peripheral PORTE base pointer */
#define PORTE_BASE_PTR                           ((PORT_MemMapPtr)0x4004D000u)
/* PORT - Register instance definitions */
/* PORTA */
/* PORT - Register array accessors */
#define PORTA_PCR(index)                         PORT_PCR_REG(PORTA_BASE_PTR,index)
#define PORTB_PCR(index)                         PORT_PCR_REG(PORTB_BASE_PTR,index)
#define PORTC_PCR(index)                         PORT_PCR_REG(PORTC_BASE_PTR,index)
#define PORTD_PCR(index)                         PORT_PCR_REG(PORTD_BASE_PTR,index)
#define PORTE_PCR(index)                         PORT_PCR_REG(PORTE_BASE_PTR,index)


#define PORTA_PCR0_R                               PORT_PCR_REG(PORTA_BASE_PTR,0)
#define PORTA_PCR1_R                               PORT_PCR_REG(PORTA_BASE_PTR,1)
#define PORTA_PCR2_R                               PORT_PCR_REG(PORTA_BASE_PTR,2)
#define PORTA_PCR3_R                               PORT_PCR_REG(PORTA_BASE_PTR,3)
#define PORTA_PCR4_R                               PORT_PCR_REG(PORTA_BASE_PTR,4)
#define PORTA_PCR5_R                               PORT_PCR_REG(PORTA_BASE_PTR,5)
#define PORTA_PCR6_R                               PORT_PCR_REG(PORTA_BASE_PTR,6)
#define PORTA_PCR7_R                               PORT_PCR_REG(PORTA_BASE_PTR,7)
#define PORTA_PCR8_R                               PORT_PCR_REG(PORTA_BASE_PTR,8)
#define PORTA_PCR9_R                               PORT_PCR_REG(PORTA_BASE_PTR,9)
#define PORTA_PCR10_R                              PORT_PCR_REG(PORTA_BASE_PTR,10)
#define PORTA_PCR11_R                              PORT_PCR_REG(PORTA_BASE_PTR,11)
#define PORTA_PCR12_R                              PORT_PCR_REG(PORTA_BASE_PTR,12)
#define PORTA_PCR13_R                              PORT_PCR_REG(PORTA_BASE_PTR,13)
#define PORTA_PCR14_R                              PORT_PCR_REG(PORTA_BASE_PTR,14)
#define PORTA_PCR15_R                              PORT_PCR_REG(PORTA_BASE_PTR,15)
#define PORTA_PCR16_R                              PORT_PCR_REG(PORTA_BASE_PTR,16)
#define PORTA_PCR17_R                              PORT_PCR_REG(PORTA_BASE_PTR,17)
#define PORTA_PCR18_R                              PORT_PCR_REG(PORTA_BASE_PTR,18)
#define PORTA_PCR19_R                              PORT_PCR_REG(PORTA_BASE_PTR,19)
#define PORTA_PCR20_R                              PORT_PCR_REG(PORTA_BASE_PTR,20)
#define PORTA_PCR21_R                              PORT_PCR_REG(PORTA_BASE_PTR,21)
#define PORTA_PCR22_R                              PORT_PCR_REG(PORTA_BASE_PTR,22)
#define PORTA_PCR23_R                              PORT_PCR_REG(PORTA_BASE_PTR,23)
#define PORTA_PCR24_R                              PORT_PCR_REG(PORTA_BASE_PTR,24)
#define PORTA_PCR25_R                              PORT_PCR_REG(PORTA_BASE_PTR,25)
#define PORTA_PCR26_R                              PORT_PCR_REG(PORTA_BASE_PTR,26)
#define PORTA_PCR27_R                              PORT_PCR_REG(PORTA_BASE_PTR,27)
#define PORTA_PCR28_R                              PORT_PCR_REG(PORTA_BASE_PTR,28)
#define PORTA_PCR29_R                              PORT_PCR_REG(PORTA_BASE_PTR,29)
#define PORTA_PCR30_R                              PORT_PCR_REG(PORTA_BASE_PTR,30)
#define PORTA_PCR31_R                              PORT_PCR_REG(PORTA_BASE_PTR,31)
#define PORTA_GPCLR_R                              PORT_GPCLR_REG(PORTA_BASE_PTR)
#define PORTA_GPCHR_R                              PORT_GPCHR_REG(PORTA_BASE_PTR)
#define PORTA_ISFR_R                               PORT_ISFR_REG(PORTA_BASE_PTR)
#define PORTA_DFER_R                               PORT_DFER_REG(PORTA_BASE_PTR)
#define PORTA_DFCR_R                               PORT_DFCR_REG(PORTA_BASE_PTR)
#define PORTA_DFWR_R                               PORT_DFWR_REG(PORTA_BASE_PTR)

/* PORTB */
#define PORTB_PCR0_R                               PORT_PCR_REG(PORTB_BASE_PTR,0)
#define PORTB_PCR1_R                               PORT_PCR_REG(PORTB_BASE_PTR,1)
#define PORTB_PCR2_R                               PORT_PCR_REG(PORTB_BASE_PTR,2)
#define PORTB_PCR3_R                               PORT_PCR_REG(PORTB_BASE_PTR,3)
#define PORTB_PCR4_R                               PORT_PCR_REG(PORTB_BASE_PTR,4)
#define PORTB_PCR5_R                               PORT_PCR_REG(PORTB_BASE_PTR,5)
#define PORTB_PCR6_R                               PORT_PCR_REG(PORTB_BASE_PTR,6)
#define PORTB_PCR7_R                               PORT_PCR_REG(PORTB_BASE_PTR,7)
#define PORTB_PCR8_R                               PORT_PCR_REG(PORTB_BASE_PTR,8)
#define PORTB_PCR9_R                               PORT_PCR_REG(PORTB_BASE_PTR,9)
#define PORTB_PCR10_R                             PORT_PCR_REG(PORTB_BASE_PTR,10)
#define PORTB_PCR11_R                             PORT_PCR_REG(PORTB_BASE_PTR,11)
#define PORTB_PCR12_R                             PORT_PCR_REG(PORTB_BASE_PTR,12)
#define PORTB_PCR13_R                             PORT_PCR_REG(PORTB_BASE_PTR,13)
#define PORTB_PCR14_R                             PORT_PCR_REG(PORTB_BASE_PTR,14)
#define PORTB_PCR15_R                             PORT_PCR_REG(PORTB_BASE_PTR,15)
#define PORTB_PCR16_R                             PORT_PCR_REG(PORTB_BASE_PTR,16)
#define PORTB_PCR17_R                             PORT_PCR_REG(PORTB_BASE_PTR,17)
#define PORTB_PCR18_R                             PORT_PCR_REG(PORTB_BASE_PTR,18)
#define PORTB_PCR19_R                             PORT_PCR_REG(PORTB_BASE_PTR,19)
#define PORTB_PCR20_R                             PORT_PCR_REG(PORTB_BASE_PTR,20)
#define PORTB_PCR21_R                             PORT_PCR_REG(PORTB_BASE_PTR,21)
#define PORTB_PCR22_R                             PORT_PCR_REG(PORTB_BASE_PTR,22)
#define PORTB_PCR23_R                             PORT_PCR_REG(PORTB_BASE_PTR,23)
#define PORTB_PCR24_R                             PORT_PCR_REG(PORTB_BASE_PTR,24)
#define PORTB_PCR25_R                             PORT_PCR_REG(PORTB_BASE_PTR,25)
#define PORTB_PCR26_R                             PORT_PCR_REG(PORTB_BASE_PTR,26)
#define PORTB_PCR27_R                             PORT_PCR_REG(PORTB_BASE_PTR,27)
#define PORTB_PCR28_R                             PORT_PCR_REG(PORTB_BASE_PTR,28)
#define PORTB_PCR29_R                             PORT_PCR_REG(PORTB_BASE_PTR,29)
#define PORTB_PCR30_R                             PORT_PCR_REG(PORTB_BASE_PTR,30)
#define PORTB_PCR31_R                             PORT_PCR_REG(PORTB_BASE_PTR,31)
#define PORTB_GPCLR_R                             PORT_GPCLR_REG(PORTB_BASE_PTR)
#define PORTB_GPCHR_R                             PORT_GPCHR_REG(PORTB_BASE_PTR)
#define PORTB_ISFR_R                              PORT_ISFR_REG(PORTB_BASE_PTR)
#define PORTB_DFER_R                               PORT_DFER_REG(PORTB_BASE_PTR)
#define PORTB_DFCR_R                               PORT_DFCR_REG(PORTB_BASE_PTR)
#define PORTB_DFWR_R                               PORT_DFWR_REG(PORTB_BASE_PTR)

/* PORTC */
#define PORTC_PCR0_R                               PORT_PCR_REG(PORTC_BASE_PTR,0)
#define PORTC_PCR1_R                               PORT_PCR_REG(PORTC_BASE_PTR,1)
#define PORTC_PCR2_R                               PORT_PCR_REG(PORTC_BASE_PTR,2)
#define PORTC_PCR3_R                               PORT_PCR_REG(PORTC_BASE_PTR,3)
#define PORTC_PCR4_R                               PORT_PCR_REG(PORTC_BASE_PTR,4)
#define PORTC_PCR5_R                               PORT_PCR_REG(PORTC_BASE_PTR,5)
#define PORTC_PCR6_R                               PORT_PCR_REG(PORTC_BASE_PTR,6)
#define PORTC_PCR7_R                               PORT_PCR_REG(PORTC_BASE_PTR,7)
#define PORTC_PCR8_R                               PORT_PCR_REG(PORTC_BASE_PTR,8)
#define PORTC_PCR9_R                               PORT_PCR_REG(PORTC_BASE_PTR,9)
#define PORTC_PCR10_R                             PORT_PCR_REG(PORTC_BASE_PTR,10)
#define PORTC_PCR11_R                             PORT_PCR_REG(PORTC_BASE_PTR,11)
#define PORTC_PCR12_R                             PORT_PCR_REG(PORTC_BASE_PTR,12)
#define PORTC_PCR13_R                             PORT_PCR_REG(PORTC_BASE_PTR,13)
#define PORTC_PCR14_R                             PORT_PCR_REG(PORTC_BASE_PTR,14)
#define PORTC_PCR15_R                             PORT_PCR_REG(PORTC_BASE_PTR,15)
#define PORTC_PCR16_R                             PORT_PCR_REG(PORTC_BASE_PTR,16)
#define PORTC_PCR17_R                             PORT_PCR_REG(PORTC_BASE_PTR,17)
#define PORTC_PCR18_R                             PORT_PCR_REG(PORTC_BASE_PTR,18)
#define PORTC_PCR19_R                             PORT_PCR_REG(PORTC_BASE_PTR,19)
#define PORTC_PCR20_R                             PORT_PCR_REG(PORTC_BASE_PTR,20)
#define PORTC_PCR21_R                             PORT_PCR_REG(PORTC_BASE_PTR,21)
#define PORTC_PCR22_R                             PORT_PCR_REG(PORTC_BASE_PTR,22)
#define PORTC_PCR23_R                             PORT_PCR_REG(PORTC_BASE_PTR,23)
#define PORTC_PCR24_R                             PORT_PCR_REG(PORTC_BASE_PTR,24)
#define PORTC_PCR25_R                             PORT_PCR_REG(PORTC_BASE_PTR,25)
#define PORTC_PCR26_R                             PORT_PCR_REG(PORTC_BASE_PTR,26)
#define PORTC_PCR27_R                             PORT_PCR_REG(PORTC_BASE_PTR,27)
#define PORTC_PCR28_R                             PORT_PCR_REG(PORTC_BASE_PTR,28)
#define PORTC_PCR29_R                             PORT_PCR_REG(PORTC_BASE_PTR,29)
#define PORTC_PCR30_R                             PORT_PCR_REG(PORTC_BASE_PTR,30)
#define PORTC_PCR31_R                             PORT_PCR_REG(PORTC_BASE_PTR,31)
#define PORTC_GPCLR_R                             PORT_GPCLR_REG(PORTC_BASE_PTR)
#define PORTC_GPCHR_R                             PORT_GPCHR_REG(PORTC_BASE_PTR)
#define PORTC_ISFR_R                               PORT_ISFR_REG(PORTC_BASE_PTR)
#define PORTC_DFER_R                               PORT_DFER_REG(PORTC_BASE_PTR)
#define PORTC_DFCR_R                               PORT_DFCR_REG(PORTC_BASE_PTR)
#define PORTC_DFWR_R                               PORT_DFWR_REG(PORTC_BASE_PTR)
/* PORTD */
#define PORTD_PCR0_R                               PORT_PCR_REG(PORTD_BASE_PTR,0)
#define PORTD_PCR1_R                               PORT_PCR_REG(PORTD_BASE_PTR,1)
#define PORTD_PCR2_R                               PORT_PCR_REG(PORTD_BASE_PTR,2)
#define PORTD_PCR3_R                               PORT_PCR_REG(PORTD_BASE_PTR,3)
#define PORTD_PCR4_R                               PORT_PCR_REG(PORTD_BASE_PTR,4)
#define PORTD_PCR5_R                               PORT_PCR_REG(PORTD_BASE_PTR,5)
#define PORTD_PCR6_R                               PORT_PCR_REG(PORTD_BASE_PTR,6)
#define PORTD_PCR7_R                               PORT_PCR_REG(PORTD_BASE_PTR,7)
#define PORTD_PCR8_R                               PORT_PCR_REG(PORTD_BASE_PTR,8)
#define PORTD_PCR9_R                               PORT_PCR_REG(PORTD_BASE_PTR,9)
#define PORTD_PCR10_R                              PORT_PCR_REG(PORTD_BASE_PTR,10)
#define PORTD_PCR11_R                              PORT_PCR_REG(PORTD_BASE_PTR,11)
#define PORTD_PCR12_R                              PORT_PCR_REG(PORTD_BASE_PTR,12)
#define PORTD_PCR13_R                              PORT_PCR_REG(PORTD_BASE_PTR,13)
#define PORTD_PCR14_R                              PORT_PCR_REG(PORTD_BASE_PTR,14)
#define PORTD_PCR15_R                              PORT_PCR_REG(PORTD_BASE_PTR,15)
#define PORTD_PCR16_R                              PORT_PCR_REG(PORTD_BASE_PTR,16)
#define PORTD_PCR17_R                              PORT_PCR_REG(PORTD_BASE_PTR,17)
#define PORTD_PCR18_R                              PORT_PCR_REG(PORTD_BASE_PTR,18)
#define PORTD_PCR19_R                              PORT_PCR_REG(PORTD_BASE_PTR,19)
#define PORTD_PCR20_R                              PORT_PCR_REG(PORTD_BASE_PTR,20)
#define PORTD_PCR21_R                              PORT_PCR_REG(PORTD_BASE_PTR,21)
#define PORTD_PCR22_R                              PORT_PCR_REG(PORTD_BASE_PTR,22)
#define PORTD_PCR23_R                              PORT_PCR_REG(PORTD_BASE_PTR,23)
#define PORTD_PCR24_R                              PORT_PCR_REG(PORTD_BASE_PTR,24)
#define PORTD_PCR25_R                              PORT_PCR_REG(PORTD_BASE_PTR,25)
#define PORTD_PCR26_R                              PORT_PCR_REG(PORTD_BASE_PTR,26)
#define PORTD_PCR27_R                              PORT_PCR_REG(PORTD_BASE_PTR,27)
#define PORTD_PCR28_R                              PORT_PCR_REG(PORTD_BASE_PTR,28)
#define PORTD_PCR29_R                              PORT_PCR_REG(PORTD_BASE_PTR,29)
#define PORTD_PCR30_R                              PORT_PCR_REG(PORTD_BASE_PTR,30)
#define PORTD_PCR31_R                              PORT_PCR_REG(PORTD_BASE_PTR,31)
#define PORTD_GPCLR_R                              PORT_GPCLR_REG(PORTD_BASE_PTR)
#define PORTD_GPCHR_R                              PORT_GPCHR_REG(PORTD_BASE_PTR)
#define PORTD_ISFR_R                               PORT_ISFR_REG(PORTD_BASE_PTR)
#define PORTD_DFER_R                               PORT_DFER_REG(PORTD_BASE_PTR)
#define PORTD_DFCR_R                               PORT_DFCR_REG(PORTD_BASE_PTR)
#define PORTD_DFWR_R                               PORT_DFWR_REG(PORTD_BASE_PTR)
/* PORTE */
#define PORTE_PCR0_R                               PORT_PCR_REG(PORTE_BASE_PTR,0)
#define PORTE_PCR1_R                               PORT_PCR_REG(PORTE_BASE_PTR,1)
#define PORTE_PCR2_R                               PORT_PCR_REG(PORTE_BASE_PTR,2)
#define PORTE_PCR3_R                               PORT_PCR_REG(PORTE_BASE_PTR,3)
#define PORTE_PCR4_R                               PORT_PCR_REG(PORTE_BASE_PTR,4)
#define PORTE_PCR5_R                               PORT_PCR_REG(PORTE_BASE_PTR,5)
#define PORTE_PCR6_R                               PORT_PCR_REG(PORTE_BASE_PTR,6)
#define PORTE_PCR7_R                               PORT_PCR_REG(PORTE_BASE_PTR,7)
#define PORTE_PCR8_R                               PORT_PCR_REG(PORTE_BASE_PTR,8)
#define PORTE_PCR9_R                               PORT_PCR_REG(PORTE_BASE_PTR,9)
#define PORTE_PCR10_R                              PORT_PCR_REG(PORTE_BASE_PTR,10)
#define PORTE_PCR11_R                              PORT_PCR_REG(PORTE_BASE_PTR,11)
#define PORTE_PCR12_R                              PORT_PCR_REG(PORTE_BASE_PTR,12)
#define PORTE_PCR13_R                              PORT_PCR_REG(PORTE_BASE_PTR,13)
#define PORTE_PCR14_R                              PORT_PCR_REG(PORTE_BASE_PTR,14)
#define PORTE_PCR15_R                              PORT_PCR_REG(PORTE_BASE_PTR,15)
#define PORTE_PCR16_R                              PORT_PCR_REG(PORTE_BASE_PTR,16)
#define PORTE_PCR17_R                              PORT_PCR_REG(PORTE_BASE_PTR,17)
#define PORTE_PCR18_R                              PORT_PCR_REG(PORTE_BASE_PTR,18)
#define PORTE_PCR19_R                              PORT_PCR_REG(PORTE_BASE_PTR,19)
#define PORTE_PCR20_R                              PORT_PCR_REG(PORTE_BASE_PTR,20)
#define PORTE_PCR21_R                              PORT_PCR_REG(PORTE_BASE_PTR,21)
#define PORTE_PCR22_R                              PORT_PCR_REG(PORTE_BASE_PTR,22)
#define PORTE_PCR23_R                              PORT_PCR_REG(PORTE_BASE_PTR,23)
#define PORTE_PCR24_R                              PORT_PCR_REG(PORTE_BASE_PTR,24)
#define PORTE_PCR25_R                              PORT_PCR_REG(PORTE_BASE_PTR,25)
#define PORTE_PCR26_R                              PORT_PCR_REG(PORTE_BASE_PTR,26)
#define PORTE_PCR27_R                              PORT_PCR_REG(PORTE_BASE_PTR,27)
#define PORTE_PCR28_R                              PORT_PCR_REG(PORTE_BASE_PTR,28)
#define PORTE_PCR29_R                              PORT_PCR_REG(PORTE_BASE_PTR,29)
#define PORTE_PCR30_R                              PORT_PCR_REG(PORTE_BASE_PTR,30)
#define PORTE_PCR31_R                              PORT_PCR_REG(PORTE_BASE_PTR,31)
#define PORTE_GPCLR_R                              PORT_GPCLR_REG(PORTE_BASE_PTR)
#define PORTE_GPCHR_R                              PORT_GPCHR_REG(PORTE_BASE_PTR)
#define PORTE_ISFR_R                               PORT_ISFR_REG(PORTE_BASE_PTR)
#define PORTE_DFER_R                               PORT_DFER_REG(PORTE_BASE_PTR)
#define PORTE_DFCR_R                               PORT_DFCR_REG(PORTE_BASE_PTR)
#define PORTE_DFWR_R                               PORT_DFWR_REG(PORTE_BASE_PTR)

#define PORT_PCR_MUX_MASK                        0x700u
#define PORT_PCR_MUX_SHIFT                       8
#define PORT_PCR_MUX(x)                          (((uint32_t)(((uint32_t)(x))<<PORT_PCR_MUX_SHIFT))&PORT_PCR_MUX_MASK)

typedef struct 
{
    uint32_t u32PORT_PCR0;
    uint32_t u32PORT_PCR1;
    uint32_t u32PORT_PCR2;
    uint32_t u32PORT_PCR3;
    uint32_t u32PORT_PCR4;
    uint32_t u32PORT_PCR5;
    uint32_t u32PORT_PCR6;
    uint32_t u32PORT_PCR7;
    uint32_t u32PORT_PCR8;
    uint32_t u32PORT_PCR9;
    uint32_t u32PORT_PCR10;
    uint32_t u32PORT_PCR11;
    uint32_t u32PORT_PCR12;
    uint32_t u32PORT_PCR13;
    uint32_t u32PORT_PCR14;
    uint32_t u32PORT_PCR15;
    uint32_t u32PORT_PCR16;
    uint32_t u32PORT_PCR17;
    uint32_t u32PORT_PCR18;
    uint32_t u32PORT_PCR19;
    uint32_t u32PORT_PCR20;
    uint32_t u32PORT_PCR21;
    uint32_t u32PORT_PCR22;
    uint32_t u32PORT_PCR23;
    uint32_t u32PORT_PCR24;
    uint32_t u32PORT_PCR25;
    uint32_t u32PORT_PCR26;
    uint32_t u32PORT_PCR27;
    uint32_t u32PORT_PCR28;
    uint32_t u32PORT_PCR29;
    uint32_t u32PORT_PCR30;
    uint32_t u32PORT_PCR31;
    uint32_t u32PORT_GPCLR;
    uint32_t u32PORT_GPCHR;
    uint32_t u32PORT_DFER;
    uint32_t u32PORT_DFCR;
    uint32_t u32PORT_DFWR;

}PortRegisters_t;

#endif