#include "I2C.h"
#include "I2C_cfg.h"
#include "Port.h"
#include "DiagM.h"

#define PRINT(x)     vfnDiagnosticConsole_Print(MCAL,I2C,x)
I2C_cfg_stPtr i2c_Cfg_St_Ptr;
I2C_InterruptStatus_st i2cMode_st;
uint8_t i2cTxBufferCH0_a[I2C_TX_MAILBOX];
uint8_t i2cTxBufferIndexCH0_u8;
uint8_t i2cTxBufferLoadedJCH0_u8;
uint8_t i2cTxBufferCH1_a[I2C_TX_MAILBOX];
uint8_t i2cTxBufferIndexCH1_u8;
uint8_t i2cTxBufferCH2_a[I2C_TX_MAILBOX];
uint8_t i2cTxBufferIndexCH2_u8;
uint8_t i2cRxBufferCH0_au8[I2C_RX_MAILBOX];
uint8_t i2cRxBufferIndexCH0_u8;
I2C_StatusType_e i2cDriverState;
I2C_JobResultType_e i2cJobState;
I2C_SeqResultType_e i2cSeqState;
I2C_Err_t i2cError;
I2C_ChannelState_t i2cChannelsState_a[I2C_HW_UNITS];
I2C_SequenceType_Ptr i2c_DriverSeq_aPtr[I2C_MAX_SEQ_NUMBER];
I2C_JobType_t i2cJobIndex;

I2C_Job_st i2cJobs_a[I2C_MAX_JOBS];

I2C_Job_st* i2cJobs_a_HASH[I2C_MAX_JOBS];
I2C_Job_st* dummyJob;
I2C_Job_st* job;

I2C_JobType_t hashCode(I2C_JobType_t i2cJobID_lt )
{
    return i2cJobID_lt % I2C_MAX_JOBS;
}

I2C_Job_st* search(I2C_JobType_t i2cJobID_lt)
{
    I2C_JobType_t jobHashIndex = hashCode(i2cJobID_lt);
    boolean searchFlah_b = FALSE;
    I2C_Job_st* tempStrct_Ptr = NULLPTR;
    while(i2cJobs_a_HASH[jobHashIndex] != NULL || searchFlah_b == FALSE)
    {
        if(i2cJobs_a_HASH[jobHashIndex]->i2cJobID_t == i2cJobID_lt)
        {
            searchFlah_b = TRUE;
            tempStrct_Ptr = i2cJobs_a_HASH[jobHashIndex];
        }
        else
        {
            ++jobHashIndex;
            jobHashIndex %= I2C_MAX_JOBS;
        }
    }

    return tempStrct_Ptr;
}

void insert(I2C_Job_st* tempInsertStrct_Ptr)
{
    I2C_JobType_t jobHashIndex = hashCode(tempInsertStrct_Ptr->i2cJobID_t);
    while (i2cJobs_a_HASH[jobHashIndex] != NULL && i2cJobs_a_HASH[jobHashIndex]->i2cJobID_t != I2C_JOBHASH_DELETED )
    {
        ++jobHashIndex;
        jobHashIndex %= I2C_MAX_JOBS;
    }
    i2cJobs_a_HASH[jobHashIndex] = tempInsertStrct_Ptr;
}

I2C_Job_st* delete(I2C_Job_st* tempDeleteStrct_Ptr)
{
    I2C_Job_strct_Ptr deletedStrct_Ptr = NULLPTR;
    I2C_JobType_t job_lt = tempDeleteStrct_Ptr->i2cJobID_t;
    I2C_JobType_t jobHashIndex = hashCode(job_lt);

    while(i2cJobs_a_HASH[jobHashIndex] != NULL)
    {
        if(i2cJobs_a_HASH[jobHashIndex]->i2cJobID_t == job_lt)
        {
            deletedStrct_Ptr = i2cJobs_a_HASH[jobHashIndex];
            i2cJobs_a_HASH[jobHashIndex] = dummyJob;
        }
        else
        {
            ++jobHashIndex;
            jobHashIndex %= I2C_MAX_JOBS;
        }
        
    }
    return deletedStrct_Ptr;
}





FUNC(void,AUTOMATIC)vfnI2C_Init()
{
    i2c_Cfg_St_Ptr = pfnI2C_SetCfg();
    if(i2c_Cfg_St_Ptr == NULLPTR)
    {
        i2cChannelsState_a[I2C0]= I2C_CFG_STRUCT_NULL_E;
        i2cChannelsState_a[I2C1]= I2C_CFG_STRUCT_NULL_E;
        i2cChannelsState_a[I2C2]= I2C_CFG_STRUCT_NULL_E;

        i2cDriverState = I2C_FAILURE;
    }
    else
    {
        dummyJob = &job;
        dummyJob->i2cJobID_t = I2C_JOBHASH_DELETED;
        
#if(I2C0_Channel == STD_ON)
     
        I2C0_A1 = i2c_Cfg_St_Ptr[I2C0].I2C_A1_cfg;
        I2C0_C1 = i2c_Cfg_St_Ptr[I2C0].I2C_C1_cfg;
        I2C0_C2 = i2c_Cfg_St_Ptr[I2C0].I2C_FLT_cfg;
        I2C0_FLT = i2c_Cfg_St_Ptr[I2C0].I2C_FLT_cfg;
        I2C0_RA = i2c_Cfg_St_Ptr[I2C0].I2C_RA_cfg;
        I2C0_SMB = i2c_Cfg_St_Ptr[I2C0].I2C_SMB_cfg;
        I2C0_SLTH = i2c_Cfg_St_Ptr[I2C0].I2C_SLTH_cfg;
        I2C0_SLTL = i2c_Cfg_St_Ptr[I2C0].I2C_SLTL_cfg;

        // if(SIM_SCGC4 & I2C_0_SIM_SCGC4)
        // {
        //     if(((PORTE_PCR24) & I2C_0_SCL_PTE24)&&((PORTE_PCR25) & I2C_0_SDA_PTE25))
        //     {
        //         i2cChannelsState_a[I2C0] = E_OK;
        //     }
        //     else if((PORTB_PCR0 & I2C_0_SCL_PTB0) && (PORTB_PCR2 & I2C_0_SDA_PTB1))
        //     {
        //         i2cChannelsState_a[I2C0] = E_OK;
        //     }
        //     else if((PORTB_PCR2 & I2C_0_SCL_PTB2) && (PORTB_PCR2 & I2C_0_SDA_PTB3))
        //     {
        //         i2cChannelsState_a[I2C0] = E_OK;
        //     }
        //     else if((PORTD_PCR2 & I2C_0_SCL_PTD2) && (PORTD_PCR2 & I2C_0_SDA_PTD3))
        //     {
        //         i2cChannelsState_a[I2C0] = E_OK;
        //     }
        //     else if((PORTD_PCR2 & I2C_0_SCL_PTD8) && (PORTD_PCR2 & I2C_0_SDA_PTD9))
        //     {
        //         i2cChannelsState_a[I2C0] = E_OK;
        //     }
        //     else
        //     {
        //         i2cChannelsState_a[I2C0] = I2C_0_PORT_PINS_CFG_MISMATCH_E;
        //     }/*End I2C0 multiplexing verification*/
        // }/*End IF of SIM_SGCC4 Clock verification*/
        // else
        // {
        //     i2cChannelsState_a[I2C0] = I2C_0_SIM_CFG_MISMATCH_E;
        // }/*End else of SIM_SCGC4 Clock verification*/
        
        

        
#endif

#if(I2C1_Channel == STD_ON)
     
        I2C1_A1 = i2c_Cfg_St_Ptr[I2C1].I2C_A1_cfg;
        I2C1_C1 = i2c_Cfg_St_Ptr[I2C1].I2C_C1_cfg;
        I2C1_C2 = i2c_Cfg_St_Ptr[I2C1].I2C_FLT_cfg;
        I2C1_FLT = i2c_Cfg_St_Ptr[I2C1].I2C_FLT_cfg;
        I2C1_RA = i2c_Cfg_St_Ptr[I2C1].I2C_RA_cfg;
        I2C1_SMB = i2c_Cfg_St_Ptr[I2C1].I2C_SMB_cfg;
        I2C1_SLTH = i2c_Cfg_St_Ptr[I2C1].I2C_SLTH_cfg;
        I2C1_SLTL = i2c_Cfg_St_Ptr[I2C1].I2C_SLTL_cfg;

        // if(SIM_SCGC4 & I2C_1_SIM_SCGC4)
        // {
        //     if((PORTE_PCR1 & I2C_1_SCL_PTE1)&&(PORTE_PCR0 & I2C_1_SDA_PTE0))
        //     {
        //         i2cChannelsState_a[I2C1] = E_OK;
        //     }
        //     else if((PORTC_PCR10 & I2C_1_SCL_PTC10) && (PORTC_PCR11 & I2C_1_SDA_PTC11))
        //     {
        //         i2cChannelsState_a[I2C1] = E_OK;
        //     }
        //     else
        //     {
        //         i2cChannelsState_a[I2C1] = I2C_1_PORT_PINS_CFG_MISMATCH_E;
        //     }/*End I2C1 multiplexing verification*/
        // }/*End IF of SIM_SGCC4 Clock verification*/
        // else
        // {
        //     i2cChannelsState_a[I2C1] = I2C_1_SIM_CFG_MISMATCH_E;
        // }/*End else of SIM_SCGC4 Clock verification*/

#endif

#if(I2C2_Channel == STD_ON)
     
        I2C2_A1 = i2c_Cfg_St_Ptr[I2C2].I2C_A1_cfg;
        I2C2_C1 = i2c_Cfg_St_Ptr[I2C2].I2C_C1_cfg;
        I2C2_C2 = i2c_Cfg_St_Ptr[I2C2].I2C_FLT_cfg;
        I2C2_FLT = i2c_Cfg_St_Ptr[I2C2].I2C_FLT_cfg;
        I2C2_RA = i2c_Cfg_St_Ptr[I2C2].I2C_RA_cfg;
        I2C2_SMB = i2c_Cfg_St_Ptr[I2C2].I2C_SMB_cfg;
        I2C2_SLTH = i2c_Cfg_St_Ptr[I2C2].I2C_SLTH_cfg;
        I2C2_SLTL = i2c_Cfg_St_Ptr[I2C2].I2C_SLTL_cfg;
        if(SIM_SCGC1 & I2C_2_SIM_SCGC1)
        {
            if((PORTA_PCR12 & I2C_2_SCL_PTA12) && (PORTA_PCR11 & I2C_2_SDA_PTA11))
            {
                i2cChannelsState_a[I2C2] = E_OK;
            }
            else if((PORTC_PCR10 & I2C_2_SCL_PTA14) && (PORTC_PCR11 & I2C_2_SDA_PTA13))
            {
                i2cChannelsState_a[I2C2] = E_OK;
            }
            else
            {
                i2cChannelsState_a[I2C2] = I2C_2_PORT_PINS_CFG_MISMATCH_E;
            }/*End I2C2 multiplexing verification*/
        }/*End IF of SIM_SGCC1 Clock verification*/
        else
        {
            i2cChannelsState_a[I2C2] = I2C_2_SIM_CFG_MISMATCH_E;
        }/*End else of SIM_SCGC1 Clock verification*/
#endif
        i2cDriverState = I2C_IDLE;
#if (I2C_INTERRUPT_H == STD_ON)
        vfnI2C_InterruptsEnable();
#elif(I2C_DMA_H== STD_ON)
        vfnI2C_DMAEnable();
#endif
        vfnGroupSequences();
        vfnSetBaudrate();
    }

}

FUNC(void,AUTOMATIC)vfnI2C_TestSend()
{
        i2cMode_st.TXMODE = 1u;
        i2cTxBufferCH0_a[0] = 0xA0;
        i2cTxBufferIndexCH0_u8++;
        i2cTxBufferCH0_a[1] = 0xBB;
        i2cTxBufferIndexCH0_u8++;
        i2cMode_st.CHANNEL = 1;
        i2cTxBufferLoadedJCH0_u8 = i2cTxBufferIndexCH0_u8;
        i2cTxBufferIndexCH0_u8 = NULL;
        I2C0_C1 |= (I2C_MST_MODE_MASK | I2C_TX_RX_SLCT_MASK);
        I2C0_D |=  i2cTxBufferCH0_a[i2cTxBufferIndexCH0_u8];
        while( i2cMode_st.CHANNEL == 1){ ;}


}
FUNC(void,AUTOMATIC)vfnI2C_InterruptsEnable()
{
#if(I2C0_Channel == STD_ON)
I2C0_C1 |= I2C_IICIE_MASK;
#endif

#if(I2C1_Channel == STD_ON)
I2C1_C1 |= I2C_IICIE_MASK;
#endif

#if(I2C2_Channel == STD_ON)
I2C2_C1 |= I2C_IICIE_MASK;
#endif

}

FUNC(void,AUTOMATIC)vfnI2C_Send()
{
    if(TRANSMISSION_OFF == i2cMode_st.TXMODE)
    {
      
    }
    else
    {
        if(i2cTxBufferIndexCH0_u8 > NULL)
        {
            i2cMode_st.CHANNEL = 1;
            i2cTxBufferLoadedJCH0_u8 = i2cTxBufferIndexCH0_u8;
            i2cTxBufferIndexCH0_u8 = NULL;
            I2C0_C1 |= (I2C_MST_MODE_MASK | I2C_TX_RX_SLCT_MASK);
            I2C0_D |=  i2cTxBufferCH0_a[i2cTxBufferIndexCH0_u8];
            while( i2cMode_st.CHANNEL == 1){ ;}
        }
        if(i2cTxBufferIndexCH1_u8 > NULL)
        {
            i2cMode_st.CHANNEL = 1;
            I2C1_C1 |= (I2C_MST_MODE_MASK | I2C_TX_RX_SLCT_MASK);
            I2C1_D |=  i2cTxBufferCH1_a[i2cTxBufferIndexCH1_u8 % i2cTxBufferIndexCH1_u8];
            while( i2cMode_st.CHANNEL == 1){ ;}
        }

        if(i2cTxBufferIndexCH2_u8 > NULL)
        {
            i2cMode_st.CHANNEL = 1;
            I2C2_C1 |= (I2C_MST_MODE_MASK | I2C_TX_RX_SLCT_MASK);
            I2C2_D |=  i2cTxBufferCH2_a[i2cTxBufferIndexCH2_u8 % i2cTxBufferIndexCH2_u8];
            while( i2cMode_st.CHANNEL == 1){ ;}
        }
    }
    
}

FUNC(void,AUTOMATIC)vfnI2C_WriteIB(I2C_channel_e ei2cChannel, uint8_t txData_lu8)
{
    uint8_t lu8MailboxLengthCounter;
    i2cDriverState = I2C_BUSY;
    i2cMode_st.TXMODE = TRANSMISSION_ON;
    switch(ei2cChannel)
    {
      case(I2C0):
        {
            i2cTxBufferCH0_a[i2cTxBufferIndexCH0_u8++] = txData_lu8;
                
        }break;

      case(I2C1):
        {
            i2cTxBufferCH1_a[i2cTxBufferIndexCH1_u8++] = txData_lu8;
        }break;
            
        case(I2C2):
        {
            i2cTxBufferCH2_a[i2cTxBufferIndexCH2_u8++] = txData_lu8;
        }break;

        default:
        {
            i2cMode_st.TXMODE = TRANSMISSION_OFF;
        }
    }
    
}

FUNC(void,AUTOMATIC)vfnI2C_JobDecode(I2C_JobType_t jobID_lt,I2C_channel_e* i2cChannel_lePtr,I2C_Command_t* i2cCommand_ltPtr)
{
    I2C_Job_strct_Ptr currentJobToSend_lstPtr;
    currentJobToSend_lstPtr = search(jobID_lt);
    *i2cChannel_lePtr = currentJobToSend_lstPtr ->i2cChannel_e;
    *i2cCommand_ltPtr = currentJobToSend_lstPtr ->i2cCommand_t;
}

FUNC(void,AUTOMATIC)vfnI2C_AsyncTransmit(I2C_SequenceType_t sequence_t)
{
    I2C_SequenceType_t sequenceCounter_lt = sequence_t - I2C_SEQ_OFFSET;
    I2C_SequenceType_t (*seqToSend)[I2C_SEQ_JOBS_LENGTH];
    I2C_channel_e i2cChannel_le;
    I2C_Command_t i2cCommand_lt;

    seqToSend = i2c_DriverSeq_aPtr[sequenceCounter_lt];
    for(I2C_JobType_t jobSeqCounter_lt = NULL; jobSeqCounter_lt < I2C_SEQ_JOBS_LENGTH; jobSeqCounter_lt++)
    {
       vfnI2C_JobDecode(seqToSend[jobSeqCounter_lt],&i2cChannel_le,&i2cCommand_lt); 
       vfnI2C_WriteIB(i2cChannel_le,i2cCommand_lt);
    }
    vfnI2C_Send();

}

FUNC(void,AUTOMATIC)vfnGroupSequences()
{
    for(I2C_SequenceType_t seqIndex_lt= 0; seqIndex_lt < I2C_MAX_SEQ_NUMBER; seqIndex_lt++)
    {
        stdfnI2C_GetSequence(&i2c_DriverSeq_aPtr[seqIndex_lt],seqIndex_lt);
    }
}


FUNC(std_ReturnType,AUTOMATIC) stdfnSetJob(I2C_JobType_t jobID_lt, I2C_channel_e i2cJobChannel_le, I2C_Command_t i2cJobCommand_lt)
{
    I2C_JobType_t jobCounter_lt = I2C_FIRST_JOB;
    std_ReturnType setJobStatus_lstd;
    if((NULL == jobID_lt) || (I2C_MAX_JOBS < jobID_lt) )
    {
        setJobStatus_lstd = E_NOT_OK;
    }
    else
    {
        i2cJobs_a[jobID_lt -I2C_FIRST_JOB].i2cChannel_e = i2cJobChannel_le;
        i2cJobs_a[jobID_lt -I2C_FIRST_JOB].i2cJobID_t = jobID_lt;
        i2cJobs_a[jobID_lt -I2C_FIRST_JOB].i2cCommand_t = i2cJobCommand_lt;
        setJobStatus_lstd = E_OK;
        insert(&i2cJobs_a[jobID_lt -I2C_FIRST_JOB]);
      
    }
}

FUNC(std_ReturnType,AUTOMATIC)stdfnSortJobsSeq()
{
    std_ReturnType sortingStatus_lstd = E_NOT_OK;
    for(I2C_JobType_t jobCounter_lt = I2C_FIRST_JOB; jobCounter_lt < I2C_MAX_JOBS; jobCounter_lt++)
    {
        if(i2cJobs_a[jobCounter_lt -I2C_FIRST_JOB].i2cJobID_t != NULL)
        {
            sortingStatus_lstd = stdfnI2C_SetSequence(jobCounter_lt);
        }
    }
    return sortingStatus_lstd;
}

FUNC(void,AUTOMATIC)vfnSetBaudrate()
{
#if(I2C0_Channel == STD_ON)
    I2C0_F = I2C0_SCL_DIVIDER_ICR | I2C0_MULT;
#endif

#if(I2C1_Channel == STD_ON)
    I2C1_F = I2C1_SCL_DIVIDER_ICR | I2C1_MULT;
#endif
    
#if(I2C2_Channel == STD_ON)
    I2C2_F = I2C2_SCL_DIVIDER_ICR | I2C2_MULT;
#endif
}

FUNC(I2C_StatusType_e,AUTOMATIC)efnI2C_GetStatus()
{
 return i2cDriverState;
}

FUNC(I2C_ChannelState_t,AUTOMATIC)u8fnI2C_GetChannelState(I2C_channel_e i2cChannel_e)
{
    uint8_t i2cChannel_le;
    switch(i2cChannel_e)
    {
        case(I2C0):
        {
            i2cChannel_le = i2cChannelsState_a[I2C0];
        }break;

        case(I2C1):
        {
            i2cChannel_le = i2cChannelsState_a[I2C1];
        }break;

        case(I2C2):
        {
            i2cChannel_le = i2cChannelsState_a[I2C2];
        }break;

        default:
        {
            i2cChannel_le = I2C_PARAM_CHANNEL_E;
        }break;
    }
}

FUNC(I2C_Err_t,AUTOMATIC)tfnGetError()
{
    return i2cError;
}

FUNC(void,AUTOMATIC)vfnInitSMBChannel(I2C_Channel_t i2cSMBChannel_lt)
{
    
}