#ifndef PORT_CFG_H
#define PORT_CFG_H

#include "Port_Types.h"
#include "ProjectDefinitions.h"


#define PORT_I2C_PIN_PTE24_SCL 24u;
#define PORT_I2C_PIN_PTE25_SDA 25u;

#if(PORTA==STD_ON)
#define PORTA_PCR0     STD_OFF
#define PORTA_PCR1     STD_OFF
#define PORTA_PCR2     STD_OFF
#define PORTA_PCR3     STD_OFF
#define PORTA_PCR4     STD_OFF
#define PORTA_PCR5     STD_OFF
#define PORTA_PCR6     STD_OFF
#define PORTA_PCR7     STD_OFF
#define PORTA_PCR8     STD_OFF
#define PORTA_PCR9     STD_OFF
#define PORTA_PCR10    STD_OFF
#define PORTA_PCR11    STD_OFF
#define PORTA_PCR12    STD_OFF
#define PORTA_PCR13    STD_OFF
#define PORTA_PCR14    STD_OFF
#define PORTA_PCR15    STD_OFF
#define PORTA_PCR16    STD_OFF
#define PORTA_PCR17    STD_OFF
#define PORTA_PCR18    STD_OFF
#define PORTA_PCR19    STD_OFF
#define PORTA_PCR20    STD_OFF
#define PORTA_PCR21    STD_OFF
#define PORTA_PCR22    STD_OFF
#define PORTA_PCR23    STD_OFF
#define PORTA_PCR24    STD_OFF
#define PORTA_PCR25    STD_OFF
#define PORTA_PCR26    STD_OFF
#define PORTA_PCR27    STD_OFF
#define PORTA_PCR28    STD_OFF
#define PORTA_PCR29    STD_OFF
#define PORTA_PCR30    STD_OFF
#define PORTA_PCR31    STD_OFF
#define PORTA_PCRGPCLR STD_OFF
#define PORTA_PCRGPCHR STD_OFF
#define PORTA_ISFR     STD_OFF
#define PORTA_DFER     STD_OFF
#define PORTA_DFCR     STD_OFF
#define PORTA_DFWR     STD_OFF
#endif

#if(PORTB==STD_ON)
#define PORTB_PCR0     STD_OFF
#define PORTB_PCR1     STD_OFF
#define PORTB_PCR2     STD_OFF
#define PORTB_PCR3     STD_OFF
#define PORTB_PCR4     STD_OFF
#define PORTB_PCR5     STD_OFF
#define PORTB_PCR6     STD_OFF
#define PORTB_PCR7     STD_OFF
#define PORTB_PCR8     STD_OFF
#define PORTB_PCR9     STD_OFF
#define PORTB_PCR10    STD_OFF
#define PORTB_PCR11    STD_OFF
#define PORTB_PCR12    STD_OFF
#define PORTB_PCR13    STD_OFF
#define PORTB_PCR14    STD_OFF
#define PORTB_PCR15    STD_OFF
#define PORTB_PCR16    STD_OFF
#define PORTB_PCR17    STD_OFF
#define PORTB_PCR18    STD_OFF
#define PORTB_PCR19    STD_OFF
#define PORTB_PCR20    STD_OFF
#define PORTB_PCR21    STD_OFF
#define PORTB_PCR22    STD_OFF
#define PORTB_PCR23    STD_OFF
#define PORTB_PCR24    STD_OFF
#define PORTB_PCR25    STD_OFF
#define PORTB_PCR26    STD_OFF
#define PORTB_PCR27    STD_OFF
#define PORTB_PCR28    STD_OFF
#define PORTB_PCR29    STD_OFF
#define PORTB_PCR30    STD_OFF
#define PORTB_PCR31    STD_OFF
#define PORTB_PCRGPCLR STD_OFF
#define PORTB_PCRGPCHR STD_OFF
#define PORTB_ISFR     STD_OFF
#define PORTB_DFER     STD_OFF
#define PORTB_DFCR     STD_OFF
#define PORTB_DFWR     STD_OFF
#endif

#if(PORTC==STD_ON)
#define PORTC_PCR0 STD_OFF
#define PORTC_PCR1 STD_OFF
#define PORTC_PCR2 STD_OFF
#define PORTC_PCR3 STD_OFF
#define PORTC_PCR4 STD_OFF
#define PORTC_PCR5 STD_OFF
#define PORTC_PCR6 STD_OFF
#define PORTC_PCR7 STD_OFF
#define PORTC_PCR8 STD_OFF
#define PORTC_PCR9 STD_OFF
#define PORTC_PCR10 STD_OFF
#define PORTC_PCR11 STD_OFF
#define PORTC_PCR12 STD_OFF
#define PORTC_PCR13 STD_OFF
#define PORTC_PCR14 STD_OFF
#define PORTC_PCR15 STD_OFF
#define PORTC_PCR16 STD_ON
#if(PORTC_PCR16==STD_ON)
    #define PORTC_PCR16_IRQC_MASK (0u)
    #define PORTC_PCR16_MUX_MASK (0x300u)
    #define PORTC_PCR16_LK_MASK (0u)
    #define PORTC_PCR16_DSE_MASK (0u)
    #define PORTC_PCR16_PFE_MASK (0u)
    #define PORTC_PCR16_SRE_MASK (0u)
    #define PORTC_PCR16_PE_MASK (0u)
    #define PORTC_PCR16_PS_MASK (0u)
#define PORTC_PCR16_CFG_VALUES (PORTC_PCR16_IRQC_MASK|PORTC_PCR16_MUX_MASK\
                                |PORTC_PCR16_LK_MASK|PORTC_PCR16_DSE_MASK\
                                |PORTC_PCR16_PFE_MASK|PORTC_PCR16_SRE_MASK\
                                |PORTC_PCR16_PE_MASK|PORTC_PCR16_PS_MASK)
#endif
#define PORTC_PCR17 STD_ON
#if(PORTC_PCR17==STD_ON)
    #define PORTC_PCR17_IRQC_MASK (0u)
    #define PORTC_PCR17_MUX_MASK (0x300u)
    #define PORTC_PCR17_LK_MASK (0u)
    #define PORTC_PCR17_DSE_MASK (0u)
    #define PORTC_PCR17_PFE_MASK (0u)
    #define PORTC_PCR17_SRE_MASK (0u)
    #define PORTC_PCR17_PE_MASK (0u)
    #define PORTC_PCR17_PS_MASK (0u)
#define PORTC_PCR17_CFG_VALUES (PORTC_PCR17_IRQC_MASK|PORTC_PCR17_MUX_MASK\
                                |PORTC_PCR17_LK_MASK|PORTC_PCR17_DSE_MASK \
                                |PORTC_PCR17_PFE_MASK|PORTC_PCR17_SRE_MASK\
                                |PORTC_PCR17_PE_MASK|PORTC_PCR17_PS_MASK)
#endif
#define PORTC_PCR18    STD_OFF
#define PORTC_PCR19    STD_OFF
#define PORTC_PCR20    STD_OFF
#define PORTC_PCR21    STD_OFF
#define PORTC_PCR22    STD_OFF
#define PORTC_PCR23    STD_OFF
#define PORTC_PCR24    STD_OFF
#define PORTC_PCR25    STD_OFF
#define PORTC_PCR26    STD_OFF
#define PORTC_PCR27    STD_OFF
#define PORTC_PCR28    STD_OFF
#define PORTC_PCR29    STD_OFF
#define PORTC_PCR30    STD_OFF
#define PORTC_PCR31    STD_OFF
#define PORTC_PCRGPCLR STD_OFF
#define PORTC_PCRGPCHR STD_OFF
#define PORTC_ISFR     STD_OFF
#define PORTC_DFER     STD_OFF
#define PORTC_DFCR     STD_OFF
#define PORTC_DFWR     STD_OFF
#endif

#if(PORTD==STD_ON)
#define PORTD_PCR0     STD_OFF
#define PORTD_PCR1     STD_OFF
#define PORTD_PCR2     STD_OFF
#define PORTD_PCR3     STD_OFF
#define PORTD_PCR4     STD_OFF
#define PORTD_PCR5     STD_OFF
#define PORTD_PCR6     STD_OFF
#define PORTD_PCR7     STD_OFF
#define PORTD_PCR8     STD_OFF
#define PORTD_PCR9     STD_OFF
#define PORTD_PCR10    STD_OFF
#define PORTD_PCR11    STD_OFF
#define PORTD_PCR12    STD_OFF
#define PORTD_PCR13    STD_OFF
#define PORTD_PCR14    STD_OFF
#define PORTD_PCR15    STD_OFF
#define PORTD_PCR16    STD_OFF
#define PORTD_PCR17    STD_OFF
#define PORTD_PCR18    STD_OFF
#define PORTD_PCR19    STD_OFF
#define PORTD_PCR20    STD_OFF
#define PORTD_PCR21    STD_OFF
#define PORTD_PCR22    STD_OFF
#define PORTD_PCR23    STD_OFF
#define PORTD_PCR24    STD_OFF
#define PORTD_PCR25    STD_OFF
#define PORTD_PCR26    STD_OFF
#define PORTD_PCR27    STD_OFF
#define PORTD_PCR28    STD_OFF
#define PORTD_PCR29    STD_OFF
#define PORTD_PCR30    STD_OFF
#define PORTD_PCR31    STD_OFF
#define PORTD_PCRGPCLR STD_OFF
#define PORTD_PCRGPCHR STD_OFF
#define PORTD_ISFR     STD_OFF
#define PORTD_DFER     STD_OFF
#define PORTD_DFCR     STD_OFF
#define PORTD_DFWR     STD_OFF
#endif

#if(PORTE==STD_ON)
#define PORTE_PCR0     STD_OFF
#define PORTE_PCR1     STD_OFF
#define PORTE_PCR2     STD_OFF
#define PORTE_PCR3     STD_OFF
#define PORTE_PCR4     STD_OFF
#define PORTE_PCR5     STD_OFF
#define PORTE_PCR6     STD_OFF
#define PORTE_PCR7     STD_OFF
#define PORTE_PCR8     STD_OFF
#define PORTE_PCR9     STD_OFF
#define PORTE_PCR10    STD_OFF
#define PORTE_PCR11    STD_OFF
#define PORTE_PCR12    STD_OFF
#define PORTE_PCR13    STD_OFF
#define PORTE_PCR14    STD_OFF
#define PORTE_PCR15    STD_OFF
#define PORTE_PCR16    STD_OFF
#define PORTE_PCR17    STD_OFF
#define PORTE_PCR18    STD_OFF
#define PORTE_PCR19    STD_OFF
#define PORTE_PCR20    STD_OFF
#define PORTE_PCR21    STD_OFF
#define PORTE_PCR22    STD_OFF
#define PORTE_PCR23    STD_OFF
#define PORTE_PCR24    STD_ON
#if(PORTE_PCR24==STD_ON)
    #define PORTE_PCR24_IRQC_MASK (0u)
    #define PORTE_PCR24_MUX_MASK (0x500u)
    #define PORTE_PCR24_LK_MASK (0u)
    #define PORTE_PCR24_DSE_MASK (0u)
    #define PORTE_PCR24_PFE_MASK (0u)
    #define PORTE_PCR24_SRE_MASK (0u)
    #define PORTE_PCR24_PE_MASK (0u)
    #define PORTE_PCR24_PS_MASK (0u)
#define PORTE_PCR24_CFG_VALUES (PORTE_PCR24_IRQC_MASK|PORTE_PCR24_MUX_MASK\
                                |PORTE_PCR24_LK_MASK|PORTE_PCR24_DSE_MASK\
                                |PORTE_PCR24_PFE_MASK|PORTE_PCR24_SRE_MASK\
                                |PORTE_PCR24_PE_MASK|PORTE_PCR24_PS_MASK)
#endif
#define PORTE_PCR25    STD_ON
#if(PORTE_PCR25==STD_ON)
    #define PORTE_PCR25_IRQC_MASK (0u)
    #define PORTE_PCR25_MUX_MASK (0x500u)
    #define PORTE_PCR25_LK_MASK (0u)
    #define PORTE_PCR25_DSE_MASK (0u)
    #define PORTE_PCR25_PFE_MASK (0u)
    #define PORTE_PCR25_SRE_MASK (0u)
    #define PORTE_PCR25_PE_MASK (0u)
    #define PORTE_PCR25_PS_MASK (0u)
#define PORTE_PCR25_CFG_VALUES (PORTE_PCR25_IRQC_MASK|PORTE_PCR25_MUX_MASK\
                                |PORTE_PCR25_LK_MASK|PORTE_PCR25_DSE_MASK\
                                |PORTE_PCR25_PFE_MASK|PORTE_PCR25_SRE_MASK\
                                |PORTE_PCR25_PE_MASK|PORTE_PCR25_PS_MASK)
#endif
#define PORTE_PCR26    STD_OFF
#define PORTE_PCR27    STD_OFF
#define PORTE_PCR28    STD_OFF
#define PORTE_PCR29    STD_OFF
#define PORTE_PCR30    STD_OFF
#define PORTE_PCR31    STD_OFF
#define PORTE_PCRGPCLR STD_OFF
#define PORTE_PCRGPCHR STD_OFF
#define PORTE_ISFR     STD_OFF
#define PORTE_DFER     STD_OFF
#define PORTE_DFCR     STD_OFF
#define PORTE_DFWR     STD_OFF
#endif

#define PORTA_INDEX 0u
#define PORTB_INDEX 1u
#define PORTC_INDEX 2u
#define PORTD_INDEX 3u
#define PORTE_INDEX 4u

FUNC(std_ReturnType,AUTOMATIC) stdfnPort_SetCfg(void);
FUNC(PortRegisters_t*,AUTOMATIC) cxfnPort_GetCfg(void);

#endif