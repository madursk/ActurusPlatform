
#include "Os.h"
#include "Os_isr.h"
#include "Os_cfg.h"
#include "Os_tasks.h"
#include "EcuM.h"

TaskType_t currentTaskID;
static OS_COUNTER_TYPE_t OsCounter;
static uint8_t osRequestPrivilege;
extern Task_cfg_Ptr taskList[NUMBER_OF_TASKS];
extern Alarms_st alarms_st;
uint8_t taskQCounter;
uint8_t taskQCurrentPriority;
uint8_t taskQHighestPriority;
uint8_t taskQTempHighestPriority= 0xFFu;
TaskType_t taskID2Q;
volatile uint32_t* pxCurrentTCB;
volatile uint32_t* pxCurrentTCB_RB;
extern void _SetPendSV();


void startOs()
{
    Os_SetTasks();
    Os_StartupHook();
}

void Os_StartupHook()
{
    /*initiate systick with OS ticks*/
    Systick_Init();
    /*Initiate EcuM that will initialize all peripherals*/
    vfnEcuM_Init();
    /*call scheduler*/
    Scheduler();
}

inline void Os_SetPrivilege()
{
    /*Set privileged execution*/
    osRequestPrivilege = PRIVILEGED;
    __asm__("SVC #0": : );
}

inline void Os_ClearPrivilege()
{
    /*Release privileged execution*/
    osRequestPrivilege = UNPRIVILEGED;
    __asm__("SVC #0": : );

}

inline void SwitchTask(void)
{
            /*If the running task finished its execution it will fall here
            * the context has to be done reseting the Task stack pointer,
            otherwise it will consume itself in to a overflow*/
            __asm__ volatile("MOV r0, #0x1"::);                                     /*Move 1 to R0*/
            __asm__ volatile("msr primask, r0"::);                                  /*Disable all configurable interrupts/exceptions*/
            __asm__ volatile("ISB"::);                                              /*Synchronization instruction*/
            __asm__ volatile("LDR r3, =pxCurrentTCB"::);                            /*Load from memory the pointer of the current task TCB into R3*/
            __asm__ volatile("LDR r2, [r3]"::);                                     /*Load the value of the pointer to R2*/
            __asm__ volatile("MOV r0, r2"::);                                       /*Move R2 register to R0*/
            __asm__ volatile("STMDB r0!, {r4-r11, r14}"::);                         /*Store in multiple decrements from R0 registers 4 to 11 and link register
                                                                                    *This is the stack frame of the task and R0 will have the result of R0 - (number of reg * 4)*/
            __asm__ volatile("STR r0,[r2]"::);                                      /*R2 has the value of the TCB pointer which stores the value of the stack frame location*/
            __asm__ volatile("MRS R12, CONTROL"::);                                 /*Since we are using PSP and we are not in an exception calling ContextSwitch function will modify 
                                                                                    *R0 and R3 registers, CONTROL register will manually be modified to simulate an exception */
            __asm__ volatile("ORR R12, R12, #0x0"::);                               /*OR to modify CONTROL register read and modify intended bit*/
            __asm__ volatile("MSR CONTROL, R12"::);                                 /*Write the modified CONTROL register value in R12 which will switch to Main Stack pointer*/
            __asm__ volatile("ISB"::);                                              /*Synchronization*/
            __asm__ volatile("mrs r12, msp"::);                                     /*Read Main Stack pointer and load it to R12*/
            __asm__ volatile("mov sp, r12"::);                                      /*Move MSP value to Stack pointer register*/
            __asm__ volatile("STMDB sp!, {r0, r3}"::);                              /*Store R0 and R3 in multiple decrements using MSP as the base*/
            __asm__ volatile("bl ContextSwitch"::);                                 /*Since we have preserved the important values jump to context switch function*/
            __asm__ volatile("LDMIA sp!, {r0, r3}"::);                              /*Load in multiple decrements what was stored in the MSP to R0 and R3*/
            __asm__ volatile("MRS R12, CONTROL"::);                                 /*There is no further use to MSP so we can return to PSP*/
            __asm__ volatile("ORR R12, R12, #0x2"::);                               /*OR with modified value of CONTROL in R12 to write with PSP active*/
            __asm__ volatile("MSR CONTROL, R12"::);                                 /*Write CONTROL with PSP active*/
            __asm__ volatile("mrs r12, psp"::);                                     /*Retrieve PSP and store it in R12*/
            __asm__ volatile("mov sp, r12"::);                                      /*Move R12 to the Stack Pointer*/
            __asm__ volatile("ISB"::);                                              /*Synchronization*/
            __asm__ volatile("LDR r1, [r3]"::);                                     /* Load value of R3 (pxCurrentTCB) in R1 */
            __asm__ volatile("LDR r0, [r1]"::);                                     /*Load from R3 where the stack frame begins*/
            __asm__ volatile("LDMIA r0!, {r4-r11, r14}"::);                         /*Load in multiple increments where stack frame with Link Register at the end*/
            __asm__ volatile("msr psp, r0"::);                                      /*Write PSP with the top of the task stack*/
            __asm__ volatile("ISB"::);                                              /*Synchronization*/
            __asm__ volatile("MOV r0, #0"::);                                       /*Move 0 to R0*/
            __asm__ volatile("msr primask, r0"::);                                  /*Enable all configurable interrupts/exceptions*/
            __asm__ volatile("ISB"::);                                              /*Synchronization instruction*/
            __asm__ volatile("bx r14"::);                                           /*Jump to the Link Register of the saved stack frame*/
}

inline void Scheduler(void)
{
#if(ECC1 == STD_ON)
    /*check the current task*/
    if(currentTaskID == NULL)
    {
        /* first start of scheduler */
        /*order task from low to high priority and insert it to queue(pool)*/
        /*select from task list the one with higher priority and run it in assembly
        no function call since it will consume the whole stack, the stack ptr
        must be reseted*/

            while( taskQCounter < NUMBER_OF_TASKS )
            {
                taskList[taskQCounter]->TaskState_t = TASK_STATE_READY;
                if(taskList[taskQCounter]->TaskPriority_t < taskQTempHighestPriority)
                {
                    taskID2Q = taskList[taskQCounter]->TaskID_t;
                    taskQTempHighestPriority = taskList[taskQCounter]->TaskPriority_t;
                    taskQHighestPriority = taskQCounter;
                }
                taskQCounter++;
            }

            
            pxCurrentTCB = taskList[taskQHighestPriority]->TCB->StackTop;
            currentTaskID = taskID2Q;
            taskList[taskID2Q-1]->TaskState_t = TASK_STATE_RUNNING;
            taskQTempHighestPriority = 255u;
            taskQHighestPriority = 0u;
            taskQCounter = 0u;
            
            /*Load top of the stack contained in the TCB
            * No need to store stack frame since its first task
            */
            __asm__ volatile("LDR r3, =pxCurrentTCB"::); /*Load  register R3 with a memory address in label pxCurrentTCB*/
            __asm__ volatile("LDR r0, [r3]"::); /*Load what is contained in the memory address in R3 in R0*/
            __asm__ volatile("LDMIA r0!, {r4-r11, r14}"::); /* Load multiple register incrementing each time, start from R0 mem address
                                                            * load to memory R4 to R11 and finalize with R14 (LR)*/
            __asm__ volatile("msr psp, r0"::);  /* LDMIA finalize with R0 pointer at the end of the increment
                                                * move R0 to the process stack pointer so it can be at the top of the stack 
                                                */
            __asm__ volatile("ISB"::);          /*Synchronization instruction*/
            __asm__ volatile("bx r14"::);       /*Since this is the first task to run 
                                                all the tasks have in their inital stack frame the R14(LR) pointing
                                                to the start of the task*/
    }
    else if(taskList[currentTaskID-1]->TaskState_t == TASK_STATE_SUSPENDED)
    {
            
    }
    else
    {
        /*N/A*/
    }

#elif(ECC2 == STD_ON)

#elif(BCC1 == STD_ON)

#elif(BCC2 == STD_ON)

#endif

}

void Systick_Init()
{
    /*init systick*/
    /*cause an interrupt every 5ms */
    SYST_RVR = (uint32_t) SYSTEM_TICKS;
    SYST_CVR = (uint32_t) NULL;
    SYST_CSR = (uint32_t) (SYSTICK_CLKSOURCE_MASK | SYSTICK_TICKINT_MASK | SYSTICK_ENABLE_MASK);
    SCB_SHPR3 |= SCB_SHPR3_PRI_15_MASK;
     /* SCB_SHPR3: PRI_15=0x80 */
    SCB_SHPR3 = (uint32_t)((SCB_SHPR3 & (uint32_t)~(uint32_t)(SCB_SHPR3_PRI_15(0x7F))) | (uint32_t)(SCB_SHPR3_PRI_15(0x80)));
}

void SysStick_Handler(void)
{
    ++OsCounter;

    if(NULL == OsCounter % EV_10MS)
    {
        /*Detonate 10 ms alarms*/
        alarms_st.e10_ms_AlarmState = ALARM_TRIGGERED;
    }
    
    if(NULL == OsCounter % EV_20MS)
    {
        /*Detonate 20 ms alarms*/
         alarms_st.e20_ms_AlarmState = ALARM_TRIGGERED;
    }

    
    if(NULL == OsCounter % EV_50MS)
    {
        /*Detonate 50 ms alarms*/
         alarms_st.e50_ms_AlarmState = ALARM_TRIGGERED;
    }

    
    if(NULL == OsCounter % EV_100MS)
    {
        /*Detonate 100 ms alarms*/
         alarms_st.e100_ms_AlarmState = ALARM_TRIGGERED;
    }


}

void SVC_Handler(void)
{
    /*TO DO: SVC enable or disable privilege*/
}

void PendSRV_Handler(void)
{
    /*Software based interrupt*/
}


void ContextSwitch(void)
{
    /*Change the init tasks priority to disable them from running again*/
    if(taskList[currentTaskID-1]->taskType == INIT)
    {
        taskList[currentTaskID-1]->TaskPriority_t = 255u;
    }
    /*Iterate over the the number of tasks*/
     while( taskQCounter < NUMBER_OF_TASKS )
        {
            /*Check wether a task is in the suspended state and is not an init to return it to READY*/
            if((TASK_STATE_SUSPENDED == taskList[currentTaskID-1]->TaskState_t) && (INIT != taskList[currentTaskID-1]->taskType))
            {
                taskList[currentTaskID-1]->TaskState_t = TASK_STATE_READY;
            }

            /*Simple comparison to know the lowest number which means its the highest priority*/
            if(taskList[taskQCounter]->TaskPriority_t < taskQTempHighestPriority)
            {
                /*Since current task may be in suspended a check needs to be done to prevent same task running twice*/
                if((taskQCounter != (currentTaskID-1u)) && (INIT != taskList[taskQCounter]->taskType))
                {
                    /*Load the task id of the one with highest priority*/
                    taskID2Q = taskList[taskQCounter]->TaskID_t;
                    /*A variable that will store the priority to be used in the comparison for next cycle*/
                    taskQTempHighestPriority = taskList[taskQCounter]->TaskPriority_t;
                    /*Store the array index of the task in the list*/
                    taskQHighestPriority = taskQCounter;
                }
            }
            taskQCounter++;
        }
        /*Pointer that holds the current running task TCB address*/
        pxCurrentTCB = taskList[taskQHighestPriority]->TCB;
        /*Update the current running Task ID*/
        currentTaskID = taskID2Q;
        /*Set the task state to RUNNING*/
        taskList[taskID2Q-1]->TaskState_t = TASK_STATE_RUNNING;
        /*Reset variables used*/
        taskQTempHighestPriority = 255u;
        taskQHighestPriority = 0u;
        taskQCounter = 0u;
}

void Os_ErrorHook(void)
{
    __asm("bkpt");
    /*Detect source of error*/
    /*choose recovery mechanism*/
}

void Os_ProtectionHook()
{
    /*Protection Hook will be launched when an error is detected*/
}

void TerminateTask(TaskType_t taskID_t)
{
    taskList[taskID_t-1]->TaskState_t = TASK_STATE_SUSPENDED;
}