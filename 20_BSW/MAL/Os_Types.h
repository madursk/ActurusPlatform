#ifndef OS_TYPES_H
#define OS_TYPES_H

#include "Datatypes.h"

#define TASK_STATE_RUNNING 10u
#define TASK_STATE_WAITING 20u
#define TASK_STATE_READY 30u
#define TASK_STATE_SUSPENDED 40u
#define INVALID_TASK 50u

typedef enum
{
    E_OS_ACCESS = 1u,
    E_OS_CALLEVEL,
    E_OS_ID,
    E_OS_LIMIT,
    E_OS_NOFUNC,
    E_OS_RESOURCE,
    E_OS_STATE,
    E_OS_VALUE

}StatusType_en;

typedef enum
{
    NON,
    FULL
}SchedulableT_en;

typedef uint8_t TaskType_t;
typedef TaskType_t* TaskRefType_Ptr;
typedef uint8_t TaskStateType_t;

typedef struct
{
    volatile uint32_t* StackTop;
    volatile uint32_t* LINKR_R14;

}TaskControlBlock;

typedef struct
{
    TaskType_t TaskID_t;
    TaskType_t TaskPriority_t;
    TaskStateType_t TaskState_t;
    void (*TaskPtr)(void);
    TaskType_t Activation_t;
    SchedulableT_en Schedule;
    boolean Autostart_b;
    TaskType_t taskType;
    volatile TaskControlBlock* TCB;
}Task_cfg_st;

typedef enum
{
    ALARM_TRIGGERED,
    ALARM_NOT_TRIGGERED,
    ALARM_ERASED

}AlarmsStates_e;

typedef struct
{
    uint32_t PC; /*Program Counter*/
    uint32_t LR; /*Link Register*/
}TaskContext_st;


typedef Task_cfg_st* Task_cfg_Ptr;


#endif