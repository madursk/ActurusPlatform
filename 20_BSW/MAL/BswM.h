#ifndef BSWM_H
#define BSWM_H

#include "Datatypes.h"


typedef struct 
{
  uint16_t cryHandlerState_u16;
  uint16_t mcuHandlerState_u16;
  uint16_t storageHandlerState_u16;
  uint16_t ioHandlerState_u16;
  uint16_t devHandlerState_u16;
  uint16_t diagHandlerState_u16;
  uint16_t comHandlerState_u16;

}HAL_STATUS_s;



typedef struct 
{
  uint8_t SecMState_u8;
  uint8_t LcMState_u8;
  uint8_t StoreMState_u8;
  uint8_t SignalMState_u8;
  uint8_t MulMState_u8;
  uint8_t diagMState_u8;
  uint8_t comMState_u8;

}MAL_STATUS_s;

typedef HAL_STATUS_s* HAL_STATUS_Ptr;
typedef MAL_STATUS_s* MAL_STATUS_Ptr;


FUNC(void,AUTOMATIC)vfnHal_Init();
FUNC(void,AUTOMATIC)vfnMal_Init();
FUNC(void,AUTOMATIC)vfnEcuM_Init();
FUNC(void,AUTOMATIC)BswM_Scheduler();
FUNC(void,AUTOMATIC)BswM_Init();


#endif