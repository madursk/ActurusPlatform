#include "Os_tasks.h"
#include "Os_cfg.h"
#include "BswM.h"
#include "DiagM.h"
#include "AppController.h"

extern volatile uint32_t* _taskInitStart;
extern volatile uint32_t* _taskInitEnd;
extern volatile uint32_t* _taskBswStart;
extern volatile uint32_t* _taskBswEnd;
extern volatile uint32_t* _taskApplStart;
extern volatile uint32_t* _taskApplEnd;
extern volatile uint32_t* _taskIdleStart;
extern volatile uint32_t* _taskIdleEnd;

volatile Task_cfg_st taskInit_st;
volatile Task_cfg_st taskBsw_st;
volatile Task_cfg_st taskAppl_st;
volatile Task_cfg_st taskIdle_st;
volatile TaskControlBlock TCB_initTsk OS_TASK_INIT_TCB;
volatile TaskControlBlock TCB_bswTsk OS_TASK_BSW_TCB;
volatile TaskControlBlock TCB_ApplTsk OS_TASK_APP_TCB;
volatile TaskControlBlock TCB_IdleTsk OS_TASK_IDLE_TCB;


Task_cfg_Ptr taskList[NUMBER_OF_TASKS];
Alarms_st alarms_st;

void Os_SetTasks()
{

    taskBsw_st.TaskID_t = TASK_BSW_;
    taskAppl_st.TaskID_t = TASK_APPL_;
    taskIdle_st.TaskID_t = TASK_IDLE_;
    taskInit_st.TaskID_t = TASK_INIT_;

    taskInit_st.TaskPriority_t  =  TASK_INIT_PRIORITY;
    taskBsw_st.TaskPriority_t  =  TASK_BSW_PRIORITY;
    taskAppl_st.TaskPriority_t =  TASK_APPL_PRIORITY;
    taskIdle_st.TaskPriority_t  =  TASK_IDLE_PRIORITY;

    taskInit_st.TaskPtr =TASK_INIT;
    taskBsw_st.TaskPtr =TASK_BSW;
    taskAppl_st.TaskPtr =TASK_APPL;
    taskIdle_st.TaskPtr = TASK_IDLE;

    taskInit_st.Activation_t = ONE_ACTIVATION;
    taskBsw_st.Activation_t = ONE_ACTIVATION;
    taskAppl_st.Activation_t = ONE_ACTIVATION;
    
    taskInit_st.taskType = INIT;
    taskBsw_st.taskType = NONINIT;
    taskAppl_st.taskType = NONINIT;

    taskInit_st.Autostart_b = TRUE;
    taskBsw_st.Autostart_b = FALSE;
    taskAppl_st.Autostart_b = FALSE;

    taskInit_st.TCB = &TCB_initTsk;
    taskBsw_st.TCB = &TCB_bswTsk;
    taskAppl_st.TCB = &TCB_ApplTsk;
    taskIdle_st.TCB = &TCB_IdleTsk;

    taskInit_st.TCB->LINKR_R14 = TASK_INIT;
    taskBsw_st.TCB->LINKR_R14 = TASK_BSW;
    taskAppl_st.TCB->LINKR_R14 = TASK_APPL;
    taskIdle_st.TCB->LINKR_R14 = TASK_IDLE;

    taskList[0] = &taskInit_st;
    taskList[1] = &taskBsw_st;
    taskList[2] = &taskAppl_st;
    taskList[3] = &taskIdle_st;

    alarms_st.e10_ms_AlarmState = ALARM_NOT_TRIGGERED;
    alarms_st.e20_ms_AlarmState = ALARM_NOT_TRIGGERED;
    alarms_st.e50_ms_AlarmState = ALARM_NOT_TRIGGERED;
    alarms_st.e100_ms_AlarmState = ALARM_NOT_TRIGGERED;
}
TASK TASK_BSW()
{
    for(;;)
    {
        if(ALARM_TRIGGERED == alarms_st.e10_ms_AlarmState)
        {
            // DeviceManager_main();
            // LifecycleManager_main();
            alarms_st.e10_ms_AlarmState = ALARM_NOT_TRIGGERED;
        }

        if(ALARM_TRIGGERED == alarms_st.e20_ms_AlarmState)
        {
            // StorageManager_main();
            // SecurityManager_main();
            DiagnosticManager_Main();
            // ComManager();
            alarms_st.e20_ms_AlarmState = ALARM_NOT_TRIGGERED;
        }

        if(ALARM_TRIGGERED == alarms_st.e50_ms_AlarmState)
        {
            //SignalManager_main();
            alarms_st.e50_ms_AlarmState = ALARM_NOT_TRIGGERED;
        }

        TerminateTask(taskBsw_st.TaskID_t);
        SwitchTask();
    }
    
}


TASK TASK_APPL()
{
    for(;;)
    {
        if(ALARM_TRIGGERED == alarms_st.e100_ms_AlarmState)
        {
            vfnAppController_Main();
            alarms_st.e100_ms_AlarmState = ALARM_NOT_TRIGGERED;
        }
        TerminateTask(taskAppl_st.TaskID_t);
        SwitchTask();
    }
}


TASK TASK_IDLE()
{
    for(;;)
    {
        /*Calculate CPU Load*/
        TerminateTask(taskIdle_st.TaskID_t);
        
    }
    
}

TASK TASK_INIT()
{
    
    BswM_Init();
    TerminateTask(taskInit_st.TaskID_t);
    SwitchTask();
}