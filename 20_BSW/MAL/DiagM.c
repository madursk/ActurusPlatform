#include "DiagM.h"
#include "DiagM_Cfg.h"
#include "DiagHandler.h"


/**
 * Name DiagnosticManager_Init
 * @brief Function that initializes
 * the dianostic manager prints banner
 * 
 * @param None
 * 
 * @return None
 */
FUNC(void, AUTOMATIC) DiagnosticManager_Init(void)
{
    GetCurrenDiagMStateMachineState = STARTUP_PHASE;
    vfnSetRoutines();
    vfnDiagnosticConsole_RunRoutine(BANNER);
    GetCurrenDiagMStateMachineState = PRE_RUN_PHASE;


}

/**
 * Name DiagnosticManager_Main
 * @brief Function that iterates over the states machines
 * 
 * @param None
 * 
 * @return None
 */
FUNC(void, AUTOMATIC) DiagnosticManager_Main(void)
{
    /*Check if Diagnostic Manager State Machine is Up*/
   DiagM_States_t cxCurrentState = GetCurrenDiagMStateMachineState;
    switch(cxCurrentState)
    {
        case(PRE_RUN_PHASE):
        {
            /*Signals the lifecyle manager that is in no clear*/
            DiagnosticConsole_Tx();
        }break;
        case(RUN_PHASE):
        {
            /*Check for diagnostic requests evalueate DTCs conditions*/
            /*Check for LifecycleManager state  to know when to switch*/
            DiagnosticConsole_Tx();
        }break;
        case(PRE_SHUTDOWN_PHASE):
        {
            /*Starts writing to NVM and stops printing*/
            DiagnosticConsole_Tx();
        }break;
        
    }

}


FUNC(void, AUTOMATIC) DiagnosticEventControl_SM(void)
{

}

/**
 * Name DiagnosticConsole_Tx
 * @brief The console TX processes
 * 
 * @param None
 * 
 * @return None
 */
FUNC(void, AUTOMATIC) DiagnosticConsole_Tx(void)
{
    vfnDiagHandler_TxQueueProcessing();
}

/**
 * Name vfnDiagnosticConsole_Print
 * @brief Print function that prints any string for the
 * internal diagnostic channel
 * 
 * @param DiagM_Module_t cxModule
 * @param DiagM_Layer_t cxLayer
 * @param char_t cxMessage[]
 * 
 * @return None
 */
FUNC(void,AUTOMATIC) vfnDiagnosticConsole_Print(DiagM_Module_t cxModule, DiagM_Layer_t cxLayer, char_t cxMessage[])
{
    vfnBuildFrameForIntDiag(cxModule,cxLayer,cxMessage);

}

/**
 * Name vfnDiagnosticConsole_RunRoutine
 * @brief Runs the selected routine of the
 * Diagnostic Console Routine
 * 
 * @param RoutineId_t cxRoutine
 * @return None
 */
FUNC(void,AUTOMATIC) vfnDiagnosticConsole_RunRoutine(RoutineId_t cxRoutine)
{
    switch (cxRoutine)
    {
        case(BANNER):
        {
            vfnDiagnosticConsole_Banner();
        }
        break;
    
        default:
        break;
    }
}

