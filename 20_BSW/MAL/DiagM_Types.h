#ifndef DIAG_M_TYPES_H
#define DIAG_M_TYPES_H
#include "Datatypes.h"
#include "ProjectDefinitions.h"
#include "DiagHandler_Types.h"

typedef enum
{
    STARTUP_PHASE,
    PRE_RUN_PHASE,
    RUN_PHASE,
    PRE_SHUTDOWN_PHASE,
    SHUTDOWN_PHASE

}DiagM_States_t;

typedef enum
{
    BANNER,
    MCAL_CHECK,
    HAL_CHECK,
    MAL_CHECK,
    APP_CHECK
}RoutineId_t;

typedef struct
{
    RoutineId_t routineId;
    uint8_t routineSteps;
}Routines_t;

typedef Module_t DiagM_Module_t;
typedef Layer_t DiagM_Layer_t;



VAR(DiagM_States_t, DEFAULT) cxDiagMCurrentState;
#endif