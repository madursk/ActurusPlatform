#ifndef DIAG_M_H
#define DIAG_M_H
#include "DiagM_Types.h"
#include "DiagM_Cfg.h"

#define GetCurrenDiagMStateMachineState     cxDiagMCurrentState
#define GetChannelForConsole                GetChannelForConsolePtr

FUNC(void, AUTOMATIC) DiagnosticManager_Init(void);
FUNC(void, AUTOMATIC) DiagnosticManager_Main(void);
FUNC(void, AUTOMATIC) DiagnosticConsole_Tx(void);
FUNC(void, AUTOMATIC) DiagnosticEventControl_SM(void);
FUNC(void,AUTOMATIC) vfnDiagnosticConsole_Print(DiagM_Module_t cxModule, DiagM_Layer_t cxLayer, char_t* cxMessagePtr);
FUNC(void, AUTOMATIC) vfnDiagnosticConsole_RunRoutine(RoutineId_t cxRoutine);


#endif