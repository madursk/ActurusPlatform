#ifndef OS_CFG_H
#define OS_CFG_H

#include "Os_types.h"
#define NUMBER_OF_TASKS 4u
#define ECC1 STD_ON
#define SCHEDULE_TABLE
#define OS_ENABLED STD_ON

#if(ECC1 == STD_ON)
#define MIN_NUMBER_OF_PRIORITIES 16u
#define MIN_NUMBER_EVENTS_PER_TASK 8u
#define MIN_NUMBER_RESOURSES 8u
#define MIN_NUMBER_INT_RES 2u
#define MIN_NUMBER_ALARM 1u
#define APP_MODE 1u
#endif

#define TASK_INIT_ 0x01
#define TASK_BSW_ 0x02
#define TASK_APPL_ 0x03
#define TASK_IDLE_ 0x04

#define EV_10MS 0x02
#define EV_20MS 0x04
#define EV_50MS 0x0A
#define EV_100MS 0x14


#define ONE_ACTIVATION 0x01

#define NONINIT 0x01u
#define INIT 0x02u
typedef struct
{
 AlarmsStates_e e10_ms_AlarmState;
 AlarmsStates_e e20_ms_AlarmState;
 AlarmsStates_e e50_ms_AlarmState;
 AlarmsStates_e e100_ms_AlarmState;
}Alarms_st;

typedef Alarms_st* Alarms_stPtr;

#endif