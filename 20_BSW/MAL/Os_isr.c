#include "Os_isr.h"
#include "Os_isr_Cfg.h"
#include "Os.h"
#include "NVIC.h"
#include "I2C_irq.h"
#include "UART_irq.h"

extern UART3_IRQHandler_Ptr uart3_IRQ;
extern I2C0_IRQHandler_Ptr i2c0_IRQ;
uint32_t nestedIRQs_u32;


void ResumeOSInterrupts(void)
{
    __asm__("mov R0,%[value]": : [value] "r"(0x00));
    __asm__("MSR PRIMASK,R0": : );
}

void SuspendOSInterrupts(void)
{
    __asm__("mov R0,%[value]": : [value] "r"(0x01));
    __asm__("MSR PRIMASK,R0": : );
}