#ifndef DIAG_M_CFG_H
#define DIAG_M_CFG_H

#include "DiagM_Types.h"
#define GetChannelForConsolePtr  0u

FUNC(void, AUTOMATIC) vfnSetRoutines(void);
FUNC_P2VAR(Routines_t,AUTOMATIC) cxfnGetRoutineTablePtr(RoutineId_t cxRoutine);
FUNC(void,AUTOMATIC) vfnDiagnosticConsole_Banner(void);
#endif