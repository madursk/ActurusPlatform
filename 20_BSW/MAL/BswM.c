

#include "BswM.h"
#include "IoHandler.h"
#include "DiagHandler.h"
#include "DiagM.h"

HAL_STATUS_s halStatus_s;

#define PRINT(x)     vfnDiagnosticConsole_Print(MAL,BSWM,x)
FUNC(void,AUTOMATIC)BswM_Init()
{
    vfnHal_Init();
    vfnMal_Init();
}
FUNC(void,AUTOMATIC)BswM_Scheduler()
{

}

FUNC(void,AUTOMATIC)vfnHal_Init(void)
{
    u16fnDiagHandler_Init();
}

FUNC(void,AUTOMATIC)vfnMal_Init(void)
{
    DiagnosticManager_Init();
}
