#include "DiagM_Cfg.h"

VAR(Routines_t, DEFAULT) cxBannerRoutine;
VAR(Routines_t, DEFAULT) cxMcalCheckRoutine;
VAR(Routines_t, DEFAULT) cxHalCheckRoutine;
VAR(Routines_t, DEFAULT) cxMalCheckRoutine;

char_t chBannerVctrLine1[] = {"/******************************************/\n"};
char_t chBannerVctrLine2[] = {"*              ACTURUS PLATFORM             *\n"};
char_t chBannerVctrLine3[] = {"*              Software Version             *\n"};
char_t chBannerVctrLine4[] = {"*                      1.0.0                *\n"};
char_t chBannerVctrLine5[] = {"/*******************************************/\n"};

Routines_t* cxRoutines_Table[] =
{
    &cxBannerRoutine, &cxMcalCheckRoutine, &cxHalCheckRoutine,
    &cxMalCheckRoutine
};

/**
 * Name vfnSetRoutines
 * @brief Sets the configuration for Console Routines
 * 
 * @param None
 * @return None
 */
FUNC(void, AUTOMATIC) vfnSetRoutines(void)
{
    cxBannerRoutine.routineId = BANNER;
    cxBannerRoutine.routineSteps = 5u;
}

/**
 * Name cxfnGetRoutineTablePtr
 * @brief Gets the pointer to the routine table
 * 
 * @param RoutineId_t cxRoutine
 * @return Routines_t* pointer to the routine table
 */
FUNC_P2VAR(Routines_t, AUTOMATIC) cxfnGetRoutineTablePtr(RoutineId_t cxRoutine)
{
    return cxRoutines_Table[cxRoutine];
}

/**
 * Name vfnDiagnosticConsole_Banner
 * @brief Exceution of printing the banner
 * 
 * @param None
 * @return None
 */
FUNC(void,AUTOMATIC) vfnDiagnosticConsole_Banner(void)
{
    boolean bIsBannerPrinted = FALSE;
    uint8_t u8RoutineSteps = 0u;
    Routines_t cxBannerRoutine = (*cxfnGetRoutineTablePtr(BANNER));

    stdfnDiagHandler_InsertToQueue(&chBannerVctrLine1[0], SELECT_INTERNAL_CHANNEL);
    vfnDiagHandler_TxQueueProcessing();
    while(bGetTxConfirmationIntDiag() != TRUE);
    stdfnDiagHandler_RemoveFromQueue();

    stdfnDiagHandler_InsertToQueue(&chBannerVctrLine2[0], SELECT_INTERNAL_CHANNEL);
    vfnDiagHandler_TxQueueProcessing();
    while(bGetTxConfirmationIntDiag() != TRUE);
    stdfnDiagHandler_RemoveFromQueue();

    stdfnDiagHandler_InsertToQueue(&chBannerVctrLine3[0], SELECT_INTERNAL_CHANNEL);
    vfnDiagHandler_TxQueueProcessing();
    while(bGetTxConfirmationIntDiag() != TRUE);
    stdfnDiagHandler_RemoveFromQueue();

    stdfnDiagHandler_InsertToQueue(&chBannerVctrLine4[0], SELECT_INTERNAL_CHANNEL);
    vfnDiagHandler_TxQueueProcessing();
    while(bGetTxConfirmationIntDiag() != TRUE);
    stdfnDiagHandler_RemoveFromQueue();

    stdfnDiagHandler_InsertToQueue(&chBannerVctrLine5[0], SELECT_INTERNAL_CHANNEL);
    vfnDiagHandler_TxQueueProcessing();
    while(bGetTxConfirmationIntDiag() != TRUE);
    stdfnDiagHandler_RemoveFromQueue();

}