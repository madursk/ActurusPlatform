#include "Os_Hal_Isr_cfg.h"

extern uint32_t _estack;

OS_CODE_INTVECT  uint32_t const IsrInterruptVectorTable[] = 
{
    
    (uint32_t)&_estack,
    (uint32_t)Reset_Handler,
    (uint32_t)NMI_Handler,
    (uint32_t)HardFault_Handler,
    (uint32_t)MemManageFault_Handler,
    (uint32_t)BusFault_Handler,
    (uint32_t)UsageFault_Handler,
    0,
    0,
    0,
    0,
    (uint32_t)SVC_Handler,
    (uint32_t)DebugMon_Handler,
    0,
    (uint32_t)PendSRV_Handler,
    (uint32_t)SysStick_Handler,
    (uint32_t)DMA_Channel0_IRQHandler,
    (uint32_t)DMA_Channel1_IRQHandler,
    (uint32_t)DMA_Channel2_IRQHandler,
    (uint32_t)DMA_Channel3_IRQHandler,
    (uint32_t)DMA_Channel4_IRQHandler,
    (uint32_t)DMA_Channel5_IRQHandler,
    (uint32_t)DMA_Channel6_IRQHandler,
    (uint32_t)DMA_Channel7_IRQHandler,
    (uint32_t)DMA_Channel8_IRQHandler,
    (uint32_t)DMA_Channel9_IRQHandler,
    (uint32_t)DMA_Channel10_IRQHandler,
    (uint32_t)DMA_Channel11_IRQHandler,
    (uint32_t)DMA_Channel12_IRQHandler,
    (uint32_t)DMA_Channel13_IRQHandler,
    (uint32_t)DMA_Channel14_IRQHandler,
    (uint32_t)DMA_Channel15_IRQHandler,
    (uint32_t)DMA_Error_IRQHandler,
    (uint32_t)MCM_IRQHandler,
    (uint32_t)FLS_CC_IRQHandler,
    (uint32_t)FLS_RC_IRQHandler,
    (uint32_t)MODCON_LV_IRQHandler,
    (uint32_t)LLWU_IRQHandler,
    (uint32_t)WDG_IRQHandler,
    (uint32_t)CRY_RNG_IRQHandler,
    (uint32_t)I2C0_IRQHandler,
    (uint32_t)I2C1_IRQHandler,
    (uint32_t)SPI0_IRQHandler,
    (uint32_t)SPI1_IRQHandler,
    (uint32_t)I2S0_TX_IRQHandler,
    (uint32_t)I2S0_RX_IRQHandler,
    0,
    (uint32_t)UART0_IRQHandler,
    (uint32_t)UART0_Error_IRQHandler,
    (uint32_t)UART1_IRQHandler,
    (uint32_t)UART1_Error_IRQHandler,
    (uint32_t)UART2_IRQHandler,
    (uint32_t)UART2_Error_IRQHandler,
    (uint32_t)UART3_IRQHandler,
    (uint32_t)UART3_Error_IRQHandler,
    (uint32_t)ADC0_IRQHandler,
    (uint32_t)CMP0_IRQHandler,
    (uint32_t)CMP1_IRQHandler,
    (uint32_t)FTM0_IRQHandler,
    (uint32_t)FTM1_IRQHandler,
    (uint32_t)FTM2_IRQHandler,
    (uint32_t)CMT_IRQHandler,
    (uint32_t)RTC_Alarm_IRQHanlder,
    (uint32_t)RTC_Sec_IRQHandler,
    (uint32_t)PIT0_IRQHandler,
    (uint32_t)PIT1_IRQHandler,
    (uint32_t)PIT2_IRQHandler,
    (uint32_t)PIT3_IRQHandler,
    (uint32_t)PTB_IRQHandler,
    (uint32_t)USB_OTG_IRQHandler,
    (uint32_t)USB_CHRG_IRQHandler,
    0,
    (uint32_t)DAC0_IRQHandler,
    (uint32_t)MCG_IRQHandler,
    (uint32_t)LP_TIMER_IRQHandler,
    (uint32_t)PCMA_IRQHandler,
    (uint32_t)PCMB_IRQHandler,
    (uint32_t)PCMC_IRQHandler,
    (uint32_t)PCMD_IRQHandler,
    (uint32_t)PCME_IRQHandler,
    (uint32_t)SW_IRQHandler,
    (uint32_t)SPI2_IRQHandler,
    (uint32_t)UART4_IRQHandler,
    (uint32_t)UART4_Error_IRQHandler,
    (uint32_t)UART5_IRQHandler,
    (uint32_t)UART5_Error_IRQHandler,
    (uint32_t)CMP2_IRQHandler,
    (uint32_t)FTM3_IRQHandler,
    (uint32_t)DAC1_IRQHandler,
    (uint32_t)ADC1_IRQHandler,
    (uint32_t)I2C2_IRQHandler,
    (uint32_t)CAN0_BUFF_IRQHandler,
    (uint32_t)CAN0_BUSOFF_IRQHandler,
    (uint32_t)CAN0_Error_IRQHandler,
    (uint32_t)CAN0_TXW_IRQHandler,
    (uint32_t)CAN0_RXW_IRQHandler,
    (uint32_t)CAN0_WU_IRQHandler,
    (uint32_t)SDHC_IRQHandler,
    (uint32_t)ETH_TMR_IRQHandler,
    (uint32_t)ETH_TX_IRQHandler,
    (uint32_t)ETH_RX_IRQHandler,
    (uint32_t)ETH_Error_IRQHandler,
};

void NMI_Handler(void)
{
 __asm("bkpt");
}
void HardFault_Handler(void)
{
 __asm("bkpt");
}
void MemManageFault_Handler(void)
{
  __asm("bkpt");
}
void BusFault_Handler(void)
{
  __asm("bkpt");
}
void UsageFault_Handler(void)
{
 __asm("bkpt");
}
void DebugMon_Handler(void)
{
 __asm("bkpt");
}

void DMA_Channel0_IRQHandler(void)
{
     __asm("bkpt");
}

void DMA_Channel1_IRQHandler(void)
{
     __asm("bkpt");
}

void DMA_Channel2_IRQHandler(void)       
{
     __asm("bkpt");
}

void DMA_Channel3_IRQHandler(void)       
{
     __asm("bkpt");
}

void DMA_Channel4_IRQHandler(void)       
{
     __asm("bkpt");
}

void DMA_Channel5_IRQHandler(void)       
{
 __asm("bkpt");
}

void DMA_Channel6_IRQHandler(void)       
{
 __asm("bkpt");
}

void DMA_Channel7_IRQHandler(void)       
{
 __asm("bkpt");
}

void DMA_Channel8_IRQHandler(void)       
{
 __asm("bkpt");
}

void DMA_Channel9_IRQHandler(void)       
{
 __asm("bkpt");
}

void DMA_Channel10_IRQHandler(void)      
{
     __asm("bkpt");
}

void DMA_Channel11_IRQHandler(void)      
{
     __asm("bkpt");
}

void DMA_Channel12_IRQHandler(void)      
{
     __asm("bkpt");
}
void DMA_Channel13_IRQHandler(void)      
{
     __asm("bkpt");
}
void DMA_Channel14_IRQHandler(void)      
{
     __asm("bkpt");
}
void DMA_Channel15_IRQHandler(void)      
{
     __asm("bkpt");
}
void DMA_Error_IRQHandler(void)          
{
     __asm("bkpt");
}
void MCM_IRQHandler(void)                
{
     __asm("bkpt");
}
void FLS_CC_IRQHandler(void)             
{
     __asm("bkpt");
}
void FLS_RC_IRQHandler(void)             
{
     __asm("bkpt");
}
void MODCON_LV_IRQHandler(void)          
{
     __asm("bkpt");
}
void LLWU_IRQHandler(void)               
{
     __asm("bkpt");
}
void WDG_IRQHandler(void)                
{
     __asm("bkpt");
}
void CRY_RNG_IRQHandler(void)            
{
     __asm("bkpt");
}

void I2C1_IRQHandler(void)               
{
     __asm("bkpt");
}
void SPI0_IRQHandler(void)               
{
     __asm("bkpt");
}
void SPI1_IRQHandler(void)               
{
     __asm("bkpt");
}
void I2S0_TX_IRQHandler(void)            
{
     __asm("bkpt");
}
void I2S0_RX_IRQHandler(void)            
{
     __asm("bkpt");
}
void UART0_IRQHandler(void)              
{
     __asm("bkpt");
}
void UART0_Error_IRQHandler(void)        
{
     __asm("bkpt");
}
void UART1_IRQHandler(void)              
{
     __asm("bkpt");
}
void UART1_Error_IRQHandler(void)        
{
     __asm("bkpt");
}
void UART2_IRQHandler(void)              
{
     __asm("bkpt");
}
void UART2_Error_IRQHandler(void)        
{
     __asm("bkpt");
}
void UART3_Error_IRQHandler(void)        
{
     __asm("bkpt");
}
void ADC0_IRQHandler(void)               
{
     __asm("bkpt");
}
void CMP0_IRQHandler(void)               
{
     __asm("bkpt");
}
void CMP1_IRQHandler(void)               
{
     __asm("bkpt");
}
void FTM0_IRQHandler(void)               
{
     __asm("bkpt");
}
void FTM1_IRQHandler(void)               
{
     __asm("bkpt");
}
void FTM2_IRQHandler(void)               
{
     __asm("bkpt");
}
void CMT_IRQHandler(void)                
{
     __asm("bkpt");
}
void RTC_Alarm_IRQHanlder(void)          
{
     __asm("bkpt");
}
void RTC_Sec_IRQHandler(void)            
{
     __asm("bkpt");
}
void PIT0_IRQHandler(void)               
{
     __asm("bkpt");
}
void PIT1_IRQHandler(void)               
{
     __asm("bkpt");
}
void PIT2_IRQHandler(void)               
{
     __asm("bkpt");
}
void PIT3_IRQHandler(void)               
{
     __asm("bkpt");
}
void PTB_IRQHandler(void)                
{
     __asm("bkpt");
}
void USB_OTG_IRQHandler(void)            
{
     __asm("bkpt");
}
void USB_CHRG_IRQHandler(void)           
{
     __asm("bkpt");
}
void DAC0_IRQHandler(void)               
{
     __asm("bkpt");
}
void MCG_IRQHandler(void)                
{
     __asm("bkpt");
}
void LP_TIMER_IRQHandler(void)           
{
     __asm("bkpt");
}
void PCMA_IRQHandler(void)               
{
     __asm("bkpt");
}
void PCMB_IRQHandler(void)               
{
     __asm("bkpt");
}
void PCMC_IRQHandler(void)            
{
     __asm("bkpt");
}
void PCMD_IRQHandler(void)              
{
     __asm("bkpt");
}
void PCME_IRQHandler(void)         
{
     __asm("bkpt");
}
void SW_IRQHandler(void)             
{
     __asm("bkpt");
}
void SPI2_IRQHandler(void)             
{
     __asm("bkpt");
}
void UART4_IRQHandler(void)             
{
     __asm("bkpt");
}
void UART4_Error_IRQHandler(void)       
{
     __asm("bkpt");
}
void UART5_IRQHandler(void)           
{
     __asm("bkpt");
}
void UART5_Error_IRQHandler(void)      
{
     __asm("bkpt");
}
void CMP2_IRQHandler(void)            
{
     __asm("bkpt");
}
void FTM3_IRQHandler(void)             
{
     __asm("bkpt");
}
void DAC1_IRQHandler(void)            
{
     __asm("bkpt");
}
void ADC1_IRQHandler(void)              
{
     __asm("bkpt");
}
void I2C2_IRQHandler(void)               
{
  __asm("bkpt");
}
void CAN0_BUFF_IRQHandler(void)       
{
 __asm("bkpt");
}
void CAN0_BUSOFF_IRQHandler(void)       
{
 __asm("bkpt");
}
void CAN0_Error_IRQHandler(void)        
{
 __asm("bkpt");
}
void CAN0_TXW_IRQHandler(void)          
{
 __asm("bkpt");
}
void CAN0_RXW_IRQHandler(void)          
{
 __asm("bkpt");
}
void CAN0_WU_IRQHandler(void)          
{
 __asm("bkpt");
}
void SDHC_IRQHandler(void)
{
    __asm("bkpt");
}
void ETH_TMR_IRQHandler(void)
{
    __asm("bkpt");
}
void ETH_TX_IRQHandler(void)            
{
 __asm("bkpt");
} 

void ETH_RX_IRQHandler(void)            
{
 __asm("bkpt");
}
void ETH_Error_IRQHandler(void)          
{
 __asm("bkpt");
}