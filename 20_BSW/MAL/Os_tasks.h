#ifndef OS_TASKS_H
#define OS_TASKS_H

#include "Os_types.h"

typedef void TASK;

#define TASK_INIT_PRIORITY 1u
#define TASK_BSW_PRIORITY 2u
#define TASK_APPL_PRIORITY 3u
#define TASK_IDLE_PRIORITY 16u

#define PRIORITY_0 0u
#define PRIORITY_1 1u
#define PRIORITY_2 2u
#define PRIORITY_3 3u
#define PRIORITY_4 4u
#define PRIORITY_5 5u
#define PRIORITY_6 6u
#define PRIORITY_7 7u
#define PRIORITY_8 8u
#define PRIORITY_9 9u
#define PRIORITY_10 10u
#define PRIORITY_11 11u
#define PRIORITY_12 12u
#define PRIORITY_13 13u
#define PRIORITY_14 14u
#define PRIORITY_15 15u
#define PRIORITY_16 16u
void Os_SetTasks();
TASK TASK_INIT();
TASK TASK_BSW();
TASK TASK_APPL();
TASK TASK_IDLE();
extern inline void SwitchTask(void);
extern inline void Scheduler(void);
#endif