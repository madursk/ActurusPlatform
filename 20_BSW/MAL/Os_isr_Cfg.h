#ifndef OS_ISR_CFG_H
#define OS_ISR_CFG_H

#include "Os_types.h"

#define CAT_2_IRQ 85u
#define IABR_LEBGTH 32u

void(*isrQueue[CAT_2_IRQ])(void);

#endif