#ifndef APP_CONTROLLER_H
#define APP_CONTROLLER_H
#include "Datatypes.h"
#include "DiagM.h"

#define PRINT(x)     vfnDiagnosticConsole_Print(APP,NA,x)
FUNC(void,AUTOMATIC) vfnAppController_Main(void);
FUNC(void,AUTOMATIC) vfnAppController_Init(void);
#endif