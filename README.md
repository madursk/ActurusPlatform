# Acturus Platform

## Description

Hello my name is Miguel Duran, I am an embedded software engineer with 5+ years of experience in automotive domain, since I was in college embedded has been my life long passion, there is something about interfacing between software and hardware that fascinates me, unfortunately most of the projects I worked on had already an established middleware.

 I have spent most of my career working with AUTOSAR classic configuring so the chances to work on pure embedded development has been few.

 This Platform is the materialization of my passion and love for embedded and is an ongoing process for everyone with the same passion,please take a look!

## Project Structure

The project is ment to run on windows using bat files to set up all the necessary environment

![Project Structure](http://url/to/img.png)

## Documentation

The documentation can be found in the [doc directory](doc/index.md).


