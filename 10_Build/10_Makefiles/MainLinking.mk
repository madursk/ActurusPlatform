##########################################################################
##																		##
##							LINKED SOLUTIONS							##
##  Copyright (C) 2019 linked Solutions, SAPI. All rights reserved.		##
##																		##
##																		##
##																		##
##########################################################################

#Set project directory
PROJDIR :=M:

#set project bsw sub modules
BSW_SUBMOD = Startup

#set subdirectories
BSW_SOURCE := $(PROJDIR)/20_BSW

#set project name
PORJNAME = ActurusPlatform

#Set Object path
OBJ_DIR = $(PROJDIR)/10_build/20_Results/objects

#Set binary path 
BIN_DIR =  $(PROJDIR)/10_build/20_Results/bin
#Set final executable name
TARGET = $(BIN_DIR)/$(PORJNAME).elf
#set Linker Path
LINKER_PATH := $(BSW_SOURCE)/$(BSW_SUBMOD)/PlatformB.ld

#Set Compiler 
CC = arm-none-eabi-gcc

#Set compilation flags
CFLAGS = -mcpu=cortex-m4 \
         -mthumb \
         -mfloat-abi=hard \
         -mfpu=fpv4-sp-d16 \
         -g3 \
         -o0 \
         -ffunction-sections\
         -fdata-sections \
         -fmessage-length=0 \
         -Wall \
         -gstrict-dwarf \
         -gdwarf-2 \
         -std=c99

LFLAGS = -nostdlib -Xlinker -Map=$(BIN_DIR)/$(PORJNAME).map -T 

#Rule to link
$(TARGET) :
	$(CC) $(LFLAGS) $(LINKER_PATH) -Xlinker -print-memory-usage $(OBJ_DIR)/*.o -o $@